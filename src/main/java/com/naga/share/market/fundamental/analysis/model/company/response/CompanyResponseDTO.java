package com.naga.share.market.fundamental.analysis.model.company.response;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyResponseDTO {

	private String companyName;

	private String companySymbol;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanySymbol() {
		return companySymbol;
	}

	public void setCompanySymbol(String companySymbol) {
		this.companySymbol = companySymbol;
	}

	@Override
	public String toString() {
		return "CompanyResponseDTO [companyName=" + companyName + ", companySymbol=" + companySymbol + "]";
	}

}
