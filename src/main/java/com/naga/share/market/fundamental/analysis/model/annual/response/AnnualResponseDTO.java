package com.naga.share.market.fundamental.analysis.model.annual.response;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.naga.share.market.fundamental.analysis.model.annual.request.AnnualAnalyzeRequestDTO;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;
import com.naga.share.market.fundamental.analysis.model.industry.response.IndustryResponseDTO;
import com.naga.share.market.fundamental.analysis.model.sector.response.SectorResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnnualResponseDTO {

	private SectorResponseDTO sector;

	private IndustryResponseDTO industry;

	private CompanyResponseDTO company;

	private AnnuallyAnalyzedDetails annualAnalyzedDetails;

	private AnnualAnalyzeRequestDTO annualAnalysisInput;

	private List<Integer> analysisYears;

	/**
	 * @return the analysisYears
	 */
	public List<Integer> getAnalysisYears() {
		return analysisYears;
	}

	/**
	 * @param analysisYears the analysisYears to set
	 */
	public void setAnalysisYears(List<Integer> analysisYears) {
		this.analysisYears = analysisYears;
	}

	/**
	 * @return the company
	 */
	public CompanyResponseDTO getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(CompanyResponseDTO company) {
		this.company = company;
	}

	/**
	 * @return the sector
	 */
	public SectorResponseDTO getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(SectorResponseDTO sector) {
		this.sector = sector;
	}

	/**
	 * @return the industry
	 */
	public IndustryResponseDTO getIndustry() {
		return industry;
	}

	/**
	 * @param industry the industry to set
	 */
	public void setIndustry(IndustryResponseDTO industry) {
		this.industry = industry;
	}

	/**
	 * @return the annualAnalyzedDetails
	 */
	public AnnuallyAnalyzedDetails getAnnualAnalyzedDetails() {
		return annualAnalyzedDetails;
	}

	/**
	 * @param annualAnalyzedDetails the annualAnalyzedDetails to set
	 */
	public void setAnnualAnalyzedDetails(AnnuallyAnalyzedDetails annualAnalyzedDetails) {
		this.annualAnalyzedDetails = annualAnalyzedDetails;
	}

	/**
	 * @return the annualAnalysisInput
	 */
	public AnnualAnalyzeRequestDTO getAnnualAnalysisInput() {
		return annualAnalysisInput;
	}

	/**
	 * @param annualAnalysisInput the annualAnalysisInput to set
	 */
	public void setAnnualAnalysisInput(AnnualAnalyzeRequestDTO annualAnalysisInput) {
		this.annualAnalysisInput = annualAnalysisInput;
	}

	@Override
	public String toString() {
		return "AnnualResponseDTO [sector=" + sector + ", industry=" + industry + ", company=" + company
				+ ", annualAnalyzedDetails=" + annualAnalyzedDetails + ", annualAnalysisInput=" + annualAnalysisInput
				+ ", analysisYears=" + analysisYears + "]";
	}

}
