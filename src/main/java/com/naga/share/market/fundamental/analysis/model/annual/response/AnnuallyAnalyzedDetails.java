package com.naga.share.market.fundamental.analysis.model.annual.response;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnnuallyAnalyzedDetails {

	private int analysisYear;

	private String grossProfitMarginCheck;

	private String EBITDAMarginCheck;

	private String profitAfterTaxMarginCheck;

	private String returnOnEquityCheck;

	private String returnOnAssetsCheck;

	private String returnOnCapitalEmployedCheck;

	private String interestCoverageRatioCheck;

	private String debtToEquityRatioCheck;

	private String debtToAssetsRatioCheck;

	private String financialLeverageRatioCheck;

	private String fixedAssetsTurnoverRatioCheck;

	private String totalAssetsTurnoverRatioCheck;

	private String workingCapitalTurnoverRatioCheck;

	private String priceToSalesRatioCheck;

	private String priceToBookRatioCheck;

	private String priceToEarningsRatioCheck;

	private double grossProfit;

	private double earningsBfrIncomeTaxDepriAndAmor;

	private double profitBeforeTax;

	private double profitAfterTax;

	private double totalAssets;

	private double cashFlow;

	private double cashAndCashEquivalentAtYearEnding;

	private double grossProfitMargin;

	private double earningsBfrIncomeTaxDepriAndAmorMargin;

	private double profitAfterTaxMargin;

	private double netProfitMargin;

	private double returnOnEquity;

	private double returnOnAssets;

	private double returnOnCapitalEmployed;

	private double interestCoverageRatio;

	private double debtToEquityRatio;

	private double debtToAssestRatio;

	private double financialLeverageRatio;

	private double fixedAssetsTurnoverRatio;

	private double totalAssetsTurnoverRatio;

	private double workingCapitalTurnoverRatio;

	private double inventoryTurnoverRatio;

	private double inventoryNoOfDays;

	private double accountsReceivableTurnoverRatio;

	private double daysSalesOutstanding;

	private double salesPerShare;

	private double bookValuePerShare;

	private double earningsPerShare;

	private double priceToSalesRatio;

	private double priceToBookValueRatio;

	private double priceToEarningsRatio;

	/**
	 * @return the analysisYear
	 */
	public int getAnalysisYear() {
		return analysisYear;
	}

	/**
	 * @param analysisYear the analysisYear to set
	 */
	public void setAnalysisYear(int analysisYear) {
		this.analysisYear = analysisYear;
	}

	/**
	 * @return the grossProfitMarginCheck
	 */
	public String getGrossProfitMarginCheck() {
		return grossProfitMarginCheck;
	}

	/**
	 * @param grossProfitMarginCheck the grossProfitMarginCheck to set
	 */
	public void setGrossProfitMarginCheck(String grossProfitMarginCheck) {
		this.grossProfitMarginCheck = grossProfitMarginCheck;
	}

	/**
	 * @return the eBITDAMarginCheck
	 */
	public String getEBITDAMarginCheck() {
		return EBITDAMarginCheck;
	}

	/**
	 * @param eBITDAMarginCheck the eBITDAMarginCheck to set
	 */
	public void setEBITDAMarginCheck(String eBITDAMarginCheck) {
		EBITDAMarginCheck = eBITDAMarginCheck;
	}

	/**
	 * @return the profitAfterTaxMarginCheck
	 */
	public String getProfitAfterTaxMarginCheck() {
		return profitAfterTaxMarginCheck;
	}

	/**
	 * @param profitAfterTaxMarginCheck the profitAfterTaxMarginCheck to set
	 */
	public void setProfitAfterTaxMarginCheck(String profitAfterTaxMarginCheck) {
		this.profitAfterTaxMarginCheck = profitAfterTaxMarginCheck;
	}

	/**
	 * @return the returnOnEquityCheck
	 */
	public String getReturnOnEquityCheck() {
		return returnOnEquityCheck;
	}

	/**
	 * @param returnOnEquityCheck the returnOnEquityCheck to set
	 */
	public void setReturnOnEquityCheck(String returnOnEquityCheck) {
		this.returnOnEquityCheck = returnOnEquityCheck;
	}

	/**
	 * @return the returnOnAssetsCheck
	 */
	public String getReturnOnAssetsCheck() {
		return returnOnAssetsCheck;
	}

	/**
	 * @param returnOnAssetsCheck the returnOnAssetsCheck to set
	 */
	public void setReturnOnAssetsCheck(String returnOnAssetsCheck) {
		this.returnOnAssetsCheck = returnOnAssetsCheck;
	}

	/**
	 * @return the returnOnCapitalEmployedCheck
	 */
	public String getReturnOnCapitalEmployedCheck() {
		return returnOnCapitalEmployedCheck;
	}

	/**
	 * @param returnOnCapitalEmployedCheck the returnOnCapitalEmployedCheck to set
	 */
	public void setReturnOnCapitalEmployedCheck(String returnOnCapitalEmployedCheck) {
		this.returnOnCapitalEmployedCheck = returnOnCapitalEmployedCheck;
	}

	/**
	 * @return the interestCoverageRatioCheck
	 */
	public String getInterestCoverageRatioCheck() {
		return interestCoverageRatioCheck;
	}

	/**
	 * @param interestCoverageRatioCheck the interestCoverageRatioCheck to set
	 */
	public void setInterestCoverageRatioCheck(String interestCoverageRatioCheck) {
		this.interestCoverageRatioCheck = interestCoverageRatioCheck;
	}

	/**
	 * @return the debtToEquityRatioCheck
	 */
	public String getDebtToEquityRatioCheck() {
		return debtToEquityRatioCheck;
	}

	/**
	 * @param debtToEquityRatioCheck the debtToEquityRatioCheck to set
	 */
	public void setDebtToEquityRatioCheck(String debtToEquityRatioCheck) {
		this.debtToEquityRatioCheck = debtToEquityRatioCheck;
	}

	/**
	 * @return the debtToAssetsRatioCheck
	 */
	public String getDebtToAssetsRatioCheck() {
		return debtToAssetsRatioCheck;
	}

	/**
	 * @param debtToAssetsRatioCheck the debtToAssetsRatioCheck to set
	 */
	public void setDebtToAssetsRatioCheck(String debtToAssetsRatioCheck) {
		this.debtToAssetsRatioCheck = debtToAssetsRatioCheck;
	}

	/**
	 * @return the financialLeverageRatioCheck
	 */
	public String getFinancialLeverageRatioCheck() {
		return financialLeverageRatioCheck;
	}

	/**
	 * @param financialLeverageRatioCheck the financialLeverageRatioCheck to set
	 */
	public void setFinancialLeverageRatioCheck(String financialLeverageRatioCheck) {
		this.financialLeverageRatioCheck = financialLeverageRatioCheck;
	}

	/**
	 * @return the fixedAssetsTurnoverRatioCheck
	 */
	public String getFixedAssetsTurnoverRatioCheck() {
		return fixedAssetsTurnoverRatioCheck;
	}

	/**
	 * @param fixedAssetsTurnoverRatioCheck the fixedAssetsTurnoverRatioCheck to set
	 */
	public void setFixedAssetsTurnoverRatioCheck(String fixedAssetsTurnoverRatioCheck) {
		this.fixedAssetsTurnoverRatioCheck = fixedAssetsTurnoverRatioCheck;
	}

	/**
	 * @return the totalAssetsTurnoverRatioCheck
	 */
	public String getTotalAssetsTurnoverRatioCheck() {
		return totalAssetsTurnoverRatioCheck;
	}

	/**
	 * @param totalAssetsTurnoverRatioCheck the totalAssetsTurnoverRatioCheck to set
	 */
	public void setTotalAssetsTurnoverRatioCheck(String totalAssetsTurnoverRatioCheck) {
		this.totalAssetsTurnoverRatioCheck = totalAssetsTurnoverRatioCheck;
	}

	/**
	 * @return the workingCapitalTurnoverRatioCheck
	 */
	public String getWorkingCapitalTurnoverRatioCheck() {
		return workingCapitalTurnoverRatioCheck;
	}

	/**
	 * @param workingCapitalTurnoverRatioCheck the workingCapitalTurnoverRatioCheck
	 *                                         to set
	 */
	public void setWorkingCapitalTurnoverRatioCheck(String workingCapitalTurnoverRatioCheck) {
		this.workingCapitalTurnoverRatioCheck = workingCapitalTurnoverRatioCheck;
	}

	/**
	 * @return the priceToSalesRatioCheck
	 */
	public String getPriceToSalesRatioCheck() {
		return priceToSalesRatioCheck;
	}

	/**
	 * @param priceToSalesRatioCheck the priceToSalesRatioCheck to set
	 */
	public void setPriceToSalesRatioCheck(String priceToSalesRatioCheck) {
		this.priceToSalesRatioCheck = priceToSalesRatioCheck;
	}

	/**
	 * @return the priceToBookRatioCheck
	 */
	public String getPriceToBookRatioCheck() {
		return priceToBookRatioCheck;
	}

	/**
	 * @param priceToBookRatioCheck the priceToBookRatioCheck to set
	 */
	public void setPriceToBookRatioCheck(String priceToBookRatioCheck) {
		this.priceToBookRatioCheck = priceToBookRatioCheck;
	}

	/**
	 * @return the priceToEarningsCheck
	 */
	public String getPriceToEarningsRatioCheck() {
		return priceToEarningsRatioCheck;
	}

	/**
	 * @param priceToEarningsRatioCheck the priceToEarningsCheck to set
	 */
	public void setPriceToEarningsRatioCheck(String priceToEarningsRatioCheck) {
		this.priceToEarningsRatioCheck = priceToEarningsRatioCheck;
	}

	/**
	 * @return the grossProfit
	 */
	public double getGrossProfit() {
		return grossProfit;
	}

	/**
	 * @param grossProfit the grossProfit to set
	 */
	public void setGrossProfit(double grossProfit) {
		this.grossProfit = grossProfit;
	}

	/**
	 * @return the earningsBfrIncomeTaxDepriAndAmor
	 */
	public double getEarningsBfrIncomeTaxDepriAndAmor() {
		return earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @param earningsBfrIncomeTaxDepriAndAmor the earningsBfrIncomeTaxDepriAndAmor
	 *                                         to set
	 */
	public void setEarningsBfrIncomeTaxDepriAndAmor(double earningsBfrIncomeTaxDepriAndAmor) {
		this.earningsBfrIncomeTaxDepriAndAmor = earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @return the profitBeforeTax
	 */
	public double getProfitBeforeTax() {
		return profitBeforeTax;
	}

	/**
	 * @param profitBeforeTax the profitBeforeTax to set
	 */
	public void setProfitBeforeTax(double profitBeforeTax) {
		this.profitBeforeTax = profitBeforeTax;
	}

	/**
	 * @return the profitAfterTax
	 */
	public double getProfitAfterTax() {
		return profitAfterTax;
	}

	/**
	 * @param profitAfterTax the profitAfterTax to set
	 */
	public void setProfitAfterTax(double profitAfterTax) {
		this.profitAfterTax = profitAfterTax;
	}

	/**
	 * @return the totalAssets
	 */
	public double getTotalAssets() {
		return totalAssets;
	}

	/**
	 * @param totalAssets the totalAssets to set
	 */
	public void setTotalAssets(double totalAssets) {
		this.totalAssets = totalAssets;
	}

	/**
	 * @return the cashFlow
	 */
	public double getCashFlow() {
		return cashFlow;
	}

	/**
	 * @param cashFlow the cashFlow to set
	 */
	public void setCashFlow(double cashFlow) {
		this.cashFlow = cashFlow;
	}

	/**
	 * @return the cashAndCashEquivalentAtYearEnding
	 */
	public double getCashAndCashEquivalentAtYearEnding() {
		return cashAndCashEquivalentAtYearEnding;
	}

	/**
	 * @param cashAndCashEquivalentAtYearEnding the
	 *                                          cashAndCashEquivalentAtYearEnding to
	 *                                          set
	 */
	public void setCashAndCashEquivalentAtYearEnding(double cashAndCashEquivalentAtYearEnding) {
		this.cashAndCashEquivalentAtYearEnding = cashAndCashEquivalentAtYearEnding;
	}

	/**
	 * @return the grossProfitMargin
	 */
	public double getGrossProfitMargin() {
		return grossProfitMargin;
	}

	/**
	 * @param grossProfitMargin the grossProfitMargin to set
	 */
	public void setGrossProfitMargin(double grossProfitMargin) {
		this.grossProfitMargin = grossProfitMargin;
	}

	/**
	 * @return the earningsBfrIncomeTaxDepriAndAmorMargin
	 */
	public double getEarningsBfrIncomeTaxDepriAndAmorMargin() {
		return earningsBfrIncomeTaxDepriAndAmorMargin;
	}

	/**
	 * @param earningsBfrIncomeTaxDepriAndAmorMargin the
	 *                                               earningsBfrIncomeTaxDepriAndAmorMargin
	 *                                               to set
	 */
	public void setEarningsBfrIncomeTaxDepriAndAmorMargin(double earningsBfrIncomeTaxDepriAndAmorMargin) {
		this.earningsBfrIncomeTaxDepriAndAmorMargin = earningsBfrIncomeTaxDepriAndAmorMargin;
	}

	/**
	 * @return the profitAfterTaxMargin
	 */
	public double getProfitAfterTaxMargin() {
		return profitAfterTaxMargin;
	}

	/**
	 * @param profitAfterTaxMargin the profitAfterTaxMargin to set
	 */
	public void setProfitAfterTaxMargin(double profitAfterTaxMargin) {
		this.profitAfterTaxMargin = profitAfterTaxMargin;
	}

	/**
	 * @return the netProfitMargin
	 */
	public double getNetProfitMargin() {
		return netProfitMargin;
	}

	/**
	 * @param netProfitMargin the netProfitMargin to set
	 */
	public void setNetProfitMargin(double netProfitMargin) {
		this.netProfitMargin = netProfitMargin;
	}

	/**
	 * @return the returnOnEquity
	 */
	public double getReturnOnEquity() {
		return returnOnEquity;
	}

	/**
	 * @param returnOnEquity the returnOnEquity to set
	 */
	public void setReturnOnEquity(double returnOnEquity) {
		this.returnOnEquity = returnOnEquity;
	}

	/**
	 * @return the returnOnAssets
	 */
	public double getReturnOnAssets() {
		return returnOnAssets;
	}

	/**
	 * @param returnOnAssets the returnOnAssets to set
	 */
	public void setReturnOnAssets(double returnOnAssets) {
		this.returnOnAssets = returnOnAssets;
	}

	/**
	 * @return the returnOnCapitalEmployed
	 */
	public double getReturnOnCapitalEmployed() {
		return returnOnCapitalEmployed;
	}

	/**
	 * @param returnOnCapitalEmployed the returnOnCapitalEmployed to set
	 */
	public void setReturnOnCapitalEmployed(double returnOnCapitalEmployed) {
		this.returnOnCapitalEmployed = returnOnCapitalEmployed;
	}

	/**
	 * @return the interestCoverageRatio
	 */
	public double getInterestCoverageRatio() {
		return interestCoverageRatio;
	}

	/**
	 * @param interestCoverageRatio the interestCoverageRatio to set
	 */
	public void setInterestCoverageRatio(double interestCoverageRatio) {
		this.interestCoverageRatio = interestCoverageRatio;
	}

	/**
	 * @return the debtToEquityRatio
	 */
	public double getDebtToEquityRatio() {
		return debtToEquityRatio;
	}

	/**
	 * @param debtToEquityRatio the debtToEquityRatio to set
	 */
	public void setDebtToEquityRatio(double debtToEquityRatio) {
		this.debtToEquityRatio = debtToEquityRatio;
	}

	/**
	 * @return the debtToAssestRatio
	 */
	public double getDebtToAssestRatio() {
		return debtToAssestRatio;
	}

	/**
	 * @param debtToAssestRatio the debtToAssestRatio to set
	 */
	public void setDebtToAssestRatio(double debtToAssestRatio) {
		this.debtToAssestRatio = debtToAssestRatio;
	}

	/**
	 * @return the financialLeverageRatio
	 */
	public double getFinancialLeverageRatio() {
		return financialLeverageRatio;
	}

	/**
	 * @param financialLeverageRatio the financialLeverageRatio to set
	 */
	public void setFinancialLeverageRatio(double financialLeverageRatio) {
		this.financialLeverageRatio = financialLeverageRatio;
	}

	/**
	 * @return the fixedAssetsTurnoverRatio
	 */
	public double getFixedAssetsTurnoverRatio() {
		return fixedAssetsTurnoverRatio;
	}

	/**
	 * @param fixedAssetsTurnoverRatio the fixedAssetsTurnoverRatio to set
	 */
	public void setFixedAssetsTurnoverRatio(double fixedAssetsTurnoverRatio) {
		this.fixedAssetsTurnoverRatio = fixedAssetsTurnoverRatio;
	}

	/**
	 * @return the totalAssetsTurnoverRatio
	 */
	public double getTotalAssetsTurnoverRatio() {
		return totalAssetsTurnoverRatio;
	}

	/**
	 * @param totalAssetsTurnoverRatio the totalAssetsTurnoverRatio to set
	 */
	public void setTotalAssetsTurnoverRatio(double totalAssetsTurnoverRatio) {
		this.totalAssetsTurnoverRatio = totalAssetsTurnoverRatio;
	}

	/**
	 * @return the workingCapitalTurnoverRatio
	 */
	public double getWorkingCapitalTurnoverRatio() {
		return workingCapitalTurnoverRatio;
	}

	/**
	 * @param workingCapitalTurnoverRatio the workingCapitalTurnoverRatio to set
	 */
	public void setWorkingCapitalTurnoverRatio(double workingCapitalTurnoverRatio) {
		this.workingCapitalTurnoverRatio = workingCapitalTurnoverRatio;
	}

	/**
	 * @return the inventoryTurnoverRatio
	 */
	public double getInventoryTurnoverRatio() {
		return inventoryTurnoverRatio;
	}

	/**
	 * @param inventoryTurnoverRatio the inventoryTurnoverRatio to set
	 */
	public void setInventoryTurnoverRatio(double inventoryTurnoverRatio) {
		this.inventoryTurnoverRatio = inventoryTurnoverRatio;
	}

	/**
	 * @return the inventoryNoOfDays
	 */
	public double getInventoryNoOfDays() {
		return inventoryNoOfDays;
	}

	/**
	 * @param inventoryNoOfDays the inventoryNoOfDays to set
	 */
	public void setInventoryNoOfDays(double inventoryNoOfDays) {
		this.inventoryNoOfDays = inventoryNoOfDays;
	}

	/**
	 * @return the accountsReceivableTurnoverRatio
	 */
	public double getAccountsReceivableTurnoverRatio() {
		return accountsReceivableTurnoverRatio;
	}

	/**
	 * @param accountsReceivableTurnoverRatio the accountsReceivableTurnoverRatio to
	 *                                        set
	 */
	public void setAccountsReceivableTurnoverRatio(double accountsReceivableTurnoverRatio) {
		this.accountsReceivableTurnoverRatio = accountsReceivableTurnoverRatio;
	}

	/**
	 * @return the daysSalesOutstanding
	 */
	public double getDaysSalesOutstanding() {
		return daysSalesOutstanding;
	}

	/**
	 * @param daysSalesOutstanding the daysSalesOutstanding to set
	 */
	public void setDaysSalesOutstanding(double daysSalesOutstanding) {
		this.daysSalesOutstanding = daysSalesOutstanding;
	}

	/**
	 * @return the salesPerShare
	 */
	public double getSalesPerShare() {
		return salesPerShare;
	}

	/**
	 * @param salesPerShare the salesPerShare to set
	 */
	public void setSalesPerShare(double salesPerShare) {
		this.salesPerShare = salesPerShare;
	}

	/**
	 * @return the bookValuePerShare
	 */
	public double getBookValuePerShare() {
		return bookValuePerShare;
	}

	/**
	 * @param bookValuePerShare the bookValuePerShare to set
	 */
	public void setBookValuePerShare(double bookValuePerShare) {
		this.bookValuePerShare = bookValuePerShare;
	}

	/**
	 * @return the earningsPerShare
	 */
	public double getEarningsPerShare() {
		return earningsPerShare;
	}

	/**
	 * @param earningsPerShare the earningsPerShare to set
	 */
	public void setEarningsPerShare(double earningsPerShare) {
		this.earningsPerShare = earningsPerShare;
	}

	/**
	 * @return the priceToSalesRatio
	 */
	public double getPriceToSalesRatio() {
		return priceToSalesRatio;
	}

	/**
	 * @param priceToSalesRatio the priceToSalesRatio to set
	 */
	public void setPriceToSalesRatio(double priceToSalesRatio) {
		this.priceToSalesRatio = priceToSalesRatio;
	}

	/**
	 * @return the priceToBookValueRatio
	 */
	public double getPriceToBookValueRatio() {
		return priceToBookValueRatio;
	}

	/**
	 * @param priceToBookValueRatio the priceToBookValueRatio to set
	 */
	public void setPriceToBookValueRatio(double priceToBookValueRatio) {
		this.priceToBookValueRatio = priceToBookValueRatio;
	}

	/**
	 * @return the priceToEarningsRatio
	 */
	public double getPriceToEarningsRatio() {
		return priceToEarningsRatio;
	}

	/**
	 * @param priceToEarningsRatio the priceToEarningsRatio to set
	 */
	public void setPriceToEarningsRatio(double priceToEarningsRatio) {
		this.priceToEarningsRatio = priceToEarningsRatio;
	}

	@Override
	public String toString() {
		return "AnnuallyAnalyzedDetails [analysisYear=" + analysisYear + ", grossProfitMarginCheck="
				+ grossProfitMarginCheck + ", EBITDAMarginCheck=" + EBITDAMarginCheck + ", profitAfterTaxMarginCheck="
				+ profitAfterTaxMarginCheck + ", returnOnEquityCheck=" + returnOnEquityCheck + ", returnOnAssetsCheck="
				+ returnOnAssetsCheck + ", returnOnCapitalEmployedCheck=" + returnOnCapitalEmployedCheck
				+ ", interestCoverageRatioCheck=" + interestCoverageRatioCheck + ", debtToEquityRatioCheck="
				+ debtToEquityRatioCheck + ", debtToAssetsRatioCheck=" + debtToAssetsRatioCheck
				+ ", financialLeverageRatioCheck=" + financialLeverageRatioCheck + ", fixedAssetsTurnoverRatioCheck="
				+ fixedAssetsTurnoverRatioCheck + ", totalAssetsTurnoverRatioCheck=" + totalAssetsTurnoverRatioCheck
				+ ", workingCapitalTurnoverRatioCheck=" + workingCapitalTurnoverRatioCheck + ", priceToSalesRatioCheck="
				+ priceToSalesRatioCheck + ", priceToBookRatioCheck=" + priceToBookRatioCheck
				+ ", priceToEarningsRatioCheck=" + priceToEarningsRatioCheck + ", grossProfit=" + grossProfit
				+ ", earningsBfrIncomeTaxDepriAndAmor=" + earningsBfrIncomeTaxDepriAndAmor + ", profitBeforeTax="
				+ profitBeforeTax + ", profitAfterTax=" + profitAfterTax + ", totalAssets=" + totalAssets
				+ ", cashFlow=" + cashFlow + ", cashAndCashEquivalentAtYearEnding=" + cashAndCashEquivalentAtYearEnding
				+ ", grossProfitMargin=" + grossProfitMargin + ", earningsBfrIncomeTaxDepriAndAmorMargin="
				+ earningsBfrIncomeTaxDepriAndAmorMargin + ", profitAfterTaxMargin=" + profitAfterTaxMargin
				+ ", netProfitMargin=" + netProfitMargin + ", returnOnEquity=" + returnOnEquity + ", returnOnAssets="
				+ returnOnAssets + ", returnOnCapitalEmployed=" + returnOnCapitalEmployed + ", interestCoverageRatio="
				+ interestCoverageRatio + ", debtToEquityRatio=" + debtToEquityRatio + ", debtToAssestRatio="
				+ debtToAssestRatio + ", financialLeverageRatio=" + financialLeverageRatio
				+ ", fixedAssetsTurnoverRatio=" + fixedAssetsTurnoverRatio + ", totalAssetsTurnoverRatio="
				+ totalAssetsTurnoverRatio + ", workingCapitalTurnoverRatio=" + workingCapitalTurnoverRatio
				+ ", inventoryTurnoverRatio=" + inventoryTurnoverRatio + ", inventoryNoOfDays=" + inventoryNoOfDays
				+ ", accountsReceivableTurnoverRatio=" + accountsReceivableTurnoverRatio + ", daysSalesOutstanding="
				+ daysSalesOutstanding + ", salesPerShare=" + salesPerShare + ", bookValuePerShare=" + bookValuePerShare
				+ ", earningsPerShare=" + earningsPerShare + ", priceToSalesRatio=" + priceToSalesRatio
				+ ", priceToBookValueRatio=" + priceToBookValueRatio + ", priceToEarningsRatio=" + priceToEarningsRatio
				+ "]";
	}

}
