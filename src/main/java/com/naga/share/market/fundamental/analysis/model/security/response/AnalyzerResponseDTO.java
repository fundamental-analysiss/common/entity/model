package com.naga.share.market.fundamental.analysis.model.security.response;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnalyzerResponseDTO {

	private String isValidAnalyzerId;

	private String isValidEmailId;

	private String isValidOneTimePassword;

	private String isValidDOB;

	private String isNewPasswordGenerated;

	private String isAnalyzerAdded;

	private String isAnalyzerUpdated;

	private String isAnalyzerDeleted;

	private String isAnalyzerRoleUpdated;

	private String isOneTimePasswordGenerated;

	private String isMailSent;

	public String getIsMailSent() {
		return isMailSent;
	}

	public void setIsMailSent(String ismailSent) {
		this.isMailSent = ismailSent;
	}

	public String getIsValidAnalyzerId() {
		return isValidAnalyzerId;
	}

	public void setIsValidAnalyzerId(String isValidAnalyzerId) {
		this.isValidAnalyzerId = isValidAnalyzerId;
	}

	public String getIsValidEmailId() {
		return isValidEmailId;
	}

	public void setIsValidEmailId(String isValidEmailId) {
		this.isValidEmailId = isValidEmailId;
	}

	public String getIsValidOneTimePassword() {
		return isValidOneTimePassword;
	}

	public void setIsValidOneTimePassword(String isValidOneTimePassword) {
		this.isValidOneTimePassword = isValidOneTimePassword;
	}

	public String getIsValidDOB() {
		return isValidDOB;
	}

	public void setIsValidDOB(String isValidDOB) {
		this.isValidDOB = isValidDOB;
	}

	public String getIsNewPasswordGenerated() {
		return isNewPasswordGenerated;
	}

	public void setIsNewPasswordGenerated(String isNewPasswordGenerated) {
		this.isNewPasswordGenerated = isNewPasswordGenerated;
	}

	public String getIsAnalyzerAdded() {
		return isAnalyzerAdded;
	}

	public void setIsAnalyzerAdded(String isAnalyzerAdded) {
		this.isAnalyzerAdded = isAnalyzerAdded;
	}

	public String getIsAnalyzerUpdated() {
		return isAnalyzerUpdated;
	}

	public void setIsAnalyzerUpdated(String isAnalyzerUpdated) {
		this.isAnalyzerUpdated = isAnalyzerUpdated;
	}

	public String getIsAnalyzerDeleted() {
		return isAnalyzerDeleted;
	}

	public void setIsAnalyzerDeleted(String isAnalyzerDeleted) {
		this.isAnalyzerDeleted = isAnalyzerDeleted;
	}

	public String getIsAnalyzerRoleUpdated() {
		return isAnalyzerRoleUpdated;
	}

	public void setIsAnalyzerRoleUpdated(String isAnalyzerRoleUpdated) {
		this.isAnalyzerRoleUpdated = isAnalyzerRoleUpdated;
	}

	public String getIsOneTimePasswordGenerated() {
		return isOneTimePasswordGenerated;
	}

	public void setIsOneTimePasswordGenerated(String isOneTimePasswordGenerated) {
		this.isOneTimePasswordGenerated = isOneTimePasswordGenerated;
	}

}
