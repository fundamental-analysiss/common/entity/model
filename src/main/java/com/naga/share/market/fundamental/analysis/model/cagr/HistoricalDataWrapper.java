package com.naga.share.market.fundamental.analysis.model.cagr;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalDataWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1148512282150413294L;

	@JsonProperty("historicalData")
	private List<HistoricalData> historicalData;

	/**
	 * @return the historicalData
	 */
	public List<HistoricalData> getHistoricalData() {
		return historicalData;
	}

	/**
	 * @param historicalData the historicalData to set
	 */
	public void setHistoricalData(List<HistoricalData> historicalData) {
		this.historicalData = historicalData;
	}

}
