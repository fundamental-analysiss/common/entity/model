package com.naga.share.market.fundamental.analysis.model.industry.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;

/**
 * @author Nagaaswin S
 *
 */
@NamedQueries(value = { @NamedQuery(name = "industryFindAllIndustries", query = "FROM Industry"),
		@NamedQuery(name = "industryFindByIndustryName", query = "FROM Industry industry WHERE industry.industryName = :industryName") })
@Entity
public class Industry {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Sector sector;

	@Column(nullable = false, unique = true)
	private String industryName;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(nullable = false)
	private String lastUpdatedBy;

	@Column(nullable = false)
	@UpdateTimestamp
	private LocalDateTime lastUpdatedOn;

	@OneToMany(mappedBy = "industry", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Company> company = new ArrayList<Company>();

	@OneToMany(mappedBy = "industry", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Annual> annual = new ArrayList<Annual>();

	@OneToMany(mappedBy = "industry", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompoundedAnnualGrowthRate> compoundedAnnualGrowthRate = new ArrayList<CompoundedAnnualGrowthRate>();

	public Industry() {
		super();
	}

	public void addCompany(Company company) {
		this.company.add(company);
	}

	public void removeCompany(Company company) {
		this.company.remove(company);
	}

	public void addAnnual(Annual annual) {
		this.annual.add(annual);
	}

	public void removeAnnual(Annual annual) {
		this.annual.remove(annual);
	}

	public List<Company> getCompany() {
		return company;
	}

	public List<Annual> getAnnual() {
		return annual;
	}

	public List<CompoundedAnnualGrowthRate> getCompoundedAnnualGrowthRate() {
		return compoundedAnnualGrowthRate;
	}

	public void addCompoundedAnnualGrowthRate(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		this.compoundedAnnualGrowthRate.add(compoundedAnnualGrowthRate);
	}

	public void removeCompoundedAnnualGrowthRate(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		this.compoundedAnnualGrowthRate.remove(compoundedAnnualGrowthRate);
	}

	public long getId() {
		return id;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public LocalDateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	@Override
	public String toString() {
		return "Industry [id=" + id + ", sector=" + sector + ", industryName=" + industryName + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedOn="
				+ lastUpdatedOn + ", company=" + company + ", annual=" + annual + ", compoundedAnnualGrowthRate="
				+ compoundedAnnualGrowthRate + "]";
	}

}
