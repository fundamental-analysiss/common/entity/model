package com.naga.share.market.fundamental.analysis.model.security.entity;

import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

/**
 * @author Nagaaswin S
 *
 */
@Entity
@Table(name = "authentication")
@NamedQueries(value = {
		@NamedQuery(name = "AuthFindByAnalyzerId", query = "FROM Authentication authentication WHERE authentication.analyzerId = :analyzerId") })
public class Authentication {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, unique = true)
	private String analyzerId;

	@Column(nullable = false)
	private String emailId;

	@Column(nullable = false)
	private String oneTimePassword;

	@Column(nullable = false)
	private LocalTime oneTimePassExpiration;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	public Authentication() {
	}

	public Authentication(String analyzerId, String emailId, String oneTimePassword, LocalTime oneTimePassExpiration) {
		this.analyzerId = analyzerId;
		this.emailId = emailId;
		this.oneTimePassword = oneTimePassword;
		this.oneTimePassExpiration = oneTimePassExpiration;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(String analyzerId) {
		this.analyzerId = analyzerId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

	public LocalTime getOneTimePassExpiration() {
		return oneTimePassExpiration;
	}

	public void setOneTimePassExpiration(LocalTime oneTimePassExpiration) {
		this.oneTimePassExpiration = oneTimePassExpiration;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	@Override
	public String toString() {
		return "Authentication [id=" + id + ", analyzerId=" + analyzerId + ", emailId=" + emailId + ", oneTimePassword="
				+ oneTimePassword + ", oneTimePassExpiration=" + oneTimePassExpiration + ", createdOn=" + createdOn
				+ "]";
	}

}
