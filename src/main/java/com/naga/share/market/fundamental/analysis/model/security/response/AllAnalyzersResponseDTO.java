package com.naga.share.market.fundamental.analysis.model.security.response;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AllAnalyzersResponseDTO {

	private List<AllAnalyzers> allAnalyzers;

	public List<AllAnalyzers> getAllAnalyzers() {
		return allAnalyzers;
	}

	public void setAllAnalyzers(List<AllAnalyzers> allAnalyzers) {
		this.allAnalyzers = allAnalyzers;
	}

}
