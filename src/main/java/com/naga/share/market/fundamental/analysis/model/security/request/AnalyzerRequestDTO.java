package com.naga.share.market.fundamental.analysis.model.security.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnalyzerRequestDTO {

	@NotBlank
	@Pattern(regexp = "[A-Za-z0-9 ]*$")
	@Size(min = 1, max = 50)
	private String analyzerId;

	@Size(min = 1, max = 50)
	private String password;

	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String firstName;

	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String lastName;

	@Email
	private String emailId;

	@Pattern(regexp = "[0-9\\-]*$")
	@Size(min = 10, max = 10)
	private String dateOfBirth;

	@Size(min = 7, max = 10)
	private String oneTimePassword;

	public AnalyzerRequestDTO() {
		super();
	}

	public AnalyzerRequestDTO(String analyzerId, String password, String firstName, String lastName, String emailId,
			String dateOfBirth, String oneTimePassword) {
		super();
		this.analyzerId = analyzerId;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.dateOfBirth = dateOfBirth;
		this.oneTimePassword = oneTimePassword;
	}

	public String getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(String analyzerId) {
		this.analyzerId = analyzerId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

}
