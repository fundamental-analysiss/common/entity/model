package com.naga.share.market.fundamental.analysis.model.cagr.response;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.naga.share.market.fundamental.analysis.model.company.response.CompanyResponseDTO;
import com.naga.share.market.fundamental.analysis.model.industry.response.IndustryResponseDTO;
import com.naga.share.market.fundamental.analysis.model.sector.response.SectorResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CagrResponseDTO {

	private SectorResponseDTO sector;

	private IndustryResponseDTO industry;

	private CompanyResponseDTO company;

	private CagrAnalyzedDetails cagrAnalyzedDetails;

	private List<CagrAnalyzedYears> cagrAnalyzedYears;

	/**
	 * @return the sector
	 */
	public SectorResponseDTO getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(SectorResponseDTO sector) {
		this.sector = sector;
	}

	/**
	 * @return the industry
	 */
	public IndustryResponseDTO getIndustry() {
		return industry;
	}

	/**
	 * @param industry the industry to set
	 */
	public void setIndustry(IndustryResponseDTO industry) {
		this.industry = industry;
	}

	/**
	 * @return the company
	 */
	public CompanyResponseDTO getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(CompanyResponseDTO company) {
		this.company = company;
	}

	/**
	 * @return the cagrAnalyzedDetails
	 */
	public CagrAnalyzedDetails getCagrAnalyzedDetails() {
		return cagrAnalyzedDetails;
	}

	/**
	 * @param cagrAnalyzedDetails the cagrAnalyzedDetails to set
	 */
	public void setCagrAnalyzedDetails(CagrAnalyzedDetails cagrAnalyzedDetails) {
		this.cagrAnalyzedDetails = cagrAnalyzedDetails;
	}

	/**
	 * @return the cagrAnalyzedYears
	 */
	public List<CagrAnalyzedYears> getCagrAnalyzedYears() {
		return cagrAnalyzedYears;
	}

	/**
	 * @param cagrAnalyzedYears the cagrAnalyzedYears to set
	 */
	public void setCagrAnalyzedYears(List<CagrAnalyzedYears> cagrAnalyzedYears) {
		this.cagrAnalyzedYears = cagrAnalyzedYears;
	}

	@Override
	public String toString() {
		return "CagrResponseDTO [sector=" + sector + ", industry=" + industry + ", company=" + company
				+ ", cagrAnalyzedDetails=" + cagrAnalyzedDetails + "]";
	}

}
