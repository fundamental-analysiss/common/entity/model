package com.naga.share.market.fundamental.analysis.model.cagr.response;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CagrAnalyzedYears {

	private int cagrStartYear;

	private int cagrEndYear;

	/**
	 * @return the cagrStartYear
	 */
	public int getCagrStartYear() {
		return cagrStartYear;
	}

	/**
	 * @param cagrStartYear the cagrStartYear to set
	 */
	public void setCagrStartYear(int cagrStartYear) {
		this.cagrStartYear = cagrStartYear;
	}

	/**
	 * @return the cagrEndYear
	 */
	public int getCagrEndYear() {
		return cagrEndYear;
	}

	/**
	 * @param cagrEndYear the cagrEndYear to set
	 */
	public void setCagrEndYear(int cagrEndYear) {
		this.cagrEndYear = cagrEndYear;
	}

}
