package com.naga.share.market.fundamental.analysis.model.cagr.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CagrRequestDTO {

	@NotBlank
	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String companyName;

	@NotNull
	@Digits(integer = 4, fraction = 0)
	private int analysisStartYear;

	@NotNull
	@Digits(integer = 4, fraction = 0)
	private int analysisEndYear;

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the analysisStartYear
	 */
	public int getAnalysisStartYear() {
		return analysisStartYear;
	}

	/**
	 * @param analysisStartYear the analysisStartYear to set
	 */
	public void setAnalysisStartYear(int analysisStartYear) {
		this.analysisStartYear = analysisStartYear;
	}

	/**
	 * @return the analysisEndYear
	 */
	public int getAnalysisEndYear() {
		return analysisEndYear;
	}

	/**
	 * @param analysisEndYear the analysisEndYear to set
	 */
	public void setAnalysisEndYear(int analysisEndYear) {
		this.analysisEndYear = analysisEndYear;
	}

}
