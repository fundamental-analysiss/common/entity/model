package com.naga.share.market.fundamental.analysis.model.exceptions;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nagaaswin S
 *
 */
public class ExceptionModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String exceptionCode;
	private String exceptionDescription;
	private String exceptionType;
	private List<ExceptionModel> subExceptions;

	public ExceptionModel(String exceptionCode) {
		super();
		this.exceptionCode = exceptionCode;
	}

	public ExceptionModel(String exceptionCode, String exceptionDescription) {
		super();
		this.exceptionCode = exceptionCode;
		this.exceptionDescription = exceptionDescription;
	}

	public ExceptionModel(String exceptionCode, String exceptionDescription, String exceptionType) {
		super();
		this.exceptionCode = exceptionCode;
		this.exceptionDescription = exceptionDescription;
		this.exceptionType = exceptionType;
	}

	public ExceptionModel(String exceptionCode, List<ExceptionModel> subExceptions) {
		super();
		this.exceptionCode = exceptionCode;
		this.subExceptions = subExceptions;
	}

	public ExceptionModel(String exceptionCode, String exceptionType, List<ExceptionModel> subExceptions) {
		super();
		this.exceptionCode = exceptionCode;
		this.exceptionType = exceptionType;
		this.subExceptions = subExceptions;
	}

	public String getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(String exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public String getExceptionDescription() {
		return exceptionDescription;
	}

	public void setExceptionDescription(String exceptionDescription) {
		this.exceptionDescription = exceptionDescription;
	}

	public String getExceptionType() {
		return exceptionType;
	}

	public void setExceptionType(String exceptionType) {
		this.exceptionType = exceptionType;
	}

	public List<ExceptionModel> getSubExceptions() {
		return subExceptions;
	}

	public void setSubExceptions(List<ExceptionModel> subExceptions) {
		this.subExceptions = subExceptions;
	}

	@Override
	public String toString() {
		return "ExceptionModel [exceptionCode=" + exceptionCode + ", exceptionDescription=" + exceptionDescription
				+ ", exceptionType=" + exceptionType + ", subExceptions=" + subExceptions + "]";
	}

}
