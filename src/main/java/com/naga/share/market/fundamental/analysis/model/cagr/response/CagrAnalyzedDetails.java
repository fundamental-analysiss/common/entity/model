package com.naga.share.market.fundamental.analysis.model.cagr.response;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.naga.share.market.fundamental.analysis.model.cagr.HistoricalData;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CagrAnalyzedDetails {

	private int cagrStartYear;

	private int cagrEndYear;

	private double netFreeCashflow;

	private double averageFreeCashFlow;

	private double totalRevenue;

	private double netSales;

	private double financeCost;

	private double sharePrice;

	private double shareholdersFund;

	private double inventory;

	private double tradeReceivables;

	private double cashFromOperatingActivities;

	private double cashEquivalentAtYearEnding;

	private double totalDebt;

	private double operatingRevenue;

	private double grossProfit;

	private double earningsBfrIncomeTaxDepriAndAmor;

	private double earningsBeforeIncomeTax;

	private double profitAfterTax;

	private double salesPerShare;

	private double bookValuePerShare;

	private double earningsPerShare;

	private double netDebt;

	private double totalPresentValueOfFreeFlowCash;

	private double internsicValueOfSharePrice;

	private double lowerInternsicValueOfSharePrice;

	private double upperInternsicValueOfSharePrice;

	private double marginOfSafety;

	private double terminalValue;

	private double netPresentValue;

	private double presentValueOfTerminalValue;

	private String totalRevenueInLineWithProfitAfterTaxCheck;

	private String earningsPerShareGrowthInLineWithProfitAfterTaxCheck;

	private String grossProfitMarginCheck;

	private String debtLevelCheck;

	private String inventoryGrowthInLineWithProfitAfterTaxCheck;

	private String tradeReceivablesToNetSalesCheck;

	private String cashFlowFromOperationsCheck;

	private String returnOnEquityCheck;

	private List<HistoricalData> totalRevenueGrowth;

	private List<HistoricalData> futureCashFlowValue;

	private List<HistoricalData> presentFutureCashFlowValue;

	private List<HistoricalData> profitAfterTaxGrowth;

	private List<HistoricalData> salesPerShareGrowth;

	private List<HistoricalData> bookValuePerShareGrowth;

	private List<HistoricalData> earningsPerShareGrowth;

	private List<HistoricalData> grossProfitGrowth;

	private List<HistoricalData> inventoryGrowth;

	private List<HistoricalData> netSalesGrowth;

	private List<HistoricalData> cashFlowFromOperationsGrowth;

	private List<HistoricalData> cashAndCashEquivalentGrowth;

	private List<HistoricalData> shareholdersFundGrowth;

	private List<HistoricalData> operatingRevenueGrowth;

	private List<HistoricalData> sharePriceGrowth;

	private List<HistoricalData> historicalTotalRevenue;

	private List<HistoricalData> historicalProfitAfterTax;

	private List<HistoricalData> historicalProfitAfterTaxMargin;

	private List<HistoricalData> historicalSalesPerShare;

	private List<HistoricalData> historicalBookValuePerShare;

	private List<HistoricalData> historicalEarningsPerShare;

	private List<HistoricalData> historicalGrossProfit;

	private List<HistoricalData> historicalGrossProfitMargin;

	private List<HistoricalData> historicalTotalDebt;

	private List<HistoricalData> historicalEarningsBeforeIncTax;

	private List<HistoricalData> historicalTotalDebtByEarningsBfrIncTax;

	private List<HistoricalData> historicalInventory;

	private List<HistoricalData> historicalInventoryNoOfDays;

	private List<HistoricalData> historicalNetSales;

	private List<HistoricalData> historicalTradeReceivables;

	private List<HistoricalData> historicalTradeRecievablesByNetSales;

	private List<HistoricalData> historicalCashFlowFromOperations;

	private List<HistoricalData> historicalCashAndCashEquivalent;

	private List<HistoricalData> historicalShareholdersFund;

	private List<HistoricalData> historicalReturnOnEquity;

	private List<HistoricalData> historicalOperatingRevenue;

	private List<HistoricalData> historicalSharePrice;

	/**
	 * @return the futureCashFlowValue
	 */
	public List<HistoricalData> getFutureCashFlowValue() {
		return futureCashFlowValue;
	}

	/**
	 * @param futureCashFlowValue the futureCashFlowValue to set
	 */
	public void setFutureCashFlowValue(List<HistoricalData> futureCashFlowValue) {
		this.futureCashFlowValue = futureCashFlowValue;
	}

	/**
	 * @return the presentFutureCashFlowValue
	 */
	public List<HistoricalData> getPresentFutureCashFlowValue() {
		return presentFutureCashFlowValue;
	}

	/**
	 * @param presentFutureCashFlowValue the presentFutureCashFlowValue to set
	 */
	public void setPresentFutureCashFlowValue(List<HistoricalData> presentFutureCashFlowValue) {
		this.presentFutureCashFlowValue = presentFutureCashFlowValue;
	}

	/**
	 * @return the cagrStartYear
	 */
	public int getCagrStartYear() {
		return cagrStartYear;
	}

	/**
	 * @param cagrStartYear the cagrStartYear to set
	 */
	public void setCagrStartYear(int cagrStartYear) {
		this.cagrStartYear = cagrStartYear;
	}

	/**
	 * @return the cagrEndYear
	 */
	public int getCagrEndYear() {
		return cagrEndYear;
	}

	/**
	 * @param cagrEndYear the cagrEndYear to set
	 */
	public void setCagrEndYear(int cagrEndYear) {
		this.cagrEndYear = cagrEndYear;
	}

	/**
	 * @return the netFreeCashflow
	 */
	public double getNetFreeCashflow() {
		return netFreeCashflow;
	}

	/**
	 * @param netFreeCashflow the netFreeCashflow to set
	 */
	public void setNetFreeCashflow(double netFreeCashflow) {
		this.netFreeCashflow = netFreeCashflow;
	}

	/**
	 * @return the averageFreeCashFlow
	 */
	public double getAverageFreeCashFlow() {
		return averageFreeCashFlow;
	}

	/**
	 * @param averageFreeCashFlow the averageFreeCashFlow to set
	 */
	public void setAverageFreeCashFlow(double averageFreeCashFlow) {
		this.averageFreeCashFlow = averageFreeCashFlow;
	}

	/**
	 * @return the totalRevenue
	 */
	public double getTotalRevenue() {
		return totalRevenue;
	}

	/**
	 * @param totalRevenue the totalRevenue to set
	 */
	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * @return the netSales
	 */
	public double getNetSales() {
		return netSales;
	}

	/**
	 * @param netSales the netSales to set
	 */
	public void setNetSales(double netSales) {
		this.netSales = netSales;
	}

	/**
	 * @return the financeCost
	 */
	public double getFinanceCost() {
		return financeCost;
	}

	/**
	 * @param financeCost the financeCost to set
	 */
	public void setFinanceCost(double financeCost) {
		this.financeCost = financeCost;
	}

	/**
	 * @return the sharePrice
	 */
	public double getSharePrice() {
		return sharePrice;
	}

	/**
	 * @param sharePrice the sharePrice to set
	 */
	public void setSharePrice(double sharePrice) {
		this.sharePrice = sharePrice;
	}

	/**
	 * @return the shareholdersFund
	 */
	public double getShareholdersFund() {
		return shareholdersFund;
	}

	/**
	 * @param shareholdersFund the shareholdersFund to set
	 */
	public void setShareholdersFund(double shareholdersFund) {
		this.shareholdersFund = shareholdersFund;
	}

	/**
	 * @return the inventory
	 */
	public double getInventory() {
		return inventory;
	}

	/**
	 * @param inventory the inventory to set
	 */
	public void setInventory(double inventory) {
		this.inventory = inventory;
	}

	/**
	 * @return the tradeReceivables
	 */
	public double getTradeReceivables() {
		return tradeReceivables;
	}

	/**
	 * @param tradeReceivables the tradeReceivables to set
	 */
	public void setTradeReceivables(double tradeReceivables) {
		this.tradeReceivables = tradeReceivables;
	}

	/**
	 * @return the cashFromOperatingActivities
	 */
	public double getCashFromOperatingActivities() {
		return cashFromOperatingActivities;
	}

	/**
	 * @param cashFromOperatingActivities the cashFromOperatingActivities to set
	 */
	public void setCashFromOperatingActivities(double cashFromOperatingActivities) {
		this.cashFromOperatingActivities = cashFromOperatingActivities;
	}

	/**
	 * @return the cashEquivalentAtYearEnding
	 */
	public double getCashEquivalentAtYearEnding() {
		return cashEquivalentAtYearEnding;
	}

	/**
	 * @param cashEquivalentAtYearEnding the cashEquivalentAtYearEnding to set
	 */
	public void setCashEquivalentAtYearEnding(double cashEquivalentAtYearEnding) {
		this.cashEquivalentAtYearEnding = cashEquivalentAtYearEnding;
	}

	/**
	 * @return the totalDebt
	 */
	public double getTotalDebt() {
		return totalDebt;
	}

	/**
	 * @param totalDebt the totalDebt to set
	 */
	public void setTotalDebt(double totalDebt) {
		this.totalDebt = totalDebt;
	}

	/**
	 * @return the operatingRevenue
	 */
	public double getOperatingRevenue() {
		return operatingRevenue;
	}

	/**
	 * @param operatingRevenue the operatingRevenue to set
	 */
	public void setOperatingRevenue(double operatingRevenue) {
		this.operatingRevenue = operatingRevenue;
	}

	/**
	 * @return the grossProfit
	 */
	public double getGrossProfit() {
		return grossProfit;
	}

	/**
	 * @param grossProfit the grossProfit to set
	 */
	public void setGrossProfit(double grossProfit) {
		this.grossProfit = grossProfit;
	}

	/**
	 * @return the earningsBfrIncomeTaxDepriAndAmor
	 */
	public double getEarningsBfrIncomeTaxDepriAndAmor() {
		return earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @param earningsBfrIncomeTaxDepriAndAmor the earningsBfrIncomeTaxDepriAndAmor
	 *                                         to set
	 */
	public void setEarningsBfrIncomeTaxDepriAndAmor(double earningsBfrIncomeTaxDepriAndAmor) {
		this.earningsBfrIncomeTaxDepriAndAmor = earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @return the presentValueOfTerminalValue
	 */
	public double getPresentValueOfTerminalValue() {
		return presentValueOfTerminalValue;
	}

	/**
	 * @param presentValueOfTerminalValue the presentValueOfTerminalValue to set
	 */
	public void setPresentValueOfTerminalValue(double presentValueOfTerminalValue) {
		this.presentValueOfTerminalValue = presentValueOfTerminalValue;
	}

	/**
	 * @return the earningsBeforeIncomeTax
	 */
	public double getEarningsBeforeIncomeTax() {
		return earningsBeforeIncomeTax;
	}

	/**
	 * @param earningsBeforeIncomeTax the earningsBeforeIncomeTax to set
	 */
	public void setEarningsBeforeIncomeTax(double earningsBeforeIncomeTax) {
		this.earningsBeforeIncomeTax = earningsBeforeIncomeTax;
	}

	/**
	 * @return the profitAfterTax
	 */
	public double getProfitAfterTax() {
		return profitAfterTax;
	}

	/**
	 * @param profitAfterTax the profitAfterTax to set
	 */
	public void setProfitAfterTax(double profitAfterTax) {
		this.profitAfterTax = profitAfterTax;
	}

	/**
	 * @return the salesPerShare
	 */
	public double getSalesPerShare() {
		return salesPerShare;
	}

	/**
	 * @param salesPerShare the salesPerShare to set
	 */
	public void setSalesPerShare(double salesPerShare) {
		this.salesPerShare = salesPerShare;
	}

	/**
	 * @return the bookValuePerShare
	 */
	public double getBookValuePerShare() {
		return bookValuePerShare;
	}

	/**
	 * @param bookValuePerShare the bookValuePerShare to set
	 */
	public void setBookValuePerShare(double bookValuePerShare) {
		this.bookValuePerShare = bookValuePerShare;
	}

	/**
	 * @return the earningsPerShare
	 */
	public double getEarningsPerShare() {
		return earningsPerShare;
	}

	/**
	 * @param earningsPerShare the earningsPerShare to set
	 */
	public void setEarningsPerShare(double earningsPerShare) {
		this.earningsPerShare = earningsPerShare;
	}

	/**
	 * @return the netDebt
	 */
	public double getNetDebt() {
		return netDebt;
	}

	/**
	 * @return the terminalValue
	 */
	public double getTerminalValue() {
		return terminalValue;
	}

	/**
	 * @param terminalValue the terminalValue to set
	 */
	public void setTerminalValue(double terminalValue) {
		this.terminalValue = terminalValue;
	}

	/**
	 * @return the netPresentValue
	 */
	public double getNetPresentValue() {
		return netPresentValue;
	}

	/**
	 * @param netPresentValue the netPresentValue to set
	 */
	public void setNetPresentValue(double netPresentValue) {
		this.netPresentValue = netPresentValue;
	}

	/**
	 * @param netDebt the netDebt to set
	 */
	public void setNetDebt(double netDebt) {
		this.netDebt = netDebt;
	}

	/**
	 * @return the totalPresentValueOfFreeFlowCash
	 */
	public double getTotalPresentValueOfFreeFlowCash() {
		return totalPresentValueOfFreeFlowCash;
	}

	/**
	 * @param totalPresentValueOfFreeFlowCash the totalPresentValueOfFreeFlowCash to
	 *                                        set
	 */
	public void setTotalPresentValueOfFreeFlowCash(double totalPresentValueOfFreeFlowCash) {
		this.totalPresentValueOfFreeFlowCash = totalPresentValueOfFreeFlowCash;
	}

	/**
	 * @return the internsicValueOfSharePrice
	 */
	public double getInternsicValueOfSharePrice() {
		return internsicValueOfSharePrice;
	}

	/**
	 * @param internsicValueOfSharePrice the internsicValueOfSharePrice to set
	 */
	public void setInternsicValueOfSharePrice(double internsicValueOfSharePrice) {
		this.internsicValueOfSharePrice = internsicValueOfSharePrice;
	}

	/**
	 * @return the lowerInternsicValueOfSharePrice
	 */
	public double getLowerInternsicValueOfSharePrice() {
		return lowerInternsicValueOfSharePrice;
	}

	/**
	 * @param lowerInternsicValueOfSharePrice the lowerInternsicValueOfSharePrice to
	 *                                        set
	 */
	public void setLowerInternsicValueOfSharePrice(double lowerInternsicValueOfSharePrice) {
		this.lowerInternsicValueOfSharePrice = lowerInternsicValueOfSharePrice;
	}

	/**
	 * @return the upperInternsicValueOfSharePrice
	 */
	public double getUpperInternsicValueOfSharePrice() {
		return upperInternsicValueOfSharePrice;
	}

	/**
	 * @param upperInternsicValueOfSharePrice the upperInternsicValueOfSharePrice to
	 *                                        set
	 */
	public void setUpperInternsicValueOfSharePrice(double upperInternsicValueOfSharePrice) {
		this.upperInternsicValueOfSharePrice = upperInternsicValueOfSharePrice;
	}

	/**
	 * @return the marginOfSafety
	 */
	public double getMarginOfSafety() {
		return marginOfSafety;
	}

	/**
	 * @param marginOfSafety the marginOfSafety to set
	 */
	public void setMarginOfSafety(double marginOfSafety) {
		this.marginOfSafety = marginOfSafety;
	}

	/**
	 * @return the totalRevenueInLineWithProfitAfterTaxCheck
	 */
	public String getTotalRevenueInLineWithProfitAfterTaxCheck() {
		return totalRevenueInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @param totalRevenueInLineWithProfitAfterTaxCheck the
	 *                                                  totalRevenueInLineWithProfitAfterTaxCheck
	 *                                                  to set
	 */
	public void setTotalRevenueInLineWithProfitAfterTaxCheck(String totalRevenueInLineWithProfitAfterTaxCheck) {
		this.totalRevenueInLineWithProfitAfterTaxCheck = totalRevenueInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @return the earningsPerShareGrowthInLineWithProfitAfterTaxCheck
	 */
	public String getEarningsPerShareGrowthInLineWithProfitAfterTaxCheck() {
		return earningsPerShareGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @param earningsPerShareGrowthInLineWithProfitAfterTaxCheck the
	 *                                                            earningsPerShareGrowthInLineWithProfitAfterTaxCheck
	 *                                                            to set
	 */
	public void setEarningsPerShareGrowthInLineWithProfitAfterTaxCheck(
			String earningsPerShareGrowthInLineWithProfitAfterTaxCheck) {
		this.earningsPerShareGrowthInLineWithProfitAfterTaxCheck = earningsPerShareGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @return the grossProfitMarginCheck
	 */
	public String getGrossProfitMarginCheck() {
		return grossProfitMarginCheck;
	}

	/**
	 * @param grossProfitMarginCheck the grossProfitMarginCheck to set
	 */
	public void setGrossProfitMarginCheck(String grossProfitMarginCheck) {
		this.grossProfitMarginCheck = grossProfitMarginCheck;
	}

	/**
	 * @return the debtLevelCheck
	 */
	public String getDebtLevelCheck() {
		return debtLevelCheck;
	}

	/**
	 * @param debtLevelCheck the debtLevelCheck to set
	 */
	public void setDebtLevelCheck(String debtLevelCheck) {
		this.debtLevelCheck = debtLevelCheck;
	}

	/**
	 * @return the inventoryGrowthInLineWithProfitAfterTaxCheck
	 */
	public String getInventoryGrowthInLineWithProfitAfterTaxCheck() {
		return inventoryGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @param inventoryGrowthInLineWithProfitAfterTaxCheck the
	 *                                                     inventoryGrowthInLineWithProfitAfterTaxCheck
	 *                                                     to set
	 */
	public void setInventoryGrowthInLineWithProfitAfterTaxCheck(String inventoryGrowthInLineWithProfitAfterTaxCheck) {
		this.inventoryGrowthInLineWithProfitAfterTaxCheck = inventoryGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @return the tradeReceivablesToNetSalesCheck
	 */
	public String getTradeReceivablesToNetSalesCheck() {
		return tradeReceivablesToNetSalesCheck;
	}

	/**
	 * @param tradeReceivablesToNetSalesCheck the tradeReceivablesToNetSalesCheck to
	 *                                        set
	 */
	public void setTradeReceivablesToNetSalesCheck(String tradeReceivablesToNetSalesCheck) {
		this.tradeReceivablesToNetSalesCheck = tradeReceivablesToNetSalesCheck;
	}

	/**
	 * @return the cashFlowFromOperationsCheck
	 */
	public String getCashFlowFromOperationsCheck() {
		return cashFlowFromOperationsCheck;
	}

	/**
	 * @param cashFlowFromOperationsCheck the cashFlowFromOperationsCheck to set
	 */
	public void setCashFlowFromOperationsCheck(String cashFlowFromOperationsCheck) {
		this.cashFlowFromOperationsCheck = cashFlowFromOperationsCheck;
	}

	/**
	 * @return the returnOnEquityCheck
	 */
	public String getReturnOnEquityCheck() {
		return returnOnEquityCheck;
	}

	/**
	 * @param returnOnEquityCheck the returnOnEquityCheck to set
	 */
	public void setReturnOnEquityCheck(String returnOnEquityCheck) {
		this.returnOnEquityCheck = returnOnEquityCheck;
	}

	/**
	 * @return the totalRevenueGrowth
	 */
	public List<HistoricalData> getTotalRevenueGrowth() {
		return totalRevenueGrowth;
	}

	/**
	 * @param totalRevenueGrowth the totalRevenueGrowth to set
	 */
	public void setTotalRevenueGrowth(List<HistoricalData> totalRevenueGrowth) {
		this.totalRevenueGrowth = totalRevenueGrowth;
	}

	/**
	 * @return the profitAfterTaxGrowth
	 */
	public List<HistoricalData> getProfitAfterTaxGrowth() {
		return profitAfterTaxGrowth;
	}

	/**
	 * @param profitAfterTaxGrowth the profitAfterTaxGrowth to set
	 */
	public void setProfitAfterTaxGrowth(List<HistoricalData> profitAfterTaxGrowth) {
		this.profitAfterTaxGrowth = profitAfterTaxGrowth;
	}

	/**
	 * @return the salesPerShareGrowth
	 */
	public List<HistoricalData> getSalesPerShareGrowth() {
		return salesPerShareGrowth;
	}

	/**
	 * @param salesPerShareGrowth the salesPerShareGrowth to set
	 */
	public void setSalesPerShareGrowth(List<HistoricalData> salesPerShareGrowth) {
		this.salesPerShareGrowth = salesPerShareGrowth;
	}

	/**
	 * @return the bookValuePerShareGrowth
	 */
	public List<HistoricalData> getBookValuePerShareGrowth() {
		return bookValuePerShareGrowth;
	}

	/**
	 * @param bookValuePerShareGrowth the bookValuePerShareGrowth to set
	 */
	public void setBookValuePerShareGrowth(List<HistoricalData> bookValuePerShareGrowth) {
		this.bookValuePerShareGrowth = bookValuePerShareGrowth;
	}

	/**
	 * @return the earningsPerShareGrowth
	 */
	public List<HistoricalData> getEarningsPerShareGrowth() {
		return earningsPerShareGrowth;
	}

	/**
	 * @param earningsPerShareGrowth the earningsPerShareGrowth to set
	 */
	public void setEarningsPerShareGrowth(List<HistoricalData> earningsPerShareGrowth) {
		this.earningsPerShareGrowth = earningsPerShareGrowth;
	}

	/**
	 * @return the grossProfitGrowth
	 */
	public List<HistoricalData> getGrossProfitGrowth() {
		return grossProfitGrowth;
	}

	/**
	 * @param grossProfitGrowth the grossProfitGrowth to set
	 */
	public void setGrossProfitGrowth(List<HistoricalData> grossProfitGrowth) {
		this.grossProfitGrowth = grossProfitGrowth;
	}

	/**
	 * @return the inventoryGrowth
	 */
	public List<HistoricalData> getInventoryGrowth() {
		return inventoryGrowth;
	}

	/**
	 * @param inventoryGrowth the inventoryGrowth to set
	 */
	public void setInventoryGrowth(List<HistoricalData> inventoryGrowth) {
		this.inventoryGrowth = inventoryGrowth;
	}

	/**
	 * @return the netSalesGrowth
	 */
	public List<HistoricalData> getNetSalesGrowth() {
		return netSalesGrowth;
	}

	/**
	 * @param netSalesGrowth the netSalesGrowth to set
	 */
	public void setNetSalesGrowth(List<HistoricalData> netSalesGrowth) {
		this.netSalesGrowth = netSalesGrowth;
	}

	/**
	 * @return the cashFlowFromOperationsGrowth
	 */
	public List<HistoricalData> getCashFlowFromOperationsGrowth() {
		return cashFlowFromOperationsGrowth;
	}

	/**
	 * @param cashFlowFromOperationsGrowth the cashFlowFromOperationsGrowth to set
	 */
	public void setCashFlowFromOperationsGrowth(List<HistoricalData> cashFlowFromOperationsGrowth) {
		this.cashFlowFromOperationsGrowth = cashFlowFromOperationsGrowth;
	}

	/**
	 * @return the cashAndCashEquivalentGrowth
	 */
	public List<HistoricalData> getCashAndCashEquivalentGrowth() {
		return cashAndCashEquivalentGrowth;
	}

	/**
	 * @param cashAndCashEquivalentGrowth the cashAndCashEquivalentGrowth to set
	 */
	public void setCashAndCashEquivalentGrowth(List<HistoricalData> cashAndCashEquivalentGrowth) {
		this.cashAndCashEquivalentGrowth = cashAndCashEquivalentGrowth;
	}

	/**
	 * @return the shareholdersFundGrowth
	 */
	public List<HistoricalData> getShareholdersFundGrowth() {
		return shareholdersFundGrowth;
	}

	/**
	 * @param shareholdersFundGrowth the shareholdersFundGrowth to set
	 */
	public void setShareholdersFundGrowth(List<HistoricalData> shareholdersFundGrowth) {
		this.shareholdersFundGrowth = shareholdersFundGrowth;
	}

	/**
	 * @return the operatingRevenueGrowth
	 */
	public List<HistoricalData> getOperatingRevenueGrowth() {
		return operatingRevenueGrowth;
	}

	/**
	 * @param operatingRevenueGrowth the operatingRevenueGrowth to set
	 */
	public void setOperatingRevenueGrowth(List<HistoricalData> operatingRevenueGrowth) {
		this.operatingRevenueGrowth = operatingRevenueGrowth;
	}

	/**
	 * @return the sharePriceGrowth
	 */
	public List<HistoricalData> getSharePriceGrowth() {
		return sharePriceGrowth;
	}

	/**
	 * @param sharePriceGrowth the sharePriceGrowth to set
	 */
	public void setSharePriceGrowth(List<HistoricalData> sharePriceGrowth) {
		this.sharePriceGrowth = sharePriceGrowth;
	}

	/**
	 * @return the historicalTotalRevenue
	 */
	public List<HistoricalData> getHistoricalTotalRevenue() {
		return historicalTotalRevenue;
	}

	/**
	 * @param historicalTotalRevenue the historicalTotalRevenue to set
	 */
	public void setHistoricalTotalRevenue(List<HistoricalData> historicalTotalRevenue) {
		this.historicalTotalRevenue = historicalTotalRevenue;
	}

	/**
	 * @return the historicalProfitAfterTax
	 */
	public List<HistoricalData> getHistoricalProfitAfterTax() {
		return historicalProfitAfterTax;
	}

	/**
	 * @param historicalProfitAfterTax the historicalProfitAfterTax to set
	 */
	public void setHistoricalProfitAfterTax(List<HistoricalData> historicalProfitAfterTax) {
		this.historicalProfitAfterTax = historicalProfitAfterTax;
	}

	/**
	 * @return the historicalProfitAfterTaxMargin
	 */
	public List<HistoricalData> getHistoricalProfitAfterTaxMargin() {
		return historicalProfitAfterTaxMargin;
	}

	/**
	 * @param historicalProfitAfterTaxMargin the historicalProfitAfterTaxMargin to
	 *                                       set
	 */
	public void setHistoricalProfitAfterTaxMargin(List<HistoricalData> historicalProfitAfterTaxMargin) {
		this.historicalProfitAfterTaxMargin = historicalProfitAfterTaxMargin;
	}

	/**
	 * @return the historicalSalesPerShare
	 */
	public List<HistoricalData> getHistoricalSalesPerShare() {
		return historicalSalesPerShare;
	}

	/**
	 * @param historicalSalesPerShare the historicalSalesPerShare to set
	 */
	public void setHistoricalSalesPerShare(List<HistoricalData> historicalSalesPerShare) {
		this.historicalSalesPerShare = historicalSalesPerShare;
	}

	/**
	 * @return the historicalBookValuePerShare
	 */
	public List<HistoricalData> getHistoricalBookValuePerShare() {
		return historicalBookValuePerShare;
	}

	/**
	 * @param historicalBookValuePerShare the historicalBookValuePerShare to set
	 */
	public void setHistoricalBookValuePerShare(List<HistoricalData> historicalBookValuePerShare) {
		this.historicalBookValuePerShare = historicalBookValuePerShare;
	}

	/**
	 * @return the historicalEarningsPerShare
	 */
	public List<HistoricalData> getHistoricalEarningsPerShare() {
		return historicalEarningsPerShare;
	}

	/**
	 * @param historicalEarningsPerShare the historicalEarningsPerShare to set
	 */
	public void setHistoricalEarningsPerShare(List<HistoricalData> historicalEarningsPerShare) {
		this.historicalEarningsPerShare = historicalEarningsPerShare;
	}

	/**
	 * @return the historicalGrossProfit
	 */
	public List<HistoricalData> getHistoricalGrossProfit() {
		return historicalGrossProfit;
	}

	/**
	 * @param historicalGrossProfit the historicalGrossProfit to set
	 */
	public void setHistoricalGrossProfit(List<HistoricalData> historicalGrossProfit) {
		this.historicalGrossProfit = historicalGrossProfit;
	}

	/**
	 * @return the historicalGrossProfitMargin
	 */
	public List<HistoricalData> getHistoricalGrossProfitMargin() {
		return historicalGrossProfitMargin;
	}

	/**
	 * @param historicalGrossProfitMargin the historicalGrossProfitMargin to set
	 */
	public void setHistoricalGrossProfitMargin(List<HistoricalData> historicalGrossProfitMargin) {
		this.historicalGrossProfitMargin = historicalGrossProfitMargin;
	}

	/**
	 * @return the historicalTotalDebt
	 */
	public List<HistoricalData> getHistoricalTotalDebt() {
		return historicalTotalDebt;
	}

	/**
	 * @param historicalTotalDebt the historicalTotalDebt to set
	 */
	public void setHistoricalTotalDebt(List<HistoricalData> historicalTotalDebt) {
		this.historicalTotalDebt = historicalTotalDebt;
	}

	/**
	 * @return the historicalEarningsBeforeIncTax
	 */
	public List<HistoricalData> getHistoricalEarningsBeforeIncTax() {
		return historicalEarningsBeforeIncTax;
	}

	/**
	 * @param historicalEarningsBeforeIncTax the historicalEarningsBeforeIncTax to
	 *                                       set
	 */
	public void setHistoricalEarningsBeforeIncTax(List<HistoricalData> historicalEarningsBeforeIncTax) {
		this.historicalEarningsBeforeIncTax = historicalEarningsBeforeIncTax;
	}

	/**
	 * @return the historicalTotalDebtByEarningsBfrIncTax
	 */
	public List<HistoricalData> getHistoricalTotalDebtByEarningsBfrIncTax() {
		return historicalTotalDebtByEarningsBfrIncTax;
	}

	/**
	 * @param historicalTotalDebtByEarningsBfrIncTax the
	 *                                               historicalTotalDebtByEarningsBfrIncTax
	 *                                               to set
	 */
	public void setHistoricalTotalDebtByEarningsBfrIncTax(List<HistoricalData> historicalTotalDebtByEarningsBfrIncTax) {
		this.historicalTotalDebtByEarningsBfrIncTax = historicalTotalDebtByEarningsBfrIncTax;
	}

	/**
	 * @return the historicalInventory
	 */
	public List<HistoricalData> getHistoricalInventory() {
		return historicalInventory;
	}

	/**
	 * @param historicalInventory the historicalInventory to set
	 */
	public void setHistoricalInventory(List<HistoricalData> historicalInventory) {
		this.historicalInventory = historicalInventory;
	}

	/**
	 * @return the historicalInventoryNoOfDays
	 */
	public List<HistoricalData> getHistoricalInventoryNoOfDays() {
		return historicalInventoryNoOfDays;
	}

	/**
	 * @param historicalInventoryNoOfDays the historicalInventoryNoOfDays to set
	 */
	public void setHistoricalInventoryNoOfDays(List<HistoricalData> historicalInventoryNoOfDays) {
		this.historicalInventoryNoOfDays = historicalInventoryNoOfDays;
	}

	/**
	 * @return the historicalNetSales
	 */
	public List<HistoricalData> getHistoricalNetSales() {
		return historicalNetSales;
	}

	/**
	 * @param historicalNetSales the historicalNetSales to set
	 */
	public void setHistoricalNetSales(List<HistoricalData> historicalNetSales) {
		this.historicalNetSales = historicalNetSales;
	}

	/**
	 * @return the historicalTradeReceivables
	 */
	public List<HistoricalData> getHistoricalTradeReceivables() {
		return historicalTradeReceivables;
	}

	/**
	 * @param historicalTradeReceivables the historicalTradeReceivables to set
	 */
	public void setHistoricalTradeReceivables(List<HistoricalData> historicalTradeReceivables) {
		this.historicalTradeReceivables = historicalTradeReceivables;
	}

	/**
	 * @return the historicalTradeRecievablesByNetSales
	 */
	public List<HistoricalData> getHistoricalTradeRecievablesByNetSales() {
		return historicalTradeRecievablesByNetSales;
	}

	/**
	 * @param historicalTradeRecievablesByNetSales the
	 *                                             historicalTradeRecievablesByNetSales
	 *                                             to set
	 */
	public void setHistoricalTradeRecievablesByNetSales(List<HistoricalData> historicalTradeRecievablesByNetSales) {
		this.historicalTradeRecievablesByNetSales = historicalTradeRecievablesByNetSales;
	}

	/**
	 * @return the historicalCashFlowFromOperations
	 */
	public List<HistoricalData> getHistoricalCashFlowFromOperations() {
		return historicalCashFlowFromOperations;
	}

	/**
	 * @param historicalCashFlowFromOperations the historicalCashFlowFromOperations
	 *                                         to set
	 */
	public void setHistoricalCashFlowFromOperations(List<HistoricalData> historicalCashFlowFromOperations) {
		this.historicalCashFlowFromOperations = historicalCashFlowFromOperations;
	}

	/**
	 * @return the historicalCashAndCashEquivalent
	 */
	public List<HistoricalData> getHistoricalCashAndCashEquivalent() {
		return historicalCashAndCashEquivalent;
	}

	/**
	 * @param historicalCashAndCashEquivalent the historicalCashAndCashEquivalent to
	 *                                        set
	 */
	public void setHistoricalCashAndCashEquivalent(List<HistoricalData> historicalCashAndCashEquivalent) {
		this.historicalCashAndCashEquivalent = historicalCashAndCashEquivalent;
	}

	/**
	 * @return the historicalShareholdersFund
	 */
	public List<HistoricalData> getHistoricalShareholdersFund() {
		return historicalShareholdersFund;
	}

	/**
	 * @param historicalShareholdersFund the historicalShareholdersFund to set
	 */
	public void setHistoricalShareholdersFund(List<HistoricalData> historicalShareholdersFund) {
		this.historicalShareholdersFund = historicalShareholdersFund;
	}

	/**
	 * @return the historicalReturnOnEquity
	 */
	public List<HistoricalData> getHistoricalReturnOnEquity() {
		return historicalReturnOnEquity;
	}

	/**
	 * @param historicalReturnOnEquity the historicalReturnOnEquity to set
	 */
	public void setHistoricalReturnOnEquity(List<HistoricalData> historicalReturnOnEquity) {
		this.historicalReturnOnEquity = historicalReturnOnEquity;
	}

	/**
	 * @return the historicalOperatingRevenue
	 */
	public List<HistoricalData> getHistoricalOperatingRevenue() {
		return historicalOperatingRevenue;
	}

	/**
	 * @param historicalOperatingRevenue the historicalOperatingRevenue to set
	 */
	public void setHistoricalOperatingRevenue(List<HistoricalData> historicalOperatingRevenue) {
		this.historicalOperatingRevenue = historicalOperatingRevenue;
	}

	/**
	 * @return the historicalSharePrice
	 */
	public List<HistoricalData> getHistoricalSharePrice() {
		return historicalSharePrice;
	}

	/**
	 * @param historicalSharePrice the historicalSharePrice to set
	 */
	public void setHistoricalSharePrice(List<HistoricalData> historicalSharePrice) {
		this.historicalSharePrice = historicalSharePrice;
	}

	@Override
	public String toString() {
		return "CagrAnalyzedDetails [cagrStartYear=" + cagrStartYear + ", cagrEndYear=" + cagrEndYear
				+ ", netFreeCashflow=" + netFreeCashflow + ", averageFreeCashFlow=" + averageFreeCashFlow
				+ ", totalRevenue=" + totalRevenue + ", netSales=" + netSales + ", financeCost=" + financeCost
				+ ", sharePrice=" + sharePrice + ", shareholdersFund=" + shareholdersFund + ", inventory=" + inventory
				+ ", tradeReceivables=" + tradeReceivables + ", cashFromOperatingActivities="
				+ cashFromOperatingActivities + ", cashEquivalentAtYearEnding=" + cashEquivalentAtYearEnding
				+ ", totalDebt=" + totalDebt + ", operatingRevenue=" + operatingRevenue + ", grossProfit=" + grossProfit
				+ ", earningsBfrIncomeTaxDepriAndAmor=" + earningsBfrIncomeTaxDepriAndAmor
				+ ", earningsBeforeIncomeTax=" + earningsBeforeIncomeTax + ", profitAfterTax=" + profitAfterTax
				+ ", salesPerShare=" + salesPerShare + ", bookValuePerShare=" + bookValuePerShare
				+ ", earningsPerShare=" + earningsPerShare + ", netDebt=" + netDebt
				+ ", totalPresentValueOfFreeFlowCash=" + totalPresentValueOfFreeFlowCash
				+ ", internsicValueOfSharePrice=" + internsicValueOfSharePrice + ", lowerInternsicValueOfSharePrice="
				+ lowerInternsicValueOfSharePrice + ", upperInternsicValueOfSharePrice="
				+ upperInternsicValueOfSharePrice + ", marginOfSafety=" + marginOfSafety
				+ ", totalRevenueInLineWithProfitAfterTaxCheck=" + totalRevenueInLineWithProfitAfterTaxCheck
				+ ", earningsPerShareGrowthInLineWithProfitAfterTaxCheck="
				+ earningsPerShareGrowthInLineWithProfitAfterTaxCheck + ", grossProfitMarginCheck="
				+ grossProfitMarginCheck + ", debtLevelCheck=" + debtLevelCheck
				+ ", inventoryGrowthInLineWithProfitAfterTaxCheck=" + inventoryGrowthInLineWithProfitAfterTaxCheck
				+ ", tradeReceivablesToNetSalesCheck=" + tradeReceivablesToNetSalesCheck
				+ ", cashFlowFromOperationsCheck=" + cashFlowFromOperationsCheck + ", returnOnEquityCheck="
				+ returnOnEquityCheck + ", totalRevenueGrowth=" + totalRevenueGrowth + ", profitAfterTaxGrowth="
				+ profitAfterTaxGrowth + ", salesPerShareGrowth=" + salesPerShareGrowth + ", bookValuePerShareGrowth="
				+ bookValuePerShareGrowth + ", earningsPerShareGrowth=" + earningsPerShareGrowth
				+ ", grossProfitGrowth=" + grossProfitGrowth + ", inventoryGrowth=" + inventoryGrowth
				+ ", netSalesGrowth=" + netSalesGrowth + ", cashFlowFromOperationsGrowth="
				+ cashFlowFromOperationsGrowth + ", cashAndCashEquivalentGrowth=" + cashAndCashEquivalentGrowth
				+ ", shareholdersFundGrowth=" + shareholdersFundGrowth + ", operatingRevenueGrowth="
				+ operatingRevenueGrowth + ", sharePriceGrowth=" + sharePriceGrowth + ", historicalTotalRevenue="
				+ historicalTotalRevenue + ", historicalProfitAfterTax=" + historicalProfitAfterTax
				+ ", historicalProfitAfterTaxMargin=" + historicalProfitAfterTaxMargin + ", historicalSalesPerShare="
				+ historicalSalesPerShare + ", historicalBookValuePerShare=" + historicalBookValuePerShare
				+ ", historicalEarningsPerShare=" + historicalEarningsPerShare + ", historicalGrossProfit="
				+ historicalGrossProfit + ", historicalGrossProfitMargin=" + historicalGrossProfitMargin
				+ ", historicalTotalDebt=" + historicalTotalDebt + ", historicalEarningsBeforeIncTax="
				+ historicalEarningsBeforeIncTax + ", historicalTotalDebtByEarningsBfrIncTax="
				+ historicalTotalDebtByEarningsBfrIncTax + ", historicalInventory=" + historicalInventory
				+ ", historicalInventoryNoOfDays=" + historicalInventoryNoOfDays + ", historicalNetSales="
				+ historicalNetSales + ", historicalTradeReceivables=" + historicalTradeReceivables
				+ ", historicalTradeRecievablesByNetSales=" + historicalTradeRecievablesByNetSales
				+ ", historicalCashFlowFromOperations=" + historicalCashFlowFromOperations
				+ ", historicalCashAndCashEquivalent=" + historicalCashAndCashEquivalent
				+ ", historicalShareholdersFund=" + historicalShareholdersFund + ", historicalReturnOnEquity="
				+ historicalReturnOnEquity + ", historicalOperatingRevenue=" + historicalOperatingRevenue
				+ ", historicalSharePrice=" + historicalSharePrice + "]";
	}

}
