package com.naga.share.market.fundamental.analysis.model.security.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationRequestDTO {

	@NotBlank
	@Pattern(regexp = "^[A-Za-z0-9 ]*$")
	@Size(min = 1, max = 50)
	private String analyzerId;

	@Email
	private String emailId;

	@Pattern(regexp = "^[0-9\\-]*$")
	@Size(min = 10, max = 10)
	private String dateOfBirth;

	public AuthenticationRequestDTO() {
		super();
	}

	public AuthenticationRequestDTO(String analyzerId, String emailId, String dateOfBirth) {
		super();
		this.analyzerId = analyzerId;
		this.emailId = emailId;
		this.dateOfBirth = dateOfBirth;
	}

	public String getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(String analyzerId) {
		this.analyzerId = analyzerId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return "AuthenticationRequestDTO [analyzerId=" + analyzerId + ", emailId=" + emailId + ", dateOfBirth="
				+ dateOfBirth + "]";
	}

}
