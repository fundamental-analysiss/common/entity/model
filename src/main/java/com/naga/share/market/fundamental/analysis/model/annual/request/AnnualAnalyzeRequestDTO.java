package com.naga.share.market.fundamental.analysis.model.annual.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnnualAnalyzeRequestDTO {

	@NotBlank
	@Size(min = 1, max = 50)
	@Pattern(regexp = "[A-Za-z ]*$")
	public String sector;

	@NotBlank
	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String industry;

	@NotBlank
	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String companyName;

	@NotBlank
	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String companySymbol;

	@NotNull
	@Digits(integer = 4, fraction = 0)
	private int analysisYear;

	@Digits(integer = 15, fraction = 4)
	private double totalRevenue;

	@Digits(integer = 15, fraction = 4)
	private double netSales;

	@Digits(integer = 15, fraction = 4)
	private double otherIncome;

	@Digits(integer = 15, fraction = 4)
	private double totalExpenses;

	@Digits(integer = 15, fraction = 4)
	private double costOfMaterialsConsumed;

	@Digits(integer = 15, fraction = 4)
	private double purchaseOfStockInTrade;

	@Digits(integer = 15, fraction = 4)
	private double powerAndFuelConsumption;

	@Digits(integer = 15, fraction = 4)
	private double consumptionOfStoresAndSpareParts;

	@Digits(integer = 15, fraction = 4)
	private double financeCost;

	@Digits(integer = 15, fraction = 4)
	private double depreciationAndAmortization;

	@Digits(integer = 15, fraction = 4)
	private double expectionalItemsBeforeTax;

	@Digits(integer = 15, fraction = 4)
	private double applicableTax;

	@Digits(integer = 15, fraction = 4)
	private double sharePrice;

	@Digits(integer = 15, fraction = 0)
	private long totalEquityShares;

	@Digits(integer = 4, fraction = 0)
	private int faceValue;

	@Digits(integer = 15, fraction = 4)
	private double shareHoldersFundAndTotalEquity;

	@Digits(integer = 15, fraction = 4)
	private double revaluationReserves;

	@Digits(integer = 15, fraction = 4)
	private double priorYearShareHoldersFund;

	@Digits(integer = 15, fraction = 4)
	private double nonCurrentLiabilities;

	@Digits(integer = 15, fraction = 4)
	private double longTermBorrowings;

	@Digits(integer = 15, fraction = 4)
	private double currentLiabilities;

	@Digits(integer = 15, fraction = 4)
	private double shortTermBorrowings;

	@Digits(integer = 15, fraction = 4)
	private double priorYearNonCurrentLiabilities;

	@Digits(integer = 15, fraction = 4)
	private double priorYearCurrentLiabilities;

	@Digits(integer = 15, fraction = 4)
	private double nonCurrentAssets;

	@Digits(integer = 15, fraction = 4)
	private double fixedAssets;

	@Digits(integer = 15, fraction = 4)
	private double currentAssets;

	@Digits(integer = 15, fraction = 4)
	private double inventory;

	@Digits(integer = 15, fraction = 4)
	private double tradeReceivables;

	@Digits(integer = 15, fraction = 4)
	private double priorYearNonCurrentAssets;

	@Digits(integer = 15, fraction = 4)
	private double priorYearFixedAssets;

	@Digits(integer = 15, fraction = 4)
	private double priorYearCurrentAssets;

	@Digits(integer = 15, fraction = 4)
	private double priorYearInventory;

	@Digits(integer = 15, fraction = 4)
	private double priorYearTradeReceivables;

	@Digits(integer = 15, fraction = 4)
	private double cashFlowFromOperatingActivities;

	@Digits(integer = 15, fraction = 4)
	private double capitalExpenditure;

	@Digits(integer = 15, fraction = 4)
	private double cashFlowFromInvestingActivities;

	@Digits(integer = 15, fraction = 4)
	private double cashFlowFromFinancialActivities;

	@Digits(integer = 15, fraction = 4)
	private double cashAndCashEquivalentAtYearOpening;

	@Digits(integer = 15, fraction = 4)
	private double effectOfForeignExchangeCashAndCashEquivalent;

	private String reportUrl;

	/**
	 * @return the sector
	 */
	public String getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(String sector) {
		this.sector = sector;
	}

	/**
	 * @return the industry
	 */
	public String getIndustry() {
		return industry;
	}

	/**
	 * @param industry the industry to set
	 */
	public void setIndustry(String industry) {
		this.industry = industry;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the companySymbol
	 */
	public String getCompanySymbol() {
		return companySymbol;
	}

	/**
	 * @param companySymbol the companySymbol to set
	 */
	public void setCompanySymbol(String companySymbol) {
		this.companySymbol = companySymbol;
	}

	/**
	 * @return the totalRevenue
	 */
	public double getTotalRevenue() {
		return totalRevenue;
	}

	/**
	 * @param totalRevenue the totalRevenue to set
	 */
	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * @return the netSales
	 */
	public double getNetSales() {
		return netSales;
	}

	/**
	 * @param netSales the netSales to set
	 */
	public void setNetSales(double netSales) {
		this.netSales = netSales;
	}

	/**
	 * @return the otherIncome
	 */
	public double getOtherIncome() {
		return otherIncome;
	}

	/**
	 * @param otherIncome the otherIncome to set
	 */
	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}

	/**
	 * @return the totalExpenses
	 */
	public double getTotalExpenses() {
		return totalExpenses;
	}

	/**
	 * @param totalExpenses the totalExpenses to set
	 */
	public void setTotalExpenses(double totalExpenses) {
		this.totalExpenses = totalExpenses;
	}

	/**
	 * @return the costOfMaterialsConsumed
	 */
	public double getCostOfMaterialsConsumed() {
		return costOfMaterialsConsumed;
	}

	/**
	 * @param costOfMaterialsConsumed the costOfMaterialsConsumed to set
	 */
	public void setCostOfMaterialsConsumed(double costOfMaterialsConsumed) {
		this.costOfMaterialsConsumed = costOfMaterialsConsumed;
	}

	/**
	 * @return the purchaseOfStockInTrade
	 */
	public double getPurchaseOfStockInTrade() {
		return purchaseOfStockInTrade;
	}

	/**
	 * @param purchaseOfStockInTrade the purchaseOfStockInTrade to set
	 */
	public void setPurchaseOfStockInTrade(double purchaseOfStockInTrade) {
		this.purchaseOfStockInTrade = purchaseOfStockInTrade;
	}

	/**
	 * @return the powerAndFuelConsumption
	 */
	public double getPowerAndFuelConsumption() {
		return powerAndFuelConsumption;
	}

	/**
	 * @param powerAndFuelConsumption the powerAndFuelConsumption to set
	 */
	public void setPowerAndFuelConsumption(double powerAndFuelConsumption) {
		this.powerAndFuelConsumption = powerAndFuelConsumption;
	}

	/**
	 * @return the consumptionOfStoresAndSpareParts
	 */
	public double getConsumptionOfStoresAndSpareParts() {
		return consumptionOfStoresAndSpareParts;
	}

	/**
	 * @param consumptionOfStoresAndSpareParts the consumptionOfStoresAndSpareParts
	 *                                         to set
	 */
	public void setConsumptionOfStoresAndSpareParts(double consumptionOfStoresAndSpareParts) {
		this.consumptionOfStoresAndSpareParts = consumptionOfStoresAndSpareParts;
	}

	/**
	 * @return the financeCost
	 */
	public double getFinanceCost() {
		return financeCost;
	}

	/**
	 * @param financeCost the financeCost to set
	 */
	public void setFinanceCost(double financeCost) {
		this.financeCost = financeCost;
	}

	/**
	 * @return the depreciationAndAmortization
	 */
	public double getDepreciationAndAmortization() {
		return depreciationAndAmortization;
	}

	/**
	 * @param depreciationAndAmortization the depreciationAndAmortization to set
	 */
	public void setDepreciationAndAmortization(double depreciationAndAmortization) {
		this.depreciationAndAmortization = depreciationAndAmortization;
	}

	/**
	 * @return the expectionalItemsBeforeTax
	 */
	public double getExpectionalItemsBeforeTax() {
		return expectionalItemsBeforeTax;
	}

	/**
	 * @param expectionalItemsBeforeTax the expectionalItemsBeforeTax to set
	 */
	public void setExpectionalItemsBeforeTax(double expectionalItemsBeforeTax) {
		this.expectionalItemsBeforeTax = expectionalItemsBeforeTax;
	}

	/**
	 * @return the analysisYear
	 */
	public int getAnalysisYear() {
		return analysisYear;
	}

	/**
	 * @param analysisYear the analysisYear to set
	 */
	public void setAnalysisYear(int analysisYear) {
		this.analysisYear = analysisYear;
	}

	/**
	 * @return the applicableTax
	 */
	public double getApplicableTax() {
		return applicableTax;
	}

	/**
	 * @param applicableTax the applicableTax to set
	 */
	public void setApplicableTax(double applicableTax) {
		this.applicableTax = applicableTax;
	}

	/**
	 * @return the sharePrice
	 */
	public double getSharePrice() {
		return sharePrice;
	}

	/**
	 * @param sharePrice the sharePrice to set
	 */
	public void setSharePrice(double sharePrice) {
		this.sharePrice = sharePrice;
	}

	/**
	 * @return the totalEquityShares
	 */
	public long getTotalEquityShares() {
		return totalEquityShares;
	}

	/**
	 * @param totalEquityShares the totalEquityShares to set
	 */
	public void setTotalEquityShares(long totalEquityShares) {
		this.totalEquityShares = totalEquityShares;
	}

	/**
	 * @return the faceValue
	 */
	public int getFaceValue() {
		return faceValue;
	}

	/**
	 * @param faceValue the faceValue to set
	 */
	public void setFaceValue(int faceValue) {
		this.faceValue = faceValue;
	}

	/**
	 * @return the shareHoldersFundAndTotalEquity
	 */
	public double getShareHoldersFundAndTotalEquity() {
		return shareHoldersFundAndTotalEquity;
	}

	/**
	 * @param shareHoldersFundAndTotalEquity the shareHoldersFundAndTotalEquity to
	 *                                       set
	 */
	public void setShareHoldersFundAndTotalEquity(double shareHoldersFundAndTotalEquity) {
		this.shareHoldersFundAndTotalEquity = shareHoldersFundAndTotalEquity;
	}

	/**
	 * @return the revaluationReserves
	 */
	public double getRevaluationReserves() {
		return revaluationReserves;
	}

	/**
	 * @param revaluationReserves the revaluationReserves to set
	 */
	public void setRevaluationReserves(double revaluationReserves) {
		this.revaluationReserves = revaluationReserves;
	}

	/**
	 * @return the priorYearShareHoldersFund
	 */
	public double getPriorYearShareHoldersFund() {
		return priorYearShareHoldersFund;
	}

	/**
	 * @param priorYearShareHoldersFund the priorYearShareHoldersFund to set
	 */
	public void setPriorYearShareHoldersFund(double priorYearShareHoldersFund) {
		this.priorYearShareHoldersFund = priorYearShareHoldersFund;
	}

	/**
	 * @return the nonCurrentLiabilities
	 */
	public double getNonCurrentLiabilities() {
		return nonCurrentLiabilities;
	}

	/**
	 * @param nonCurrentLiabilities the nonCurrentLiabilities to set
	 */
	public void setNonCurrentLiabilities(double nonCurrentLiabilities) {
		this.nonCurrentLiabilities = nonCurrentLiabilities;
	}

	/**
	 * @return the longTermBorrowings
	 */
	public double getLongTermBorrowings() {
		return longTermBorrowings;
	}

	/**
	 * @param longTermBorrowings the longTermBorrowings to set
	 */
	public void setLongTermBorrowings(double longTermBorrowings) {
		this.longTermBorrowings = longTermBorrowings;
	}

	/**
	 * @return the currentLiabilities
	 */
	public double getCurrentLiabilities() {
		return currentLiabilities;
	}

	/**
	 * @param currentLiabilities the currentLiabilities to set
	 */
	public void setCurrentLiabilities(double currentLiabilities) {
		this.currentLiabilities = currentLiabilities;
	}

	/**
	 * @return the shortTermBorrowings
	 */
	public double getShortTermBorrowings() {
		return shortTermBorrowings;
	}

	/**
	 * @param shortTermBorrowings the shortTermBorrowings to set
	 */
	public void setShortTermBorrowings(double shortTermBorrowings) {
		this.shortTermBorrowings = shortTermBorrowings;
	}

	/**
	 * @return the priorYearNonCurrentLiabilities
	 */
	public double getPriorYearNonCurrentLiabilities() {
		return priorYearNonCurrentLiabilities;
	}

	/**
	 * @param priorYearNonCurrentLiabilities the priorYearNonCurrentLiabilities to
	 *                                       set
	 */
	public void setPriorYearNonCurrentLiabilities(double priorYearNonCurrentLiabilities) {
		this.priorYearNonCurrentLiabilities = priorYearNonCurrentLiabilities;
	}

	/**
	 * @return the priorYearCurrentLiabilities
	 */
	public double getPriorYearCurrentLiabilities() {
		return priorYearCurrentLiabilities;
	}

	/**
	 * @param priorYearCurrentLiabilities the priorYearCurrentLiabilities to set
	 */
	public void setPriorYearCurrentLiabilities(double priorYearCurrentLiabilities) {
		this.priorYearCurrentLiabilities = priorYearCurrentLiabilities;
	}

	/**
	 * @return the nonCurrentAssets
	 */
	public double getNonCurrentAssets() {
		return nonCurrentAssets;
	}

	/**
	 * @param nonCurrentAssets the nonCurrentAssets to set
	 */
	public void setNonCurrentAssets(double nonCurrentAssets) {
		this.nonCurrentAssets = nonCurrentAssets;
	}

	/**
	 * @return the fixedAssets
	 */
	public double getFixedAssets() {
		return fixedAssets;
	}

	/**
	 * @param fixedAssets the fixedAssets to set
	 */
	public void setFixedAssets(double fixedAssets) {
		this.fixedAssets = fixedAssets;
	}

	/**
	 * @return the currentAssets
	 */
	public double getCurrentAssets() {
		return currentAssets;
	}

	/**
	 * @param currentAssets the currentAssets to set
	 */
	public void setCurrentAssets(double currentAssets) {
		this.currentAssets = currentAssets;
	}

	/**
	 * @return the inventory
	 */
	public double getInventory() {
		return inventory;
	}

	/**
	 * @param inventory the inventory to set
	 */
	public void setInventory(double inventory) {
		this.inventory = inventory;
	}

	/**
	 * @return the tradeReceivables
	 */
	public double getTradeReceivables() {
		return tradeReceivables;
	}

	/**
	 * @param tradeReceivables the tradeReceivables to set
	 */
	public void setTradeReceivables(double tradeReceivables) {
		this.tradeReceivables = tradeReceivables;
	}

	/**
	 * @return the priorYearNonCurrentAssets
	 */
	public double getPriorYearNonCurrentAssets() {
		return priorYearNonCurrentAssets;
	}

	/**
	 * @param priorYearNonCurrentAssets the priorYearNonCurrentAssets to set
	 */
	public void setPriorYearNonCurrentAssets(double priorYearNonCurrentAssets) {
		this.priorYearNonCurrentAssets = priorYearNonCurrentAssets;
	}

	/**
	 * @return the priorYearFixedAssets
	 */
	public double getPriorYearFixedAssets() {
		return priorYearFixedAssets;
	}

	/**
	 * @param priorYearFixedAssets the priorYearFixedAssets to set
	 */
	public void setPriorYearFixedAssets(double priorYearFixedAssets) {
		this.priorYearFixedAssets = priorYearFixedAssets;
	}

	/**
	 * @return the priorYearCurrentAssets
	 */
	public double getPriorYearCurrentAssets() {
		return priorYearCurrentAssets;
	}

	/**
	 * @param priorYearCurrentAssets the priorYearCurrentAssets to set
	 */
	public void setPriorYearCurrentAssets(double priorYearCurrentAssets) {
		this.priorYearCurrentAssets = priorYearCurrentAssets;
	}

	/**
	 * @return the priorYearInventory
	 */
	public double getPriorYearInventory() {
		return priorYearInventory;
	}

	/**
	 * @param priorYearInventory the priorYearInventory to set
	 */
	public void setPriorYearInventory(double priorYearInventory) {
		this.priorYearInventory = priorYearInventory;
	}

	/**
	 * @return the priorYearTradeReceivables
	 */
	public double getPriorYearTradeReceivables() {
		return priorYearTradeReceivables;
	}

	/**
	 * @param priorYearTradeReceivables the priorYearTradeReceivables to set
	 */
	public void setPriorYearTradeReceivables(double priorYearTradeReceivables) {
		this.priorYearTradeReceivables = priorYearTradeReceivables;
	}

	/**
	 * @return the cashFlowFromOperatingActivities
	 */
	public double getCashFlowFromOperatingActivities() {
		return cashFlowFromOperatingActivities;
	}

	/**
	 * @param cashFlowFromOperatingActivities the cashFlowFromOperatingActivities to
	 *                                        set
	 */
	public void setCashFlowFromOperatingActivities(double cashFlowFromOperatingActivities) {
		this.cashFlowFromOperatingActivities = cashFlowFromOperatingActivities;
	}

	/**
	 * @return the capitalExpenditure
	 */
	public double getCapitalExpenditure() {
		return capitalExpenditure;
	}

	/**
	 * @param capitalExpenditure the capitalExpenditure to set
	 */
	public void setCapitalExpenditure(double capitalExpenditure) {
		this.capitalExpenditure = capitalExpenditure;
	}

	/**
	 * @return the cashFlowFromInvestingActivities
	 */
	public double getCashFlowFromInvestingActivities() {
		return cashFlowFromInvestingActivities;
	}

	/**
	 * @param cashFlowFromInvestingActivities the cashFlowFromInvestingActivities to
	 *                                        set
	 */
	public void setCashFlowFromInvestingActivities(double cashFlowFromInvestingActivities) {
		this.cashFlowFromInvestingActivities = cashFlowFromInvestingActivities;
	}

	/**
	 * @return the cashFlowFromFinancialActivities
	 */
	public double getCashFlowFromFinancialActivities() {
		return cashFlowFromFinancialActivities;
	}

	/**
	 * @param cashFlowFromFinancialActivities the cashFlowFromFinancialActivities to
	 *                                        set
	 */
	public void setCashFlowFromFinancialActivities(double cashFlowFromFinancialActivities) {
		this.cashFlowFromFinancialActivities = cashFlowFromFinancialActivities;
	}

	/**
	 * @return the cashAndCashEquivalentAtYearOpening
	 */
	public double getCashAndCashEquivalentAtYearOpening() {
		return cashAndCashEquivalentAtYearOpening;
	}

	/**
	 * @param cashAndCashEquivalentAtYearOpening the
	 *                                           cashAndCashEquivalentAtYearOpening
	 *                                           to set
	 */
	public void setCashAndCashEquivalentAtYearOpening(double cashAndCashEquivalentAtYearOpening) {
		this.cashAndCashEquivalentAtYearOpening = cashAndCashEquivalentAtYearOpening;
	}

	/**
	 * @return the effectOfForeignExchangeCashAndCashEquivalent
	 */
	public double getEffectOfForeignExchangeCashAndCashEquivalent() {
		return effectOfForeignExchangeCashAndCashEquivalent;
	}

	/**
	 * @param effectOfForeignExchangeCashAndCashEquivalent the
	 *                                                     effectOfForeignExchangeCashAndCashEquivalent
	 *                                                     to set
	 */
	public void setEffectOfForeignExchangeCashAndCashEquivalent(double effectOfForeignExchangeCashAndCashEquivalent) {
		this.effectOfForeignExchangeCashAndCashEquivalent = effectOfForeignExchangeCashAndCashEquivalent;
	}

	/**
	 * @return the reportUrl
	 */
	public String getReportUrl() {
		return reportUrl;
	}

	/**
	 * @param reportUrl the reportUrl to set
	 */
	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}

}
