package com.naga.share.market.fundamental.analysis.model.sector.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;

/**
 * @author Nagaaswin S
 *
 */
@NamedQueries(value = { @NamedQuery(name = "sectorFindAllSectors", query = "FROM Sector"),
		@NamedQuery(name = "sectorFindBySectorName", query = "FROM Sector sector WHERE sector.sectorName = :sectorName") })
@Entity
public class Sector {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, unique = true)
	private String sectorName;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(nullable = false)
	private String lastUpdatedBy;

	@Column(nullable = false)
	@UpdateTimestamp
	private LocalDateTime lastUpdatedOn;

	@OneToMany(mappedBy = "sector", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Industry> industry = new ArrayList<Industry>();

	@OneToMany(mappedBy = "sector", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Company> company = new ArrayList<Company>();

	@OneToMany(mappedBy = "sector", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Annual> annual = new ArrayList<Annual>();

	@OneToMany(mappedBy = "sector", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompoundedAnnualGrowthRate> compoundedAnnualGrowthRate = new ArrayList<CompoundedAnnualGrowthRate>();

	public Sector() {
	}

	public String getSectorName() {
		return sectorName;
	}

	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public LocalDateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public long getId() {
		return id;
	}

	public void addIndustry(Industry industry) {
		this.industry.add(industry);
	}

	public void removeIndustry(Industry industry) {
		this.industry.remove(industry);
	}

	public void addCompany(Company company) {
		this.company.add(company);
	}

	public void removeCompany(Company company) {
		this.company.remove(company);
	}

	public void addAnnual(Annual annual) {
		this.annual.add(annual);
	}

	public void removeAnnual(Annual annual) {
		this.annual.remove(annual);
	}

	public List<Industry> getIndustry() {
		return industry;
	}

	public List<Company> getCompany() {
		return company;
	}

	public List<Annual> getAnnual() {
		return annual;
	}

	public List<CompoundedAnnualGrowthRate> getCompoundedAnnualGrowthRate() {
		return compoundedAnnualGrowthRate;
	}

	public void addCompoundedAnnualGrowthRate(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		this.compoundedAnnualGrowthRate.add(compoundedAnnualGrowthRate);
	}

	public void removeCompoundedAnnualGrowthRate(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		this.compoundedAnnualGrowthRate.remove(compoundedAnnualGrowthRate);
	}

	@Override
	public String toString() {
		return "Sector [id=" + id + ", sectorName=" + sectorName + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedOn=" + lastUpdatedOn + ", industry="
				+ industry + ", company=" + company + ", annual=" + annual + ", compoundedAnnualGrowthRate="
				+ compoundedAnnualGrowthRate + "]";
	}

}
