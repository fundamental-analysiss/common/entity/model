package com.naga.share.market.fundamental.analysis.model.security.response;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

/**
 * @author Nagaaswin S
 *
 */
@Component
public class AllAnalyzers {

	private String analyzerId;
	private String firstName;
	private String lastName;
	private ArrayList<String> roles;

	public String getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(String analyzerId) {
		this.analyzerId = analyzerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the roles
	 */
	public ArrayList<String> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "AllAnalyzers [analyzerId=" + analyzerId + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
