package com.naga.share.market.fundamental.analysis.model.company.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;

/**
 * @author Nagaaswin S
 *
 */
@NamedQueries(value = { @NamedQuery(name = "companyFindAllCompanies", query = "FROM Company"),
		@NamedQuery(name = "companyFindByCompanyName", query = "FROM Company company WHERE company.companyName = :companyName") })
@Entity
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Sector sector;

	@ManyToOne
	private Industry industry;

	@Column(nullable = false, unique = true)
	private String companyName;

	@Column(nullable = false, unique = true)
	private String companySymbol;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(nullable = false)
	private String lastUpdatedBy;

	@Column(nullable = false)
	@UpdateTimestamp
	private LocalDateTime lastUpdatedOn;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Annual> annual = new ArrayList<Annual>();

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompoundedAnnualGrowthRate> compoundedAnnualGrowthRate = new ArrayList<CompoundedAnnualGrowthRate>();

	public Company() {
		super();
	}

	public long getId() {
		return id;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanySymbol() {
		return companySymbol;
	}

	public void setCompanySymbol(String companySymbol) {
		this.companySymbol = companySymbol;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public LocalDateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public void addAnnual(Annual annual) {
		this.annual.add(annual);
	}

	public void removeAnnual(Annual annual) {
		this.annual.remove(annual);
	}

	public List<Annual> getAnnual() {
		return annual;
	}

	public List<CompoundedAnnualGrowthRate> getCompoundedAnnualGrowthRate() {
		return compoundedAnnualGrowthRate;
	}

	public void addCompoundedAnnualGrowthRate(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		this.compoundedAnnualGrowthRate.add(compoundedAnnualGrowthRate);
	}

	public void removeCompoundedAnnualGrowthRate(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		this.compoundedAnnualGrowthRate.remove(compoundedAnnualGrowthRate);
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", sector=" + sector + ", industry=" + industry + ", companyName=" + companyName
				+ ", companySymbol=" + companySymbol + ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedOn=" + lastUpdatedOn + ", annual=" + annual
				+ ", compoundedAnnualGrowthRate=" + compoundedAnnualGrowthRate + "]";
	}

}
