package com.naga.share.market.fundamental.analysis.model.annual.request;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnnualDeleteRequestDTO {

	@NotBlank
	@Pattern(regexp = "[A-Za-z ]*$")
	@Size(min = 1, max = 50)
	private String companyName;

	@NotNull
	@Digits(integer = 4, fraction = 0)
	private int analysisYear;

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the analysisYear
	 */
	public int getAnalysisYear() {
		return analysisYear;
	}

	/**
	 * @param analysisYear the analysisYear to set
	 */
	public void setAnalysisYear(int analysisYear) {
		this.analysisYear = analysisYear;
	}

}
