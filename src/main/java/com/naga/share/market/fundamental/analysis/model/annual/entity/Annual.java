package com.naga.share.market.fundamental.analysis.model.annual.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;

/**
 * @author Nagaaswin S
 *
 */
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "company_id", "analysisYear" }))
@NamedQueries(value = { @NamedQuery(name = "annualFindAllAnnuallyAnalyzed", query = "FROM Annual"),
		@NamedQuery(name = "annualFindByCompanyAndAnalysisYear", query = "FROM Annual annual WHERE annual.company = :company AND annual.analysisYear = :analysisYear"),
		@NamedQuery(name = "annualFindAllAnnuallyAnalyzedDetailsForACompany", query = "FROM Annual annual WHERE annual.company = :company"),
		@NamedQuery(name = "annualFindAllAnnuallyAnalyzedYearsForACompany", query = "SELECT annual.analysisYear FROM Annual annual WHERE annual.company = :company ORDER BY annual.analysisYear"),
		@NamedQuery(name = "annualFindAnnuallyAnalyzedDetailsForACompanyBtwYears", query = "FROM Annual annual WHERE annual.company = :company AND annual.analysisYear BETWEEN :analysisStartYear AND :analysisEndYear ORDER BY annual.analysisYear") })
@Entity
public class Annual {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Sector sector;

	@ManyToOne
	private Industry industry;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@Column(nullable = false)
	private int analysisYear;

	private double totalRevenue;

	private double netSales;

	private double otherIncome;

	private double totalExpenses;

	private double costOfMaterialsConsumed;

	private double purchaseOfStockInTrade;

	private double powerAndFuelConsumption;

	private double consumptionOfStoresAndSpareParts;

	private double financeCost;

	private double depreciationAndAmortization;

	private double expectionalItemsBeforeTax;

	private double applicableTax;

	private double sharePrice;

	private long totalEquityShares;

	private int faceValue;

	private double shareHoldersFundAndTotalEquity;

	private double revaluationReserves;

	private double priorYearShareHoldersFundAndTotalEquity;

	private double nonCurrentLiabilities;

	private double longTermBorrowings;

	private double currentLiabilities;

	private double shortTermBorrowings;

	private double priorYearNonCurrentLiabilities;

	private double priorYearCurrentLiabilities;

	private double nonCurrentAssets;

	private double fixedAssets;

	private double currentAssets;

	private double inventory;

	private double tradeReceivables;

	private double priorYearNonCurrentAssets;

	private double priorYearFixedAssets;

	private double priorYearCurrentAssets;

	private double priorYearInventory;

	private double priorYearTradeReceivables;

	private double cashFlowFromOperatingActivities;

	private double capitalExpenditure;

	private double cashFlowFromInvestingActivities;

	private double cashFlowFromFinancialActivities;

	private double cashAndCashEquivalentAtYearOpening;

	private double effectOfForeignExchangeCashAndCashEquivalent;

	private double operatingRevenue;

	private double costOfGoodsSold;

	private double grossProfit;

	private double grossProfitMargin;

	private double operatingExpenses;

	private double earningsBfrIncomeTaxDepriAndAmor;

	private double earningsBfrIncomeTaxDepriAndAmorMargin;

	private double earningsBeforeIncomeTax;

	private double interestCoverageRatio;

	private double profitBeforeTax;

	private double profitAfterTax;

	private double profitAfterTaxMargin;

	private double netProfitMargin;

	private double salesPerShare;

	private double priceToSalesRatio;

	private double earningsPerShare;

	private double shareCapital;

	private double bookValuePerShare;

	private double priceToBookValueRatio;

	private double priceToEarningsRatio;

	private double averageShareHoldersFund;

	private double overallCapitalEmployed;

	private double returnOnCapitalEmployed;

	private double totalDebt;

	private double debtToEquityRatio;

	private double totalAssets;

	private double totalLiabilities;

	private double averageTradeReceivables;

	private double averageInventory;

	private double inventoryTurnoverRatio;

	private double inventoryNoOfDays;

	private double accountsReceivableTurnoverRatio;

	private double daysSalesOutstanding;

	private double priorYearTotalLiabilities;

	private double priorYearTotalAssets;

	private double averageFixedAssets;

	private double averageTotalAssets;

	private double fixedAssetsTurnoverRatio;

	private double totalAssetsTurnoverRatio;

	private double financialLeverageRatio;

	private double returnOnEquity;

	private double returnOnAssets;

	private double debtToAssestRatio;

	private double workingCapital;

	private double priorYearWorkingCapital;

	private double averageWorkingCapital;

	private double workingCapitalTurnoverRatio;

	private double freeCashFlow;

	private double cashFlow;

	private double cashAndCashEquivalentAtYearEnding;

	private String grossProfitMarginCheck;

	@Column(name = "ebitda_margin_check")
	private String EBITDAMarginCheck;

	private String profitAfterTaxMarginCheck;

	private String returnOnEquityCheck;

	private String returnOnAssetsCheck;

	private String returnOnCapitalEmployedCheck;

	private String interestCoverageRatioCheck;

	private String debtToEquityRatioCheck;

	private String debtToAssetsRatioCheck;

	private String financialLeverageRatioCheck;

	private String fixedAssetsTurnoverRatioCheck;

	private String totalAssetsTurnoverRatioCheck;

	private String workingCapitalTurnoverRatioCheck;

	private String priceToSalesRatioCheck;

	private String priceToBookRatioCheck;

	private String priceToEarningsRatioCheck;

	private String reportURL;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(nullable = false)
	private String lastUpdatedBy;

	@Column(nullable = false)
	@UpdateTimestamp
	private LocalDateTime lastUpdatedOn;

	public Annual() {
		super();
	}

	public long getId() {
		return id;
	}

	/**
	 * @return the sector
	 */
	public Sector getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(Sector sector) {
		this.sector = sector;
	}

	/**
	 * @return the industry
	 */
	public Industry getIndustry() {
		return industry;
	}

	/**
	 * @param industry the industry to set
	 */
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the analysisYear
	 */
	public int getAnalysisYear() {
		return analysisYear;
	}

	/**
	 * @param analysisYear the analysisYear to set
	 */
	public void setAnalysisYear(int analysisYear) {
		this.analysisYear = analysisYear;
	}

	/**
	 * @return the totalRevenue
	 */
	public double getTotalRevenue() {
		return totalRevenue;
	}

	/**
	 * @param totalRevenue the totalRevenue to set
	 */
	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * @return the netSales
	 */
	public double getNetSales() {
		return netSales;
	}

	/**
	 * @param netSales the netSales to set
	 */
	public void setNetSales(double netSales) {
		this.netSales = netSales;
	}

	/**
	 * @return the otherIncome
	 */
	public double getOtherIncome() {
		return otherIncome;
	}

	/**
	 * @param otherIncome the otherIncome to set
	 */
	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}

	/**
	 * @return the totalExpenses
	 */
	public double getTotalExpenses() {
		return totalExpenses;
	}

	/**
	 * @param totalExpenses the totalExpenses to set
	 */
	public void setTotalExpenses(double totalExpenses) {
		this.totalExpenses = totalExpenses;
	}

	/**
	 * @return the costOfMaterialsConsumed
	 */
	public double getCostOfMaterialsConsumed() {
		return costOfMaterialsConsumed;
	}

	/**
	 * @param costOfMaterialsConsumed the costOfMaterialsConsumed to set
	 */
	public void setCostOfMaterialsConsumed(double costOfMaterialsConsumed) {
		this.costOfMaterialsConsumed = costOfMaterialsConsumed;
	}

	/**
	 * @return the purchaseOfStockInTrade
	 */
	public double getPurchaseOfStockInTrade() {
		return purchaseOfStockInTrade;
	}

	/**
	 * @param purchaseOfStockInTrade the purchaseOfStockInTrade to set
	 */
	public void setPurchaseOfStockInTrade(double purchaseOfStockInTrade) {
		this.purchaseOfStockInTrade = purchaseOfStockInTrade;
	}

	/**
	 * @return the powerAndFuelConsumption
	 */
	public double getPowerAndFuelConsumption() {
		return powerAndFuelConsumption;
	}

	/**
	 * @param powerAndFuelConsumption the powerAndFuelConsumption to set
	 */
	public void setPowerAndFuelConsumption(double powerAndFuelConsumption) {
		this.powerAndFuelConsumption = powerAndFuelConsumption;
	}

	/**
	 * @return the consumptionOfStoresAndSpareParts
	 */
	public double getConsumptionOfStoresAndSpareParts() {
		return consumptionOfStoresAndSpareParts;
	}

	/**
	 * @param consumptionOfStoresAndSpareParts the consumptionOfStoresAndSpareParts
	 *                                         to set
	 */
	public void setConsumptionOfStoresAndSpareParts(double consumptionOfStoresAndSpareParts) {
		this.consumptionOfStoresAndSpareParts = consumptionOfStoresAndSpareParts;
	}

	/**
	 * @return the financeCost
	 */
	public double getFinanceCost() {
		return financeCost;
	}

	/**
	 * @param financeCost the financeCost to set
	 */
	public void setFinanceCost(double financeCost) {
		this.financeCost = financeCost;
	}

	/**
	 * @return the depreciationAndAmortization
	 */
	public double getDepreciationAndAmortization() {
		return depreciationAndAmortization;
	}

	/**
	 * @param depreciationAndAmortization the depreciationAndAmortization to set
	 */
	public void setDepreciationAndAmortization(double depreciationAndAmortization) {
		this.depreciationAndAmortization = depreciationAndAmortization;
	}

	/**
	 * @return the expectionalItemsBeforeTax
	 */
	public double getExpectionalItemsBeforeTax() {
		return expectionalItemsBeforeTax;
	}

	/**
	 * @param expectionalItemsBeforeTax the expectionalItemsBeforeTax to set
	 */
	public void setExpectionalItemsBeforeTax(double expectionalItemsBeforeTax) {
		this.expectionalItemsBeforeTax = expectionalItemsBeforeTax;
	}

	/**
	 * @return the applicableTax
	 */
	public double getApplicableTax() {
		return applicableTax;
	}

	/**
	 * @param applicableTax the applicableTax to set
	 */
	public void setApplicableTax(double applicableTax) {
		this.applicableTax = applicableTax;
	}

	/**
	 * @return the sharePrice
	 */
	public double getSharePrice() {
		return sharePrice;
	}

	/**
	 * @param sharePrice the sharePrice to set
	 */
	public void setSharePrice(double sharePrice) {
		this.sharePrice = sharePrice;
	}

	/**
	 * @return the totalEquityShares
	 */
	public long getTotalEquityShares() {
		return totalEquityShares;
	}

	/**
	 * @param totalEquityShares the totalEquityShares to set
	 */
	public void setTotalEquityShares(long totalEquityShares) {
		this.totalEquityShares = totalEquityShares;
	}

	/**
	 * @return the faceValue
	 */
	public int getFaceValue() {
		return faceValue;
	}

	/**
	 * @param faceValue the faceValue to set
	 */
	public void setFaceValue(int faceValue) {
		this.faceValue = faceValue;
	}

	/**
	 * @return the shareHoldersFundAndTotalEquity
	 */
	public double getShareHoldersFundAndTotalEquity() {
		return shareHoldersFundAndTotalEquity;
	}

	/**
	 * @param shareHoldersFundAndTotalEquity the shareHoldersFundAndTotalEquity to
	 *                                       set
	 */
	public void setShareHoldersFundAndTotalEquity(double shareHoldersFundAndTotalEquity) {
		this.shareHoldersFundAndTotalEquity = shareHoldersFundAndTotalEquity;
	}

	/**
	 * @return the revaluationReserves
	 */
	public double getRevaluationReserves() {
		return revaluationReserves;
	}

	/**
	 * @param revaluationReserves the revaluationReserves to set
	 */
	public void setRevaluationReserves(double revaluationReserves) {
		this.revaluationReserves = revaluationReserves;
	}

	/**
	 * @return the priorYearShareHoldersFundAndTotalEquity
	 */
	public double getPriorYearShareHoldersFundAndTotalEquity() {
		return priorYearShareHoldersFundAndTotalEquity;
	}

	/**
	 * @param priorYearShareHoldersFundAndTotalEquity the
	 *                                                priorYearShareHoldersFundAndTotalEquity
	 *                                                to set
	 */
	public void setPriorYearShareHoldersFundAndTotalEquity(double priorYearShareHoldersFundAndTotalEquity) {
		this.priorYearShareHoldersFundAndTotalEquity = priorYearShareHoldersFundAndTotalEquity;
	}

	/**
	 * @return the nonCurrentLiabilities
	 */
	public double getNonCurrentLiabilities() {
		return nonCurrentLiabilities;
	}

	/**
	 * @param nonCurrentLiabilities the nonCurrentLiabilities to set
	 */
	public void setNonCurrentLiabilities(double nonCurrentLiabilities) {
		this.nonCurrentLiabilities = nonCurrentLiabilities;
	}

	/**
	 * @return the longTermBorrowings
	 */
	public double getLongTermBorrowings() {
		return longTermBorrowings;
	}

	/**
	 * @param longTermBorrowings the longTermBorrowings to set
	 */
	public void setLongTermBorrowings(double longTermBorrowings) {
		this.longTermBorrowings = longTermBorrowings;
	}

	/**
	 * @return the currentLiabilities
	 */
	public double getCurrentLiabilities() {
		return currentLiabilities;
	}

	/**
	 * @param currentLiabilities the currentLiabilities to set
	 */
	public void setCurrentLiabilities(double currentLiabilities) {
		this.currentLiabilities = currentLiabilities;
	}

	/**
	 * @return the shortTermBorrowings
	 */
	public double getShortTermBorrowings() {
		return shortTermBorrowings;
	}

	/**
	 * @param shortTermBorrowings the shortTermBorrowings to set
	 */
	public void setShortTermBorrowings(double shortTermBorrowings) {
		this.shortTermBorrowings = shortTermBorrowings;
	}

	/**
	 * @return the priorYearNonCurrentLiabilities
	 */
	public double getPriorYearNonCurrentLiabilities() {
		return priorYearNonCurrentLiabilities;
	}

	/**
	 * @param priorYearNonCurrentLiabilities the priorYearNonCurrentLiabilities to
	 *                                       set
	 */
	public void setPriorYearNonCurrentLiabilities(double priorYearNonCurrentLiabilities) {
		this.priorYearNonCurrentLiabilities = priorYearNonCurrentLiabilities;
	}

	/**
	 * @return the priorYearCurrentLiabilities
	 */
	public double getPriorYearCurrentLiabilities() {
		return priorYearCurrentLiabilities;
	}

	/**
	 * @param priorYearCurrentLiabilities the priorYearCurrentLiabilities to set
	 */
	public void setPriorYearCurrentLiabilities(double priorYearCurrentLiabilities) {
		this.priorYearCurrentLiabilities = priorYearCurrentLiabilities;
	}

	/**
	 * @return the nonCurrentAssets
	 */
	public double getNonCurrentAssets() {
		return nonCurrentAssets;
	}

	/**
	 * @param nonCurrentAssets the nonCurrentAssets to set
	 */
	public void setNonCurrentAssets(double nonCurrentAssets) {
		this.nonCurrentAssets = nonCurrentAssets;
	}

	/**
	 * @return the fixedAssets
	 */
	public double getFixedAssets() {
		return fixedAssets;
	}

	/**
	 * @param fixedAssets the fixedAssets to set
	 */
	public void setFixedAssets(double fixedAssets) {
		this.fixedAssets = fixedAssets;
	}

	/**
	 * @return the currentAssets
	 */
	public double getCurrentAssets() {
		return currentAssets;
	}

	/**
	 * @param currentAssets the currentAssets to set
	 */
	public void setCurrentAssets(double currentAssets) {
		this.currentAssets = currentAssets;
	}

	/**
	 * @return the inventory
	 */
	public double getInventory() {
		return inventory;
	}

	/**
	 * @param inventory the inventory to set
	 */
	public void setInventory(double inventory) {
		this.inventory = inventory;
	}

	/**
	 * @return the tradeReceivables
	 */
	public double getTradeReceivables() {
		return tradeReceivables;
	}

	/**
	 * @param tradeReceivables the tradeReceivables to set
	 */
	public void setTradeReceivables(double tradeReceivables) {
		this.tradeReceivables = tradeReceivables;
	}

	/**
	 * @return the priorYearNonCurrentAssets
	 */
	public double getPriorYearNonCurrentAssets() {
		return priorYearNonCurrentAssets;
	}

	/**
	 * @param priorYearNonCurrentAssets the priorYearNonCurrentAssets to set
	 */
	public void setPriorYearNonCurrentAssets(double priorYearNonCurrentAssets) {
		this.priorYearNonCurrentAssets = priorYearNonCurrentAssets;
	}

	/**
	 * @return the priorYearFixedAssets
	 */
	public double getPriorYearFixedAssets() {
		return priorYearFixedAssets;
	}

	/**
	 * @param priorYearFixedAssets the priorYearFixedAssets to set
	 */
	public void setPriorYearFixedAssets(double priorYearFixedAssets) {
		this.priorYearFixedAssets = priorYearFixedAssets;
	}

	/**
	 * @return the priorYearCurrentAssets
	 */
	public double getPriorYearCurrentAssets() {
		return priorYearCurrentAssets;
	}

	/**
	 * @param priorYearCurrentAssets the priorYearCurrentAssets to set
	 */
	public void setPriorYearCurrentAssets(double priorYearCurrentAssets) {
		this.priorYearCurrentAssets = priorYearCurrentAssets;
	}

	/**
	 * @return the priorYearInventory
	 */
	public double getPriorYearInventory() {
		return priorYearInventory;
	}

	/**
	 * @param priorYearInventory the priorYearInventory to set
	 */
	public void setPriorYearInventory(double priorYearInventory) {
		this.priorYearInventory = priorYearInventory;
	}

	/**
	 * @return the priorYearTradeReceivables
	 */
	public double getPriorYearTradeReceivables() {
		return priorYearTradeReceivables;
	}

	/**
	 * @param priorYearTradeReceivables the priorYearTradeReceivables to set
	 */
	public void setPriorYearTradeReceivables(double priorYearTradeReceivables) {
		this.priorYearTradeReceivables = priorYearTradeReceivables;
	}

	/**
	 * @return the cashFlowFromOperatingActivities
	 */
	public double getCashFlowFromOperatingActivities() {
		return cashFlowFromOperatingActivities;
	}

	/**
	 * @param cashFlowFromOperatingActivities the cashFlowFromOperatingActivities to
	 *                                        set
	 */
	public void setCashFlowFromOperatingActivities(double cashFlowFromOperatingActivities) {
		this.cashFlowFromOperatingActivities = cashFlowFromOperatingActivities;
	}

	/**
	 * @return the capitalExpenditure
	 */
	public double getCapitalExpenditure() {
		return capitalExpenditure;
	}

	/**
	 * @param capitalExpenditure the capitalExpenditure to set
	 */
	public void setCapitalExpenditure(double capitalExpenditure) {
		this.capitalExpenditure = capitalExpenditure;
	}

	/**
	 * @return the cashFlowFromInvestingActivities
	 */
	public double getCashFlowFromInvestingActivities() {
		return cashFlowFromInvestingActivities;
	}

	/**
	 * @param cashFlowFromInvestingActivities the cashFlowFromInvestingActivities to
	 *                                        set
	 */
	public void setCashFlowFromInvestingActivities(double cashFlowFromInvestingActivities) {
		this.cashFlowFromInvestingActivities = cashFlowFromInvestingActivities;
	}

	/**
	 * @return the cashFlowFromFinancialActivities
	 */
	public double getCashFlowFromFinancialActivities() {
		return cashFlowFromFinancialActivities;
	}

	/**
	 * @param cashFlowFromFinancialActivities the cashFlowFromFinancialActivities to
	 *                                        set
	 */
	public void setCashFlowFromFinancialActivities(double cashFlowFromFinancialActivities) {
		this.cashFlowFromFinancialActivities = cashFlowFromFinancialActivities;
	}

	/**
	 * @return the cashAndCashEquivalentAtYearOpening
	 */
	public double getCashAndCashEquivalentAtYearOpening() {
		return cashAndCashEquivalentAtYearOpening;
	}

	/**
	 * @param cashAndCashEquivalentAtYearOpening the
	 *                                           cashAndCashEquivalentAtYearOpening
	 *                                           to set
	 */
	public void setCashAndCashEquivalentAtYearOpening(double cashAndCashEquivalentAtYearOpening) {
		this.cashAndCashEquivalentAtYearOpening = cashAndCashEquivalentAtYearOpening;
	}

	/**
	 * @return the effectOfForeignExchangeCashAndCashEquivalent
	 */
	public double getEffectOfForeignExchangeCashAndCashEquivalent() {
		return effectOfForeignExchangeCashAndCashEquivalent;
	}

	/**
	 * @param effectOfForeignExchangeCashAndCashEquivalent the
	 *                                                     effectOfForeignExchangeCashAndCashEquivalent
	 *                                                     to set
	 */
	public void setEffectOfForeignExchangeCashAndCashEquivalent(double effectOfForeignExchangeCashAndCashEquivalent) {
		this.effectOfForeignExchangeCashAndCashEquivalent = effectOfForeignExchangeCashAndCashEquivalent;
	}

	/**
	 * @return the operatingRevenue
	 */
	public double getOperatingRevenue() {
		return operatingRevenue;
	}

	/**
	 * @param operatingRevenue the operatingRevenue to set
	 */
	public void setOperatingRevenue(double operatingRevenue) {
		this.operatingRevenue = operatingRevenue;
	}

	/**
	 * @return the costOfGoodsSold
	 */
	public double getCostOfGoodsSold() {
		return costOfGoodsSold;
	}

	/**
	 * @param costOfGoodsSold the costOfGoodsSold to set
	 */
	public void setCostOfGoodsSold(double costOfGoodsSold) {
		this.costOfGoodsSold = costOfGoodsSold;
	}

	/**
	 * @return the grossProfit
	 */
	public double getGrossProfit() {
		return grossProfit;
	}

	/**
	 * @param grossProfit the grossProfit to set
	 */
	public void setGrossProfit(double grossProfit) {
		this.grossProfit = grossProfit;
	}

	/**
	 * @return the grossProfitMargin
	 */
	public double getGrossProfitMargin() {
		return grossProfitMargin;
	}

	/**
	 * @param grossProfitMargin the grossProfitMargin to set
	 */
	public void setGrossProfitMargin(double grossProfitMargin) {
		this.grossProfitMargin = grossProfitMargin;
	}

	/**
	 * @return the operatingExpenses
	 */
	public double getOperatingExpenses() {
		return operatingExpenses;
	}

	/**
	 * @param operatingExpenses the operatingExpenses to set
	 */
	public void setOperatingExpenses(double operatingExpenses) {
		this.operatingExpenses = operatingExpenses;
	}

	/**
	 * @return the earningsBfrIncomeTaxDepriAndAmor
	 */
	public double getEarningsBfrIncomeTaxDepriAndAmor() {
		return earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @param earningsBfrIncomeTaxDepriAndAmor the earningsBfrIncomeTaxDepriAndAmor
	 *                                         to set
	 */
	public void setEarningsBfrIncomeTaxDepriAndAmor(double earningsBfrIncomeTaxDepriAndAmor) {
		this.earningsBfrIncomeTaxDepriAndAmor = earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @return the earningsBfrIncomeTaxDepriAndAmorMargin
	 */
	public double getEarningsBfrIncomeTaxDepriAndAmorMargin() {
		return earningsBfrIncomeTaxDepriAndAmorMargin;
	}

	/**
	 * @param earningsBfrIncomeTaxDepriAndAmorMargin the
	 *                                               earningsBfrIncomeTaxDepriAndAmorMargin
	 *                                               to set
	 */
	public void setEarningsBfrIncomeTaxDepriAndAmorMargin(double earningsBfrIncomeTaxDepriAndAmorMargin) {
		this.earningsBfrIncomeTaxDepriAndAmorMargin = earningsBfrIncomeTaxDepriAndAmorMargin;
	}

	/**
	 * @return the earningsBeforeIncomeTax
	 */
	public double getEarningsBeforeIncomeTax() {
		return earningsBeforeIncomeTax;
	}

	/**
	 * @param earningsBeforeIncomeTax the earningsBeforeIncomeTax to set
	 */
	public void setEarningsBeforeIncomeTax(double earningsBeforeIncomeTax) {
		this.earningsBeforeIncomeTax = earningsBeforeIncomeTax;
	}

	/**
	 * @return the interestCoverageRatio
	 */
	public double getInterestCoverageRatio() {
		return interestCoverageRatio;
	}

	/**
	 * @param interestCoverageRatio the interestCoverageRatio to set
	 */
	public void setInterestCoverageRatio(double interestCoverageRatio) {
		this.interestCoverageRatio = interestCoverageRatio;
	}

	/**
	 * @return the profitBeforeTax
	 */
	public double getProfitBeforeTax() {
		return profitBeforeTax;
	}

	/**
	 * @param profitBeforeTax the profitBeforeTax to set
	 */
	public void setProfitBeforeTax(double profitBeforeTax) {
		this.profitBeforeTax = profitBeforeTax;
	}

	/**
	 * @return the profitAfterTax
	 */
	public double getProfitAfterTax() {
		return profitAfterTax;
	}

	/**
	 * @param profitAfterTax the profitAfterTax to set
	 */
	public void setProfitAfterTax(double profitAfterTax) {
		this.profitAfterTax = profitAfterTax;
	}

	/**
	 * @return the profitAfterTaxMargin
	 */
	public double getProfitAfterTaxMargin() {
		return profitAfterTaxMargin;
	}

	/**
	 * @param profitAfterTaxMargin the profitAfterTaxMargin to set
	 */
	public void setProfitAfterTaxMargin(double profitAfterTaxMargin) {
		this.profitAfterTaxMargin = profitAfterTaxMargin;
	}

	/**
	 * @return the netProfitMargin
	 */
	public double getNetProfitMargin() {
		return netProfitMargin;
	}

	/**
	 * @param netProfitMargin the netProfitMargin to set
	 */
	public void setNetProfitMargin(double netProfitMargin) {
		this.netProfitMargin = netProfitMargin;
	}

	/**
	 * @return the salesPerShare
	 */
	public double getSalesPerShare() {
		return salesPerShare;
	}

	/**
	 * @param salesPerShare the salesPerShare to set
	 */
	public void setSalesPerShare(double salesPerShare) {
		this.salesPerShare = salesPerShare;
	}

	/**
	 * @return the priceToSalesRatio
	 */
	public double getPriceToSalesRatio() {
		return priceToSalesRatio;
	}

	/**
	 * @param priceToSalesRatio the priceToSalesRatio to set
	 */
	public void setPriceToSalesRatio(double priceToSalesRatio) {
		this.priceToSalesRatio = priceToSalesRatio;
	}

	/**
	 * @return the earningsPerShare
	 */
	public double getEarningsPerShare() {
		return earningsPerShare;
	}

	/**
	 * @param earningsPerShare the earningsPerShare to set
	 */
	public void setEarningsPerShare(double earningsPerShare) {
		this.earningsPerShare = earningsPerShare;
	}

	/**
	 * @return the shareCapital
	 */
	public double getShareCapital() {
		return shareCapital;
	}

	/**
	 * @param shareCapital the shareCapital to set
	 */
	public void setShareCapital(double shareCapital) {
		this.shareCapital = shareCapital;
	}

	/**
	 * @return the bookValuePerShare
	 */
	public double getBookValuePerShare() {
		return bookValuePerShare;
	}

	/**
	 * @param bookValuePerShare the bookValuePerShare to set
	 */
	public void setBookValuePerShare(double bookValuePerShare) {
		this.bookValuePerShare = bookValuePerShare;
	}

	/**
	 * @return the priceToBookValueRatio
	 */
	public double getPriceToBookValueRatio() {
		return priceToBookValueRatio;
	}

	/**
	 * @param priceToBookValueRatio the priceToBookValueRatio to set
	 */
	public void setPriceToBookValueRatio(double priceToBookValueRatio) {
		this.priceToBookValueRatio = priceToBookValueRatio;
	}

	/**
	 * @return the priceToEarningsRatio
	 */
	public double getPriceToEarningsRatio() {
		return priceToEarningsRatio;
	}

	/**
	 * @param priceToEarningsRatio the priceToEarningsRatio to set
	 */
	public void setPriceToEarningsRatio(double priceToEarningsRatio) {
		this.priceToEarningsRatio = priceToEarningsRatio;
	}

	/**
	 * @return the averageShareHoldersFund
	 */
	public double getAverageShareHoldersFund() {
		return averageShareHoldersFund;
	}

	/**
	 * @param averageShareHoldersFund the averageShareHoldersFund to set
	 */
	public void setAverageShareHoldersFund(double averageShareHoldersFund) {
		this.averageShareHoldersFund = averageShareHoldersFund;
	}

	/**
	 * @return the overallCapitalEmployed
	 */
	public double getOverallCapitalEmployed() {
		return overallCapitalEmployed;
	}

	/**
	 * @param overallCapitalEmployed the overallCapitalEmployed to set
	 */
	public void setOverallCapitalEmployed(double overallCapitalEmployed) {
		this.overallCapitalEmployed = overallCapitalEmployed;
	}

	/**
	 * @return the returnOnCapitalEmployed
	 */
	public double getReturnOnCapitalEmployed() {
		return returnOnCapitalEmployed;
	}

	/**
	 * @param returnOnCapitalEmployed the returnOnCapitalEmployed to set
	 */
	public void setReturnOnCapitalEmployed(double returnOnCapitalEmployed) {
		this.returnOnCapitalEmployed = returnOnCapitalEmployed;
	}

	/**
	 * @return the totalDebt
	 */
	public double getTotalDebt() {
		return totalDebt;
	}

	/**
	 * @param totalDebt the totalDebt to set
	 */
	public void setTotalDebt(double totalDebt) {
		this.totalDebt = totalDebt;
	}

	/**
	 * @return the debtToEquityRatio
	 */
	public double getDebtToEquityRatio() {
		return debtToEquityRatio;
	}

	/**
	 * @param debtToEquityRatio the debtToEquityRatio to set
	 */
	public void setDebtToEquityRatio(double debtToEquityRatio) {
		this.debtToEquityRatio = debtToEquityRatio;
	}

	/**
	 * @return the totalAssets
	 */
	public double getTotalAssets() {
		return totalAssets;
	}

	/**
	 * @param totalAssets the totalAssets to set
	 */
	public void setTotalAssets(double totalAssets) {
		this.totalAssets = totalAssets;
	}

	/**
	 * @return the totalLiabilities
	 */
	public double getTotalLiabilities() {
		return totalLiabilities;
	}

	/**
	 * @param totalLiabilities the totalLiabilities to set
	 */
	public void setTotalLiabilities(double totalLiabilities) {
		this.totalLiabilities = totalLiabilities;
	}

	/**
	 * @return the averageTradeReceivables
	 */
	public double getAverageTradeReceivables() {
		return averageTradeReceivables;
	}

	/**
	 * @param averageTradeReceivables the averageTradeReceivables to set
	 */
	public void setAverageTradeReceivables(double averageTradeReceivables) {
		this.averageTradeReceivables = averageTradeReceivables;
	}

	/**
	 * @return the averageInventory
	 */
	public double getAverageInventory() {
		return averageInventory;
	}

	/**
	 * @param averageInventory the averageInventory to set
	 */
	public void setAverageInventory(double averageInventory) {
		this.averageInventory = averageInventory;
	}

	/**
	 * @return the inventoryTurnoverRatio
	 */
	public double getInventoryTurnoverRatio() {
		return inventoryTurnoverRatio;
	}

	/**
	 * @param inventoryTurnoverRatio the inventoryTurnoverRatio to set
	 */
	public void setInventoryTurnoverRatio(double inventoryTurnoverRatio) {
		this.inventoryTurnoverRatio = inventoryTurnoverRatio;
	}

	/**
	 * @return the inventoryNoOfDays
	 */
	public double getInventoryNoOfDays() {
		return inventoryNoOfDays;
	}

	/**
	 * @param inventoryNoOfDays the inventoryNoOfDays to set
	 */
	public void setInventoryNoOfDays(double inventoryNoOfDays) {
		this.inventoryNoOfDays = inventoryNoOfDays;
	}

	/**
	 * @return the accountsReceivableTurnoverRatio
	 */
	public double getAccountsReceivableTurnoverRatio() {
		return accountsReceivableTurnoverRatio;
	}

	/**
	 * @param accountsReceivableTurnoverRatio the accountsReceivableTurnoverRatio to
	 *                                        set
	 */
	public void setAccountsReceivableTurnoverRatio(double accountsReceivableTurnoverRatio) {
		this.accountsReceivableTurnoverRatio = accountsReceivableTurnoverRatio;
	}

	/**
	 * @return the daysSalesOutstanding
	 */
	public double getDaysSalesOutstanding() {
		return daysSalesOutstanding;
	}

	/**
	 * @param daysSalesOutstanding the daysSalesOutstanding to set
	 */
	public void setDaysSalesOutstanding(double daysSalesOutstanding) {
		this.daysSalesOutstanding = daysSalesOutstanding;
	}

	/**
	 * @return the priorYearTotalLiabilities
	 */
	public double getPriorYearTotalLiabilities() {
		return priorYearTotalLiabilities;
	}

	/**
	 * @param priorYearTotalLiabilities the priorYearTotalLiabilities to set
	 */
	public void setPriorYearTotalLiabilities(double priorYearTotalLiabilities) {
		this.priorYearTotalLiabilities = priorYearTotalLiabilities;
	}

	/**
	 * @return the priorYearTotalAssets
	 */
	public double getPriorYearTotalAssets() {
		return priorYearTotalAssets;
	}

	/**
	 * @param priorYearTotalAssets the priorYearTotalAssets to set
	 */
	public void setPriorYearTotalAssets(double priorYearTotalAssets) {
		this.priorYearTotalAssets = priorYearTotalAssets;
	}

	/**
	 * @return the averageFixedAssets
	 */
	public double getAverageFixedAssets() {
		return averageFixedAssets;
	}

	/**
	 * @param averageFixedAssets the averageFixedAssets to set
	 */
	public void setAverageFixedAssets(double averageFixedAssets) {
		this.averageFixedAssets = averageFixedAssets;
	}

	/**
	 * @return the averageTotalAssets
	 */
	public double getAverageTotalAssets() {
		return averageTotalAssets;
	}

	/**
	 * @param averageTotalAssets the averageTotalAssets to set
	 */
	public void setAverageTotalAssets(double averageTotalAssets) {
		this.averageTotalAssets = averageTotalAssets;
	}

	/**
	 * @return the fixedAssetsTurnoverRatio
	 */
	public double getFixedAssetsTurnoverRatio() {
		return fixedAssetsTurnoverRatio;
	}

	/**
	 * @param fixedAssetsTurnoverRatio the fixedAssetsTurnoverRatio to set
	 */
	public void setFixedAssetsTurnoverRatio(double fixedAssetsTurnoverRatio) {
		this.fixedAssetsTurnoverRatio = fixedAssetsTurnoverRatio;
	}

	/**
	 * @return the totalAssetsTurnoverRatio
	 */
	public double getTotalAssetsTurnoverRatio() {
		return totalAssetsTurnoverRatio;
	}

	/**
	 * @param totalAssetsTurnoverRatio the totalAssetsTurnoverRatio to set
	 */
	public void setTotalAssetsTurnoverRatio(double totalAssetsTurnoverRatio) {
		this.totalAssetsTurnoverRatio = totalAssetsTurnoverRatio;
	}

	/**
	 * @return the financialLeverageRatio
	 */
	public double getFinancialLeverageRatio() {
		return financialLeverageRatio;
	}

	/**
	 * @param financialLeverageRatio the financialLeverageRatio to set
	 */
	public void setFinancialLeverageRatio(double financialLeverageRatio) {
		this.financialLeverageRatio = financialLeverageRatio;
	}

	/**
	 * @return the returnOnEquity
	 */
	public double getReturnOnEquity() {
		return returnOnEquity;
	}

	/**
	 * @param returnOnEquity the returnOnEquity to set
	 */
	public void setReturnOnEquity(double returnOnEquity) {
		this.returnOnEquity = returnOnEquity;
	}

	/**
	 * @return the returnOnAssets
	 */
	public double getReturnOnAssets() {
		return returnOnAssets;
	}

	/**
	 * @param returnOnAssets the returnOnAssets to set
	 */
	public void setReturnOnAssets(double returnOnAssets) {
		this.returnOnAssets = returnOnAssets;
	}

	/**
	 * @return the debtToAssestRatio
	 */
	public double getDebtToAssestRatio() {
		return debtToAssestRatio;
	}

	/**
	 * @param debtToAssestRatio the debtToAssestRatio to set
	 */
	public void setDebtToAssestRatio(double debtToAssestRatio) {
		this.debtToAssestRatio = debtToAssestRatio;
	}

	/**
	 * @return the workingCapital
	 */
	public double getWorkingCapital() {
		return workingCapital;
	}

	/**
	 * @param workingCapital the workingCapital to set
	 */
	public void setWorkingCapital(double workingCapital) {
		this.workingCapital = workingCapital;
	}

	/**
	 * @return the priorYearWorkingCapital
	 */
	public double getPriorYearWorkingCapital() {
		return priorYearWorkingCapital;
	}

	/**
	 * @param priorYearWorkingCapital the priorYearWorkingCapital to set
	 */
	public void setPriorYearWorkingCapital(double priorYearWorkingCapital) {
		this.priorYearWorkingCapital = priorYearWorkingCapital;
	}

	/**
	 * @return the averageWorkingCapital
	 */
	public double getAverageWorkingCapital() {
		return averageWorkingCapital;
	}

	/**
	 * @param averageWorkingCapital the averageWorkingCapital to set
	 */
	public void setAverageWorkingCapital(double averageWorkingCapital) {
		this.averageWorkingCapital = averageWorkingCapital;
	}

	/**
	 * @return the workingCapitalTurnoverRatio
	 */
	public double getWorkingCapitalTurnoverRatio() {
		return workingCapitalTurnoverRatio;
	}

	/**
	 * @param workingCapitalTurnoverRatio the workingCapitalTurnoverRatio to set
	 */
	public void setWorkingCapitalTurnoverRatio(double workingCapitalTurnoverRatio) {
		this.workingCapitalTurnoverRatio = workingCapitalTurnoverRatio;
	}

	/**
	 * @return the freeCashFlow
	 */
	public double getFreeCashFlow() {
		return freeCashFlow;
	}

	/**
	 * @param freeCashFlow the freeCashFlow to set
	 */
	public void setFreeCashFlow(double freeCashFlow) {
		this.freeCashFlow = freeCashFlow;
	}

	/**
	 * @return the cashFlow
	 */
	public double getCashFlow() {
		return cashFlow;
	}

	/**
	 * @param cashFlow the cashFlow to set
	 */
	public void setCashFlow(double cashFlow) {
		this.cashFlow = cashFlow;
	}

	/**
	 * @return the cashAndCashEquivalentAtYearEnding
	 */
	public double getCashAndCashEquivalentAtYearEnding() {
		return cashAndCashEquivalentAtYearEnding;
	}

	/**
	 * @param cashAndCashEquivalentAtYearEnding the
	 *                                          cashAndCashEquivalentAtYearEnding to
	 *                                          set
	 */
	public void setCashAndCashEquivalentAtYearEnding(double cashAndCashEquivalentAtYearEnding) {
		this.cashAndCashEquivalentAtYearEnding = cashAndCashEquivalentAtYearEnding;
	}

	/**
	 * @return the grossProfitMarginCheck
	 */
	public String getGrossProfitMarginCheck() {
		return grossProfitMarginCheck;
	}

	/**
	 * @param grossProfitMarginCheck the grossProfitMarginCheck to set
	 */
	public void setGrossProfitMarginCheck(String grossProfitMarginCheck) {
		this.grossProfitMarginCheck = grossProfitMarginCheck;
	}

	/**
	 * @return the eBITDAMarginCheck
	 */
	public String getEBITDAMarginCheck() {
		return EBITDAMarginCheck;
	}

	/**
	 * @param eBITDAMarginCheck the eBITDAMarginCheck to set
	 */
	public void setEBITDAMarginCheck(String eBITDAMarginCheck) {
		EBITDAMarginCheck = eBITDAMarginCheck;
	}

	/**
	 * @return the profitAfterTaxMarginCheck
	 */
	public String getProfitAfterTaxMarginCheck() {
		return profitAfterTaxMarginCheck;
	}

	/**
	 * @param profitAfterTaxMarginCheck the profitAfterTaxMarginCheck to set
	 */
	public void setProfitAfterTaxMarginCheck(String profitAfterTaxMarginCheck) {
		this.profitAfterTaxMarginCheck = profitAfterTaxMarginCheck;
	}

	/**
	 * @return the returnOnEquityCheck
	 */
	public String getReturnOnEquityCheck() {
		return returnOnEquityCheck;
	}

	/**
	 * @param returnOnEquityCheck the returnOnEquityCheck to set
	 */
	public void setReturnOnEquityCheck(String returnOnEquityCheck) {
		this.returnOnEquityCheck = returnOnEquityCheck;
	}

	/**
	 * @return the returnOnAssetsCheck
	 */
	public String getReturnOnAssetsCheck() {
		return returnOnAssetsCheck;
	}

	/**
	 * @param returnOnAssetsCheck the returnOnAssetsCheck to set
	 */
	public void setReturnOnAssetsCheck(String returnOnAssetsCheck) {
		this.returnOnAssetsCheck = returnOnAssetsCheck;
	}

	/**
	 * @return the returnOnCapitalEmployedCheck
	 */
	public String getReturnOnCapitalEmployedCheck() {
		return returnOnCapitalEmployedCheck;
	}

	/**
	 * @param returnOnCapitalEmployedCheck the returnOnCapitalEmployedCheck to set
	 */
	public void setReturnOnCapitalEmployedCheck(String returnOnCapitalEmployedCheck) {
		this.returnOnCapitalEmployedCheck = returnOnCapitalEmployedCheck;
	}

	/**
	 * @return the interestCoverageRatioCheck
	 */
	public String getInterestCoverageRatioCheck() {
		return interestCoverageRatioCheck;
	}

	/**
	 * @param interestCoverageRatioCheck the interestCoverageRatioCheck to set
	 */
	public void setInterestCoverageRatioCheck(String interestCoverageRatioCheck) {
		this.interestCoverageRatioCheck = interestCoverageRatioCheck;
	}

	/**
	 * @return the debtToEquityRatioCheck
	 */
	public String getDebtToEquityRatioCheck() {
		return debtToEquityRatioCheck;
	}

	/**
	 * @param debtToEquityRatioCheck the debtToEquityRatioCheck to set
	 */
	public void setDebtToEquityRatioCheck(String debtToEquityRatioCheck) {
		this.debtToEquityRatioCheck = debtToEquityRatioCheck;
	}

	/**
	 * @return the debtToAssetsRatioCheck
	 */
	public String getDebtToAssetsRatioCheck() {
		return debtToAssetsRatioCheck;
	}

	/**
	 * @param debtToAssetsRatioCheck the debtToAssetsRatioCheck to set
	 */
	public void setDebtToAssetsRatioCheck(String debtToAssetsRatioCheck) {
		this.debtToAssetsRatioCheck = debtToAssetsRatioCheck;
	}

	/**
	 * @return the financialLeverageRatioCheck
	 */
	public String getFinancialLeverageRatioCheck() {
		return financialLeverageRatioCheck;
	}

	/**
	 * @param financialLeverageRatioCheck the financialLeverageRatioCheck to set
	 */
	public void setFinancialLeverageRatioCheck(String financialLeverageRatioCheck) {
		this.financialLeverageRatioCheck = financialLeverageRatioCheck;
	}

	/**
	 * @return the fixedAssetsTurnoverRatioCheck
	 */
	public String getFixedAssetsTurnoverRatioCheck() {
		return fixedAssetsTurnoverRatioCheck;
	}

	/**
	 * @param fixedAssetsTurnoverRatioCheck the fixedAssetsTurnoverRatioCheck to set
	 */
	public void setFixedAssetsTurnoverRatioCheck(String fixedAssetsTurnoverRatioCheck) {
		this.fixedAssetsTurnoverRatioCheck = fixedAssetsTurnoverRatioCheck;
	}

	/**
	 * @return the totalAssetsTurnoverRatioCheck
	 */
	public String getTotalAssetsTurnoverRatioCheck() {
		return totalAssetsTurnoverRatioCheck;
	}

	/**
	 * @param totalAssetsTurnoverRatioCheck the totalAssetsTurnoverRatioCheck to set
	 */
	public void setTotalAssetsTurnoverRatioCheck(String totalAssetsTurnoverRatioCheck) {
		this.totalAssetsTurnoverRatioCheck = totalAssetsTurnoverRatioCheck;
	}

	/**
	 * @return the workingCapitalTurnoverRatioCheck
	 */
	public String getWorkingCapitalTurnoverRatioCheck() {
		return workingCapitalTurnoverRatioCheck;
	}

	/**
	 * @param workingCapitalTurnoverRatioCheck the workingCapitalTurnoverRatioCheck
	 *                                         to set
	 */
	public void setWorkingCapitalTurnoverRatioCheck(String workingCapitalTurnoverRatioCheck) {
		this.workingCapitalTurnoverRatioCheck = workingCapitalTurnoverRatioCheck;
	}

	/**
	 * @return the priceToSalesRatioCheck
	 */
	public String getPriceToSalesRatioCheck() {
		return priceToSalesRatioCheck;
	}

	/**
	 * @param priceToSalesRatioCheck the priceToSalesRatioCheck to set
	 */
	public void setPriceToSalesRatioCheck(String priceToSalesRatioCheck) {
		this.priceToSalesRatioCheck = priceToSalesRatioCheck;
	}

	/**
	 * @return the priceToBookRatioCheck
	 */
	public String getPriceToBookRatioCheck() {
		return priceToBookRatioCheck;
	}

	/**
	 * @param priceToBookRatioCheck the priceToBookRatioCheck to set
	 */
	public void setPriceToBookRatioCheck(String priceToBookRatioCheck) {
		this.priceToBookRatioCheck = priceToBookRatioCheck;
	}

	/**
	 * @return the priceToEarningsRatioCheck
	 */
	public String getPriceToEarningsRatioCheck() {
		return priceToEarningsRatioCheck;
	}

	/**
	 * @param priceToEarningsRatioCheck the priceToEarningsRatioCheck to set
	 */
	public void setPriceToEarningsRatioCheck(String priceToEarningsRatioCheck) {
		this.priceToEarningsRatioCheck = priceToEarningsRatioCheck;
	}

	/**
	 * @return the reportURL
	 */
	public String getReportURL() {
		return reportURL;
	}

	/**
	 * @param reportURL the reportURL to set
	 */
	public void setReportURL(String reportURL) {
		this.reportURL = reportURL;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/**
	 * @return the lastUpdatedOn
	 */
	public LocalDateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	/**
	 * @param lastUpdatedOn the lastUpdatedOn to set
	 */
	public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	@Override
	public String toString() {
		return "Annual [id=" + id + ", sector=" + sector + ", industry=" + industry + ", company=" + company
				+ ", analysisYear=" + analysisYear + ", totalRevenue=" + totalRevenue + ", netSales=" + netSales
				+ ", otherIncome=" + otherIncome + ", totalExpenses=" + totalExpenses + ", costOfMaterialsConsumed="
				+ costOfMaterialsConsumed + ", purchaseOfStockInTrade=" + purchaseOfStockInTrade
				+ ", powerAndFuelConsumption=" + powerAndFuelConsumption + ", consumptionOfStoresAndSpareParts="
				+ consumptionOfStoresAndSpareParts + ", financeCost=" + financeCost + ", depreciationAndAmortization="
				+ depreciationAndAmortization + ", expectionalItemsBeforeTax=" + expectionalItemsBeforeTax
				+ ", applicableTax=" + applicableTax + ", sharePrice=" + sharePrice + ", totalEquityShares="
				+ totalEquityShares + ", faceValue=" + faceValue + ", shareHoldersFundAndTotalEquity="
				+ shareHoldersFundAndTotalEquity + ", revaluationReserves=" + revaluationReserves
				+ ", priorYearShareHoldersFundAndTotalEquity=" + priorYearShareHoldersFundAndTotalEquity
				+ ", nonCurrentLiabilities=" + nonCurrentLiabilities + ", longTermBorrowings=" + longTermBorrowings
				+ ", currentLiabilities=" + currentLiabilities + ", shortTermBorrowings=" + shortTermBorrowings
				+ ", priorYearNonCurrentLiabilities=" + priorYearNonCurrentLiabilities
				+ ", priorYearCurrentLiabilities=" + priorYearCurrentLiabilities + ", nonCurrentAssets="
				+ nonCurrentAssets + ", fixedAssets=" + fixedAssets + ", currentAssets=" + currentAssets
				+ ", inventory=" + inventory + ", tradeReceivables=" + tradeReceivables + ", priorYearNonCurrentAssets="
				+ priorYearNonCurrentAssets + ", priorYearFixedAssets=" + priorYearFixedAssets
				+ ", priorYearCurrentAssets=" + priorYearCurrentAssets + ", priorYearInventory=" + priorYearInventory
				+ ", priorYearTradeReceivables=" + priorYearTradeReceivables + ", cashFlowFromOperatingActivities="
				+ cashFlowFromOperatingActivities + ", capitalExpenditure=" + capitalExpenditure
				+ ", cashFlowFromInvestingActivities=" + cashFlowFromInvestingActivities
				+ ", cashFlowFromFinancialActivities=" + cashFlowFromFinancialActivities
				+ ", cashAndCashEquivalentAtYearOpening=" + cashAndCashEquivalentAtYearOpening
				+ ", effectOfForeignExchangeCashAndCashEquivalent=" + effectOfForeignExchangeCashAndCashEquivalent
				+ ", operatingRevenue=" + operatingRevenue + ", costOfGoodsSold=" + costOfGoodsSold + ", grossProfit="
				+ grossProfit + ", grossProfitMargin=" + grossProfitMargin + ", operatingExpenses=" + operatingExpenses
				+ ", earningsBfrIncomeTaxDepriAndAmor=" + earningsBfrIncomeTaxDepriAndAmor
				+ ", earningsBfrIncomeTaxDepriAndAmorMargin=" + earningsBfrIncomeTaxDepriAndAmorMargin
				+ ", earningsBeforeIncomeTax=" + earningsBeforeIncomeTax + ", interestCoverageRatio="
				+ interestCoverageRatio + ", profitBeforeTax=" + profitBeforeTax + ", profitAfterTax=" + profitAfterTax
				+ ", profitAfterTaxMargin=" + profitAfterTaxMargin + ", netProfitMargin=" + netProfitMargin
				+ ", salesPerShare=" + salesPerShare + ", priceToSalesRatio=" + priceToSalesRatio
				+ ", earningsPerShare=" + earningsPerShare + ", shareCapital=" + shareCapital + ", bookValuePerShare="
				+ bookValuePerShare + ", priceToBookValueRatio=" + priceToBookValueRatio + ", priceToEarningsRatio="
				+ priceToEarningsRatio + ", averageShareHoldersFund=" + averageShareHoldersFund
				+ ", overallCapitalEmployed=" + overallCapitalEmployed + ", returnOnCapitalEmployed="
				+ returnOnCapitalEmployed + ", totalDebt=" + totalDebt + ", debtToEquityRatio=" + debtToEquityRatio
				+ ", totalAssets=" + totalAssets + ", totalLiabilities=" + totalLiabilities
				+ ", averageTradeReceivables=" + averageTradeReceivables + ", averageInventory=" + averageInventory
				+ ", inventoryTurnoverRatio=" + inventoryTurnoverRatio + ", inventoryNoOfDays=" + inventoryNoOfDays
				+ ", accountsReceivableTurnoverRatio=" + accountsReceivableTurnoverRatio + ", daysSalesOutstanding="
				+ daysSalesOutstanding + ", priorYearTotalLiabilities=" + priorYearTotalLiabilities
				+ ", priorYearTotalAssets=" + priorYearTotalAssets + ", averageFixedAssets=" + averageFixedAssets
				+ ", averageTotalAssets=" + averageTotalAssets + ", fixedAssetsTurnoverRatio="
				+ fixedAssetsTurnoverRatio + ", totalAssetsTurnoverRatio=" + totalAssetsTurnoverRatio
				+ ", financialLeverageRatio=" + financialLeverageRatio + ", returnOnEquity=" + returnOnEquity
				+ ", returnOnAssets=" + returnOnAssets + ", debtToAssestRatio=" + debtToAssestRatio
				+ ", workingCapital=" + workingCapital + ", priorYearWorkingCapital=" + priorYearWorkingCapital
				+ ", averageWorkingCapital=" + averageWorkingCapital + ", workingCapitalTurnoverRatio="
				+ workingCapitalTurnoverRatio + ", freeCashFlow=" + freeCashFlow + ", cashFlow=" + cashFlow
				+ ", cashAndCashEquivalentAtYearEnding=" + cashAndCashEquivalentAtYearEnding
				+ ", grossProfitMarginCheck=" + grossProfitMarginCheck + ", EBITDAMarginCheck=" + EBITDAMarginCheck
				+ ", profitAfterTaxMarginCheck=" + profitAfterTaxMarginCheck + ", returnOnEquityCheck="
				+ returnOnEquityCheck + ", returnOnAssetsCheck=" + returnOnAssetsCheck
				+ ", returnOnCapitalEmployedCheck=" + returnOnCapitalEmployedCheck + ", interestCoverageRatioCheck="
				+ interestCoverageRatioCheck + ", debtToEquityRatioCheck=" + debtToEquityRatioCheck
				+ ", debtToAssetsRatioCheck=" + debtToAssetsRatioCheck + ", financialLeverageRatioCheck="
				+ financialLeverageRatioCheck + ", fixedAssetsTurnoverRatioCheck=" + fixedAssetsTurnoverRatioCheck
				+ ", totalAssetsTurnoverRatioCheck=" + totalAssetsTurnoverRatioCheck
				+ ", workingCapitalTurnoverRatioCheck=" + workingCapitalTurnoverRatioCheck + ", priceToSalesRatioCheck="
				+ priceToSalesRatioCheck + ", priceToBookRatioCheck=" + priceToBookRatioCheck
				+ ", priceToEarningsRatioCheck=" + priceToEarningsRatioCheck + ", reportURL=" + reportURL
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", lastUpdatedBy=" + lastUpdatedBy
				+ ", lastUpdatedOn=" + lastUpdatedOn + "]";
	}

}
