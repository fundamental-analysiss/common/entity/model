package com.naga.share.market.fundamental.analysis.model.cagr;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4665910998036557319L;

	private double value;

	@JsonInclude(Include.NON_DEFAULT)
	private int currentYear;

	@JsonInclude(Include.NON_DEFAULT)
	private int priorYear;

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * @return the currentYear
	 */
	public int getCurrentYear() {
		return currentYear;
	}

	/**
	 * @param currentYear the currentYear to set
	 */
	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	/**
	 * @return the priorYear
	 */
	public int getPriorYear() {
		return priorYear;
	}

	/**
	 * @param priorYear the priorYear to set
	 */
	public void setPriorYear(int priorYear) {
		this.priorYear = priorYear;
	}

	@Override
	public String toString() {
		return "HistoricalData [value=" + value + ", analysisYear=" + currentYear + "]";
	}

}
