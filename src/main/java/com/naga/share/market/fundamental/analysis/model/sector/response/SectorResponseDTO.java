package com.naga.share.market.fundamental.analysis.model.sector.response;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Nagaaswin S
 *
 */
@Component
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SectorResponseDTO {

	private String sectorName;

	public String getSectorName() {
		return sectorName;
	}

	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}

	@Override
	public String toString() {
		return "SectorResponseDTO [sectorName=" + sectorName + "]";
	}

}
