package com.naga.share.market.fundamental.analysis.model.security.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author Nagaaswin S
 *
 */
@Entity
@NamedQueries(value = {
		@NamedQuery(name = "analyzerFindByEmailIdAndDOB", query = "FROM Analyzer analyzer WHERE analyzer.emailId = :emailId AND analyzer.dateOfBirth = :DOB"),
		@NamedQuery(name = "analyzerFindAllAnalyzers", query = "FROM Analyzer"),
		@NamedQuery(name = "analyzerFindByEmailId", query = "FROM Analyzer analyzer WHERE analyzer.emailId = :emailId"),
		@NamedQuery(name = "analyzerFindByAnalyzerId", query = "FROM Analyzer analyzer WHERE analyzer.analyzerId = :analyzerId") })
public class Analyzer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, unique = true)
	private String analyzerId;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false, unique = true)
	private String emailId;

	@Column(nullable = false)
	private LocalDate dateOfBirth;

	@Column(nullable = false)
	private boolean disabled;

	@Column(nullable = false)
	private ArrayList<String> roles;

	@Column(nullable = false)
	private LocalDateTime accountExpiration;

	@Column(nullable = false)
	private LocalDateTime credentialExpiration;

	@Column(nullable = false)
	@UpdateTimestamp
	private LocalDateTime lastUpdatedOn;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(nullable = false)
	private boolean locked;

	@Column(nullable = false)
	private String lastUpdatedBy;

	private String lastLoginChannel;

	public Analyzer() {
	}

	public Analyzer(String analyzerId, String firstName, String lastName, String password, String emailId,
			LocalDate dateOfBirth, boolean disabled, ArrayList<String> roles, LocalDateTime accountExpiration,
			LocalDateTime credentialExpiration, LocalDateTime lastUpdatedOn, LocalDateTime createdOn, boolean locked,
			String lastUpdatedBy, String lastLoginChannel) {
		super();
		this.analyzerId = analyzerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.emailId = emailId;
		this.dateOfBirth = dateOfBirth;
		this.disabled = disabled;
		this.roles = roles;
		this.accountExpiration = accountExpiration;
		this.credentialExpiration = credentialExpiration;
		this.lastUpdatedOn = lastUpdatedOn;
		this.createdOn = createdOn;
		this.locked = locked;
		this.lastUpdatedBy = lastUpdatedBy;
		this.lastLoginChannel = lastLoginChannel;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAnalyzerId() {
		return analyzerId;
	}

	public void setAnalyzerId(String analyzerId) {
		this.analyzerId = analyzerId;
	}

	public ArrayList<String> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getLastLoginChannel() {
		return lastLoginChannel;
	}

	public void setLastLoginChannel(String lastLoginChannel) {
		this.lastLoginChannel = lastLoginChannel;
	}

	public LocalDateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public LocalDateTime getAccountExpiration() {
		return accountExpiration;
	}

	public void setAccountExpiration(LocalDateTime accountExpiration) {
		this.accountExpiration = accountExpiration;
	}

	public LocalDateTime getCredentialExpiration() {
		return credentialExpiration;
	}

	public void setCredentialExpiration(LocalDateTime credentialExpiration) {
		this.credentialExpiration = credentialExpiration;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
	public String toString() {
		return "Analyzer [id=" + id + ", analyzerId=" + analyzerId + ", firstName=" + firstName + ", lastName="
				+ lastName + ", password=" + password + ", emailId=" + emailId + ", dateOfBirth=" + dateOfBirth
				+ ", disabled=" + disabled + ", roles=" + roles + ", accountExpiration=" + accountExpiration
				+ ", credentialExpiration=" + credentialExpiration + ", lastUpdatedOn=" + lastUpdatedOn + ", createdOn="
				+ createdOn + ", locked=" + locked + ", lastUpdatedBy=" + lastUpdatedBy + ", lastLoginChannel="
				+ lastLoginChannel + "]";
	}

}
