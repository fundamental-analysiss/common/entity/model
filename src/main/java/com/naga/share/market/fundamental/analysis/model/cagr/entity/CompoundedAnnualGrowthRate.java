package com.naga.share.market.fundamental.analysis.model.cagr.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.naga.share.market.fundamental.analysis.model.cagr.HistoricalDataWrapper;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;
import com.naga.share.market.fundamental.analysis.model.utils.HistoricalDataWrapperConverter;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "company_id", "cagr_start_year", "cagr_end_year" }))
@NamedQueries(value = { @NamedQuery(name = "cagrFindAllCAGRAnalyzed", query = "FROM CompoundedAnnualGrowthRate"),
		@NamedQuery(name = "cagrFindByCompanyAndCAGRYears", query = "FROM CompoundedAnnualGrowthRate cagr WHERE cagr.company = :company AND cagr.compoundedAnnualGrowthRateStartYear = :analysisStartYear AND cagr.compoundedAnnualGrowthRateEndYear = :analysisEndYear"),
		@NamedQuery(name = "cagrFindAllCAGRAnalyzedDetailsForACompany", query = "SELECT new CompoundedAnnualGrowthRate(cagr.compoundedAnnualGrowthRateStartYear,cagr.compoundedAnnualGrowthRateEndYear) FROM CompoundedAnnualGrowthRate cagr WHERE cagr.company = :company ORDER BY cagr.compoundedAnnualGrowthRateStartYear") })
@Entity
public class CompoundedAnnualGrowthRate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Sector sector;

	@ManyToOne
	private Industry industry;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@Column(nullable = false, name = "cagr_start_year")
	private int compoundedAnnualGrowthRateStartYear;

	@Column(nullable = false, name = "cagr_end_year")
	private int compoundedAnnualGrowthRateEndYear;

	private double netFreeCashflow;

	private double averageFreeCashFlow;

	private double totalRevenue;

	private double netSales;

	private double financeCost;

	private double sharePrice;

	private double shareholdersFund;

	private double inventory;

	private double tradeReceivables;

	private double cashFromOperatingActivities;

	private double cashEquivalentAtYearEnding;

	private double totalDebt;

	private double operatingRevenue;

	private double grossProfit;

	private double earningsBfrIncomeTaxDepriAndAmor;

	private double earningsBeforeIncomeTax;

	private double profitAfterTax;

	private double salesPerShare;

	private double bookValuePerShare;

	private double earningsPerShare;

	private double netDebt;

	private double presentValueOfTerminalValue;

	private double totalPresentValueOfFreeFlowCash;

	private double internsicValueOfSharePrice;

	private double lowerInternsicValueOfSharePrice;

	private double upperInternsicValueOfSharePrice;

	private double marginOfSafety;

	private double terminalValue;

	private double netPresentValue;

	public CompoundedAnnualGrowthRate(int compoundedAnnualGrowthRateStartYear, int compoundedAnnualGrowthRateEndYear) {
		this.compoundedAnnualGrowthRateStartYear = compoundedAnnualGrowthRateStartYear;
		this.compoundedAnnualGrowthRateEndYear = compoundedAnnualGrowthRateEndYear;
	}

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper futureCashFlowValue;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper presentFutureCashFlowValue;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalTotalRevenue;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper totalRevenueGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalProfitAfterTax;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalProfitAfterTaxMargin;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper profitAfterTaxGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalSalesPerShare;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper salesPerShareGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalBookValuePerShare;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper bookValuePerShareGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalEarningsPerShare;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper earningsPerShareGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalGrossProfit;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalGrossProfitMargin;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper grossProfitGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalTotalDebt;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalEarningsBeforeIncTax;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalTotalDebtByEarningsBfrIncTax;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalInventory;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper inventoryGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalInventoryNoOfDays;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalNetSales;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper netSalesGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalTradeReceivables;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalTradeRecievablesByNetSales;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalCashFlowFromOperations;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper cashFlowFromOperationsGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalCashAndCashEquivalent;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper cashAndCashEquivalentGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalShareholdersFund;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper shareholdersFundGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalReturnOnEquity;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalOperatingRevenue;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper operatingRevenueGrowth;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper historicalSharePrice;

	@Column(columnDefinition = "TEXT")
	@Convert(converter = HistoricalDataWrapperConverter.class)
	private HistoricalDataWrapper sharePriceGrowth;

	private String totalRevenueInLineWithProfitAfterTaxCheck;

	private String earningsPerShareGrowthInLineWithProfitAfterTaxCheck;

	private String grossProfitMarginCheck;

	private String debtLevelCheck;

	private String inventoryGrowthInLineWithProfitAfterTaxCheck;

	private String tradeReceivablesToNetSalesCheck;

	private String cashFlowFromOperationsCheck;

	private String returnOnEquityCheck;

	@Column(nullable = false)
	private String createdBy;

	@Column(nullable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(nullable = false)
	private String lastUpdatedBy;

	@Column(nullable = false)
	@UpdateTimestamp
	private LocalDateTime lastUpdatedOn;

	public CompoundedAnnualGrowthRate() {
		super();
	}

	public long getId() {
		return id;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}

	/**
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the compoundedAnnualGrowthRateStartYear
	 */
	public int getCompoundedAnnualGrowthRateStartYear() {
		return compoundedAnnualGrowthRateStartYear;
	}

	/**
	 * @param compoundedAnnualGrowthRateStartYear the
	 *                                            compoundedAnnualGrowthRateStartYear
	 *                                            to set
	 */
	public void setCompoundedAnnualGrowthRateStartYear(int compoundedAnnualGrowthRateStartYear) {
		this.compoundedAnnualGrowthRateStartYear = compoundedAnnualGrowthRateStartYear;
	}

	/**
	 * @return the compoundedAnnualGrowthRateEndYear
	 */
	public int getCompoundedAnnualGrowthRateEndYear() {
		return compoundedAnnualGrowthRateEndYear;
	}

	/**
	 * @param compoundedAnnualGrowthRateEndYear the
	 *                                          compoundedAnnualGrowthRateEndYear to
	 *                                          set
	 */
	public void setCompoundedAnnualGrowthRateEndYear(int compoundedAnnualGrowthRateEndYear) {
		this.compoundedAnnualGrowthRateEndYear = compoundedAnnualGrowthRateEndYear;
	}

	/**
	 * @return the netFreeCashflow
	 */
	public double getNetFreeCashflow() {
		return netFreeCashflow;
	}

	/**
	 * @param netFreeCashflow the netFreeCashflow to set
	 */
	public void setNetFreeCashflow(double netFreeCashflow) {
		this.netFreeCashflow = netFreeCashflow;
	}

	/**
	 * @return the averageFreeCashFlow
	 */
	public double getAverageFreeCashFlow() {
		return averageFreeCashFlow;
	}

	/**
	 * @param averageFreeCashFlow the averageFreeCashFlow to set
	 */
	public void setAverageFreeCashFlow(double averageFreeCashFlow) {
		this.averageFreeCashFlow = averageFreeCashFlow;
	}

	/**
	 * @return the totalRevenue
	 */
	public double getTotalRevenue() {
		return totalRevenue;
	}

	/**
	 * @param totalRevenue the totalRevenue to set
	 */
	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	/**
	 * @return the netSales
	 */
	public double getNetSales() {
		return netSales;
	}

	/**
	 * @param netSales the netSales to set
	 */
	public void setNetSales(double netSales) {
		this.netSales = netSales;
	}

	/**
	 * @return the financeCost
	 */
	public double getFinanceCost() {
		return financeCost;
	}

	/**
	 * @param financeCost the financeCost to set
	 */
	public void setFinanceCost(double financeCost) {
		this.financeCost = financeCost;
	}

	/**
	 * @return the sharePrice
	 */
	public double getSharePrice() {
		return sharePrice;
	}

	/**
	 * @param sharePrice the sharePrice to set
	 */
	public void setSharePrice(double sharePrice) {
		this.sharePrice = sharePrice;
	}

	/**
	 * @return the shareholdersFund
	 */
	public double getShareholdersFund() {
		return shareholdersFund;
	}

	/**
	 * @param shareholdersFund the shareholdersFund to set
	 */
	public void setShareholdersFund(double shareholdersFund) {
		this.shareholdersFund = shareholdersFund;
	}

	/**
	 * @return the inventory
	 */
	public double getInventory() {
		return inventory;
	}

	/**
	 * @param inventory the inventory to set
	 */
	public void setInventory(double inventory) {
		this.inventory = inventory;
	}

	/**
	 * @return the tradeReceivables
	 */
	public double getTradeReceivables() {
		return tradeReceivables;
	}

	/**
	 * @param tradeReceivables the tradeReceivables to set
	 */
	public void setTradeReceivables(double tradeReceivables) {
		this.tradeReceivables = tradeReceivables;
	}

	/**
	 * @return the cashFromOperatingActivities
	 */
	public double getCashFromOperatingActivities() {
		return cashFromOperatingActivities;
	}

	/**
	 * @param cashFromOperatingActivities the cashFromOperatingActivities to set
	 */
	public void setCashFromOperatingActivities(double cashFromOperatingActivities) {
		this.cashFromOperatingActivities = cashFromOperatingActivities;
	}

	/**
	 * @return the cashEquivalentAtYearEnding
	 */
	public double getCashEquivalentAtYearEnding() {
		return cashEquivalentAtYearEnding;
	}

	/**
	 * @param cashEquivalentAtYearEnding the cashEquivalentAtYearEnding to set
	 */
	public void setCashEquivalentAtYearEnding(double cashEquivalentAtYearEnding) {
		this.cashEquivalentAtYearEnding = cashEquivalentAtYearEnding;
	}

	/**
	 * @return the totalDebt
	 */
	public double getTotalDebt() {
		return totalDebt;
	}

	/**
	 * @param totalDebt the totalDebt to set
	 */
	public void setTotalDebt(double totalDebt) {
		this.totalDebt = totalDebt;
	}

	/**
	 * @return the operatingRevenue
	 */
	public double getOperatingRevenue() {
		return operatingRevenue;
	}

	/**
	 * @param operatingRevenue the operatingRevenue to set
	 */
	public void setOperatingRevenue(double operatingRevenue) {
		this.operatingRevenue = operatingRevenue;
	}

	/**
	 * @return the grossProfit
	 */
	public double getGrossProfit() {
		return grossProfit;
	}

	/**
	 * @param grossProfit the grossProfit to set
	 */
	public void setGrossProfit(double grossProfit) {
		this.grossProfit = grossProfit;
	}

	/**
	 * @return the earningsBfrIncomeTaxDepriAndAmor
	 */
	public double getEarningsBfrIncomeTaxDepriAndAmor() {
		return earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @param earningsBfrIncomeTaxDepriAndAmor the earningsBfrIncomeTaxDepriAndAmor
	 *                                         to set
	 */
	public void setEarningsBfrIncomeTaxDepriAndAmor(double earningsBfrIncomeTaxDepriAndAmor) {
		this.earningsBfrIncomeTaxDepriAndAmor = earningsBfrIncomeTaxDepriAndAmor;
	}

	/**
	 * @return the earningsBeforeIncomeTax
	 */
	public double getEarningsBeforeIncomeTax() {
		return earningsBeforeIncomeTax;
	}

	/**
	 * @param earningsBeforeIncomeTax the earningsBeforeIncomeTax to set
	 */
	public void setEarningsBeforeIncomeTax(double earningsBeforeIncomeTax) {
		this.earningsBeforeIncomeTax = earningsBeforeIncomeTax;
	}

	/**
	 * @return the profitAfterTax
	 */
	public double getProfitAfterTax() {
		return profitAfterTax;
	}

	/**
	 * @param profitAfterTax the profitAfterTax to set
	 */
	public void setProfitAfterTax(double profitAfterTax) {
		this.profitAfterTax = profitAfterTax;
	}

	/**
	 * @return the salesPerShare
	 */
	public double getSalesPerShare() {
		return salesPerShare;
	}

	/**
	 * @param salesPerShare the salesPerShare to set
	 */
	public void setSalesPerShare(double salesPerShare) {
		this.salesPerShare = salesPerShare;
	}

	/**
	 * @return the bookValuePerShare
	 */
	public double getBookValuePerShare() {
		return bookValuePerShare;
	}

	/**
	 * @param bookValuePerShare the bookValuePerShare to set
	 */
	public void setBookValuePerShare(double bookValuePerShare) {
		this.bookValuePerShare = bookValuePerShare;
	}

	/**
	 * @return the earningsPerShare
	 */
	public double getEarningsPerShare() {
		return earningsPerShare;
	}

	/**
	 * @param earningsPerShare the earningsPerShare to set
	 */
	public void setEarningsPerShare(double earningsPerShare) {
		this.earningsPerShare = earningsPerShare;
	}

	/**
	 * @return the netDebt
	 */
	public double getNetDebt() {
		return netDebt;
	}

	/**
	 * @param netDebt the netDebt to set
	 */
	public void setNetDebt(double netDebt) {
		this.netDebt = netDebt;
	}

	/**
	 * @return the presentValueOfTerminalValue
	 */
	public double getPresentValueOfTerminalValue() {
		return presentValueOfTerminalValue;
	}

	/**
	 * @param presentValueOfTerminalValue the presentValueOfTerminalValue to set
	 */
	public void setPresentValueOfTerminalValue(double presentValueOfTerminalValue) {
		this.presentValueOfTerminalValue = presentValueOfTerminalValue;
	}

	/**
	 * @return the totalPresentValueOfFreeFlowCash
	 */
	public double getTotalPresentValueOfFreeFlowCash() {
		return totalPresentValueOfFreeFlowCash;
	}

	/**
	 * @param totalPresentValueOfFreeFlowCash the totalPresentValueOfFreeFlowCash to
	 *                                        set
	 */
	public void setTotalPresentValueOfFreeFlowCash(double totalPresentValueOfFreeFlowCash) {
		this.totalPresentValueOfFreeFlowCash = totalPresentValueOfFreeFlowCash;
	}

	/**
	 * @return the internsicValueOfSharePrice
	 */
	public double getInternsicValueOfSharePrice() {
		return internsicValueOfSharePrice;
	}

	/**
	 * @param internsicValueOfSharePrice the internsicValueOfSharePrice to set
	 */
	public void setInternsicValueOfSharePrice(double internsicValueOfSharePrice) {
		this.internsicValueOfSharePrice = internsicValueOfSharePrice;
	}

	/**
	 * @return the lowerInternsicValueOfSharePrice
	 */
	public double getLowerInternsicValueOfSharePrice() {
		return lowerInternsicValueOfSharePrice;
	}

	/**
	 * @param lowerInternsicValueOfSharePrice the lowerInternsicValueOfSharePrice to
	 *                                        set
	 */
	public void setLowerInternsicValueOfSharePrice(double lowerInternsicValueOfSharePrice) {
		this.lowerInternsicValueOfSharePrice = lowerInternsicValueOfSharePrice;
	}

	/**
	 * @return the upperInternsicValueOfSharePrice
	 */
	public double getUpperInternsicValueOfSharePrice() {
		return upperInternsicValueOfSharePrice;
	}

	/**
	 * @param upperInternsicValueOfSharePrice the upperInternsicValueOfSharePrice to
	 *                                        set
	 */
	public void setUpperInternsicValueOfSharePrice(double upperInternsicValueOfSharePrice) {
		this.upperInternsicValueOfSharePrice = upperInternsicValueOfSharePrice;
	}

	/**
	 * @return the marginOfSafety
	 */
	public double getMarginOfSafety() {
		return marginOfSafety;
	}

	/**
	 * @param marginOfSafety the marginOfSafety to set
	 */
	public void setMarginOfSafety(double marginOfSafety) {
		this.marginOfSafety = marginOfSafety;
	}

	/**
	 * @return the terminalValue
	 */
	public double getTerminalValue() {
		return terminalValue;
	}

	/**
	 * @param terminalValue the terminalValue to set
	 */
	public void setTerminalValue(double terminalValue) {
		this.terminalValue = terminalValue;
	}

	/**
	 * @return the netPresentValue
	 */
	public double getNetPresentValue() {
		return netPresentValue;
	}

	/**
	 * @param netPresentValue the netPresentValue to set
	 */
	public void setNetPresentValue(double netPresentValue) {
		this.netPresentValue = netPresentValue;
	}

	/**
	 * @return the futureCashFlowValue
	 */
	public HistoricalDataWrapper getFutureCashFlowValue() {
		return futureCashFlowValue;
	}

	/**
	 * @param futureCashFlowValue the futureCashFlowValue to set
	 */
	public void setFutureCashFlowValue(HistoricalDataWrapper futureCashFlowValue) {
		this.futureCashFlowValue = futureCashFlowValue;
	}

	/**
	 * @return the presentFutureCashFlowValue
	 */
	public HistoricalDataWrapper getPresentFutureCashFlowValue() {
		return presentFutureCashFlowValue;
	}

	/**
	 * @param presentFutureCashFlowValue the presentFutureCashFlowValue to set
	 */
	public void setPresentFutureCashFlowValue(HistoricalDataWrapper presentFutureCashFlowValue) {
		this.presentFutureCashFlowValue = presentFutureCashFlowValue;
	}

	/**
	 * @return the historicalTotalRevenue
	 */
	public HistoricalDataWrapper getHistoricalTotalRevenue() {
		return historicalTotalRevenue;
	}

	/**
	 * @param historicalTotalRevenue the historicalTotalRevenue to set
	 */
	public void setHistoricalTotalRevenue(HistoricalDataWrapper historicalTotalRevenue) {
		this.historicalTotalRevenue = historicalTotalRevenue;
	}

	/**
	 * @return the totalRevenueGrowth
	 */
	public HistoricalDataWrapper getTotalRevenueGrowth() {
		return totalRevenueGrowth;
	}

	/**
	 * @param totalRevenueGrowth the totalRevenueGrowth to set
	 */
	public void setTotalRevenueGrowth(HistoricalDataWrapper totalRevenueGrowth) {
		this.totalRevenueGrowth = totalRevenueGrowth;
	}

	/**
	 * @return the historicalProfitAfterTax
	 */
	public HistoricalDataWrapper getHistoricalProfitAfterTax() {
		return historicalProfitAfterTax;
	}

	/**
	 * @param historicalProfitAfterTax the historicalProfitAfterTax to set
	 */
	public void setHistoricalProfitAfterTax(HistoricalDataWrapper historicalProfitAfterTax) {
		this.historicalProfitAfterTax = historicalProfitAfterTax;
	}

	/**
	 * @return the historicalProfitAfterTaxMargin
	 */
	public HistoricalDataWrapper getHistoricalProfitAfterTaxMargin() {
		return historicalProfitAfterTaxMargin;
	}

	/**
	 * @param historicalProfitAfterTaxMargin the historicalProfitAfterTaxMargin to
	 *                                       set
	 */
	public void setHistoricalProfitAfterTaxMargin(HistoricalDataWrapper historicalProfitAfterTaxMargin) {
		this.historicalProfitAfterTaxMargin = historicalProfitAfterTaxMargin;
	}

	/**
	 * @return the profitAfterTaxGrowth
	 */
	public HistoricalDataWrapper getProfitAfterTaxGrowth() {
		return profitAfterTaxGrowth;
	}

	/**
	 * @param profitAfterTaxGrowth the profitAfterTaxGrowth to set
	 */
	public void setProfitAfterTaxGrowth(HistoricalDataWrapper profitAfterTaxGrowth) {
		this.profitAfterTaxGrowth = profitAfterTaxGrowth;
	}

	/**
	 * @return the historicalSalesPerShare
	 */
	public HistoricalDataWrapper getHistoricalSalesPerShare() {
		return historicalSalesPerShare;
	}

	/**
	 * @param historicalSalesPerShare the historicalSalesPerShare to set
	 */
	public void setHistoricalSalesPerShare(HistoricalDataWrapper historicalSalesPerShare) {
		this.historicalSalesPerShare = historicalSalesPerShare;
	}

	/**
	 * @return the salesPerShareGrowth
	 */
	public HistoricalDataWrapper getSalesPerShareGrowth() {
		return salesPerShareGrowth;
	}

	/**
	 * @param salesPerShareGrowth the salesPerShareGrowth to set
	 */
	public void setSalesPerShareGrowth(HistoricalDataWrapper salesPerShareGrowth) {
		this.salesPerShareGrowth = salesPerShareGrowth;
	}

	/**
	 * @return the historicalBookValuePerShare
	 */
	public HistoricalDataWrapper getHistoricalBookValuePerShare() {
		return historicalBookValuePerShare;
	}

	/**
	 * @param historicalBookValuePerShare the historicalBookValuePerShare to set
	 */
	public void setHistoricalBookValuePerShare(HistoricalDataWrapper historicalBookValuePerShare) {
		this.historicalBookValuePerShare = historicalBookValuePerShare;
	}

	/**
	 * @return the bookValuePerShareGrowth
	 */
	public HistoricalDataWrapper getBookValuePerShareGrowth() {
		return bookValuePerShareGrowth;
	}

	/**
	 * @param bookValuePerShareGrowth the bookValuePerShareGrowth to set
	 */
	public void setBookValuePerShareGrowth(HistoricalDataWrapper bookValuePerShareGrowth) {
		this.bookValuePerShareGrowth = bookValuePerShareGrowth;
	}

	/**
	 * @return the historicalEarningsPerShare
	 */
	public HistoricalDataWrapper getHistoricalEarningsPerShare() {
		return historicalEarningsPerShare;
	}

	/**
	 * @param historicalEarningsPerShare the historicalEarningsPerShare to set
	 */
	public void setHistoricalEarningsPerShare(HistoricalDataWrapper historicalEarningsPerShare) {
		this.historicalEarningsPerShare = historicalEarningsPerShare;
	}

	/**
	 * @return the earningsPerShareGrowth
	 */
	public HistoricalDataWrapper getEarningsPerShareGrowth() {
		return earningsPerShareGrowth;
	}

	/**
	 * @param earningsPerShareGrowth the earningsPerShareGrowth to set
	 */
	public void setEarningsPerShareGrowth(HistoricalDataWrapper earningsPerShareGrowth) {
		this.earningsPerShareGrowth = earningsPerShareGrowth;
	}

	/**
	 * @return the historicalGrossProfit
	 */
	public HistoricalDataWrapper getHistoricalGrossProfit() {
		return historicalGrossProfit;
	}

	/**
	 * @param historicalGrossProfit the historicalGrossProfit to set
	 */
	public void setHistoricalGrossProfit(HistoricalDataWrapper historicalGrossProfit) {
		this.historicalGrossProfit = historicalGrossProfit;
	}

	/**
	 * @return the historicalGrossProfitMargin
	 */
	public HistoricalDataWrapper getHistoricalGrossProfitMargin() {
		return historicalGrossProfitMargin;
	}

	/**
	 * @param historicalGrossProfitMargin the historicalGrossProfitMargin to set
	 */
	public void setHistoricalGrossProfitMargin(HistoricalDataWrapper historicalGrossProfitMargin) {
		this.historicalGrossProfitMargin = historicalGrossProfitMargin;
	}

	/**
	 * @return the grossProfitGrowth
	 */
	public HistoricalDataWrapper getGrossProfitGrowth() {
		return grossProfitGrowth;
	}

	/**
	 * @param grossProfitGrowth the grossProfitGrowth to set
	 */
	public void setGrossProfitGrowth(HistoricalDataWrapper grossProfitGrowth) {
		this.grossProfitGrowth = grossProfitGrowth;
	}

	/**
	 * @return the historicalTotalDebt
	 */
	public HistoricalDataWrapper getHistoricalTotalDebt() {
		return historicalTotalDebt;
	}

	/**
	 * @param historicalTotalDebt the historicalTotalDebt to set
	 */
	public void setHistoricalTotalDebt(HistoricalDataWrapper historicalTotalDebt) {
		this.historicalTotalDebt = historicalTotalDebt;
	}

	/**
	 * @return the historicalEarningsBeforeIncTax
	 */
	public HistoricalDataWrapper getHistoricalEarningsBeforeIncTax() {
		return historicalEarningsBeforeIncTax;
	}

	/**
	 * @param historicalEarningsBeforeIncTax the historicalEarningsBeforeIncTax to
	 *                                       set
	 */
	public void setHistoricalEarningsBeforeIncTax(HistoricalDataWrapper historicalEarningsBeforeIncTax) {
		this.historicalEarningsBeforeIncTax = historicalEarningsBeforeIncTax;
	}

	/**
	 * @return the historicalTotalDebtByEarningsBfrIncTax
	 */
	public HistoricalDataWrapper getHistoricalTotalDebtByEarningsBfrIncTax() {
		return historicalTotalDebtByEarningsBfrIncTax;
	}

	/**
	 * @param historicalTotalDebtByEarningsBfrIncTax the
	 *                                               historicalTotalDebtByEarningsBfrIncTax
	 *                                               to set
	 */
	public void setHistoricalTotalDebtByEarningsBfrIncTax(
			HistoricalDataWrapper historicalTotalDebtByEarningsBfrIncTax) {
		this.historicalTotalDebtByEarningsBfrIncTax = historicalTotalDebtByEarningsBfrIncTax;
	}

	/**
	 * @return the historicalInventory
	 */
	public HistoricalDataWrapper getHistoricalInventory() {
		return historicalInventory;
	}

	/**
	 * @param historicalInventory the historicalInventory to set
	 */
	public void setHistoricalInventory(HistoricalDataWrapper historicalInventory) {
		this.historicalInventory = historicalInventory;
	}

	/**
	 * @return the inventoryGrowth
	 */
	public HistoricalDataWrapper getInventoryGrowth() {
		return inventoryGrowth;
	}

	/**
	 * @param inventoryGrowth the inventoryGrowth to set
	 */
	public void setInventoryGrowth(HistoricalDataWrapper inventoryGrowth) {
		this.inventoryGrowth = inventoryGrowth;
	}

	/**
	 * @return the historicalInventoryNoOfDays
	 */
	public HistoricalDataWrapper getHistoricalInventoryNoOfDays() {
		return historicalInventoryNoOfDays;
	}

	/**
	 * @param historicalInventoryNoOfDays the historicalInventoryNoOfDays to set
	 */
	public void setHistoricalInventoryNoOfDays(HistoricalDataWrapper historicalInventoryNoOfDays) {
		this.historicalInventoryNoOfDays = historicalInventoryNoOfDays;
	}

	/**
	 * @return the historicalNetSales
	 */
	public HistoricalDataWrapper getHistoricalNetSales() {
		return historicalNetSales;
	}

	/**
	 * @param historicalNetSales the historicalNetSales to set
	 */
	public void setHistoricalNetSales(HistoricalDataWrapper historicalNetSales) {
		this.historicalNetSales = historicalNetSales;
	}

	/**
	 * @return the netSalesGrowth
	 */
	public HistoricalDataWrapper getNetSalesGrowth() {
		return netSalesGrowth;
	}

	/**
	 * @param netSalesGrowth the netSalesGrowth to set
	 */
	public void setNetSalesGrowth(HistoricalDataWrapper netSalesGrowth) {
		this.netSalesGrowth = netSalesGrowth;
	}

	/**
	 * @return the historicalTradeReceivables
	 */
	public HistoricalDataWrapper getHistoricalTradeReceivables() {
		return historicalTradeReceivables;
	}

	/**
	 * @param historicalTradeReceivables the historicalTradeReceivables to set
	 */
	public void setHistoricalTradeReceivables(HistoricalDataWrapper historicalTradeReceivables) {
		this.historicalTradeReceivables = historicalTradeReceivables;
	}

	/**
	 * @return the historicalTradeRecievablesByNetSales
	 */
	public HistoricalDataWrapper getHistoricalTradeRecievablesByNetSales() {
		return historicalTradeRecievablesByNetSales;
	}

	/**
	 * @param historicalTradeRecievablesByNetSales the
	 *                                             historicalTradeRecievablesByNetSales
	 *                                             to set
	 */
	public void setHistoricalTradeRecievablesByNetSales(HistoricalDataWrapper historicalTradeRecievablesByNetSales) {
		this.historicalTradeRecievablesByNetSales = historicalTradeRecievablesByNetSales;
	}

	/**
	 * @return the historicalCashFlowFromOperations
	 */
	public HistoricalDataWrapper getHistoricalCashFlowFromOperations() {
		return historicalCashFlowFromOperations;
	}

	/**
	 * @param historicalCashFlowFromOperations the historicalCashFlowFromOperations
	 *                                         to set
	 */
	public void setHistoricalCashFlowFromOperations(HistoricalDataWrapper historicalCashFlowFromOperations) {
		this.historicalCashFlowFromOperations = historicalCashFlowFromOperations;
	}

	/**
	 * @return the cashFlowFromOperationsGrowth
	 */
	public HistoricalDataWrapper getCashFlowFromOperationsGrowth() {
		return cashFlowFromOperationsGrowth;
	}

	/**
	 * @param cashFlowFromOperationsGrowth the cashFlowFromOperationsGrowth to set
	 */
	public void setCashFlowFromOperationsGrowth(HistoricalDataWrapper cashFlowFromOperationsGrowth) {
		this.cashFlowFromOperationsGrowth = cashFlowFromOperationsGrowth;
	}

	/**
	 * @return the historicalCashAndCashEquivalent
	 */
	public HistoricalDataWrapper getHistoricalCashAndCashEquivalent() {
		return historicalCashAndCashEquivalent;
	}

	/**
	 * @param historicalCashAndCashEquivalent the historicalCashAndCashEquivalent to
	 *                                        set
	 */
	public void setHistoricalCashAndCashEquivalent(HistoricalDataWrapper historicalCashAndCashEquivalent) {
		this.historicalCashAndCashEquivalent = historicalCashAndCashEquivalent;
	}

	/**
	 * @return the cashAndCashEquivalentGrowth
	 */
	public HistoricalDataWrapper getCashAndCashEquivalentGrowth() {
		return cashAndCashEquivalentGrowth;
	}

	/**
	 * @param cashAndCashEquivalentGrowth the cashAndCashEquivalentGrowth to set
	 */
	public void setCashAndCashEquivalentGrowth(HistoricalDataWrapper cashAndCashEquivalentGrowth) {
		this.cashAndCashEquivalentGrowth = cashAndCashEquivalentGrowth;
	}

	/**
	 * @return the historicalShareholdersFund
	 */
	public HistoricalDataWrapper getHistoricalShareholdersFund() {
		return historicalShareholdersFund;
	}

	/**
	 * @param historicalShareholdersFund the historicalShareholdersFund to set
	 */
	public void setHistoricalShareholdersFund(HistoricalDataWrapper historicalShareholdersFund) {
		this.historicalShareholdersFund = historicalShareholdersFund;
	}

	/**
	 * @return the shareholdersFundGrowth
	 */
	public HistoricalDataWrapper getShareholdersFundGrowth() {
		return shareholdersFundGrowth;
	}

	/**
	 * @param shareholdersFundGrowth the shareholdersFundGrowth to set
	 */
	public void setShareholdersFundGrowth(HistoricalDataWrapper shareholdersFundGrowth) {
		this.shareholdersFundGrowth = shareholdersFundGrowth;
	}

	/**
	 * @return the historicalReturnOnEquity
	 */
	public HistoricalDataWrapper getHistoricalReturnOnEquity() {
		return historicalReturnOnEquity;
	}

	/**
	 * @param historicalReturnOnEquity the historicalReturnOnEquity to set
	 */
	public void setHistoricalReturnOnEquity(HistoricalDataWrapper historicalReturnOnEquity) {
		this.historicalReturnOnEquity = historicalReturnOnEquity;
	}

	/**
	 * @return the historicalOperatingRevenue
	 */
	public HistoricalDataWrapper getHistoricalOperatingRevenue() {
		return historicalOperatingRevenue;
	}

	/**
	 * @param historicalOperatingRevenue the historicalOperatingRevenue to set
	 */
	public void setHistoricalOperatingRevenue(HistoricalDataWrapper historicalOperatingRevenue) {
		this.historicalOperatingRevenue = historicalOperatingRevenue;
	}

	/**
	 * @return the operatingRevenueGrowth
	 */
	public HistoricalDataWrapper getOperatingRevenueGrowth() {
		return operatingRevenueGrowth;
	}

	/**
	 * @param operatingRevenueGrowth the operatingRevenueGrowth to set
	 */
	public void setOperatingRevenueGrowth(HistoricalDataWrapper operatingRevenueGrowth) {
		this.operatingRevenueGrowth = operatingRevenueGrowth;
	}

	/**
	 * @return the historicalSharePrice
	 */
	public HistoricalDataWrapper getHistoricalSharePrice() {
		return historicalSharePrice;
	}

	/**
	 * @param historicalSharePrice the historicalSharePrice to set
	 */
	public void setHistoricalSharePrice(HistoricalDataWrapper historicalSharePrice) {
		this.historicalSharePrice = historicalSharePrice;
	}

	/**
	 * @return the sharePriceGrowth
	 */
	public HistoricalDataWrapper getSharePriceGrowth() {
		return sharePriceGrowth;
	}

	/**
	 * @param sharePriceGrowth the sharePriceGrowth to set
	 */
	public void setSharePriceGrowth(HistoricalDataWrapper sharePriceGrowth) {
		this.sharePriceGrowth = sharePriceGrowth;
	}

	/**
	 * @return the totalRevenueInLineWithProfitAfterTaxCheck
	 */
	public String getTotalRevenueInLineWithProfitAfterTaxCheck() {
		return totalRevenueInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @param totalRevenueInLineWithProfitAfterTaxCheck the
	 *                                                  totalRevenueInLineWithProfitAfterTaxCheck
	 *                                                  to set
	 */
	public void setTotalRevenueInLineWithProfitAfterTaxCheck(String totalRevenueInLineWithProfitAfterTaxCheck) {
		this.totalRevenueInLineWithProfitAfterTaxCheck = totalRevenueInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @return the earningsPerShareGrowthInLineWithProfitAfterTaxCheck
	 */
	public String getEarningsPerShareGrowthInLineWithProfitAfterTaxCheck() {
		return earningsPerShareGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @param earningsPerShareGrowthInLineWithProfitAfterTaxCheck the
	 *                                                            earningsPerShareGrowthInLineWithProfitAfterTaxCheck
	 *                                                            to set
	 */
	public void setEarningsPerShareGrowthInLineWithProfitAfterTaxCheck(
			String earningsPerShareGrowthInLineWithProfitAfterTaxCheck) {
		this.earningsPerShareGrowthInLineWithProfitAfterTaxCheck = earningsPerShareGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @return the grossProfitMarginCheck
	 */
	public String getGrossProfitMarginCheck() {
		return grossProfitMarginCheck;
	}

	/**
	 * @param grossProfitMarginCheck the grossProfitMarginCheck to set
	 */
	public void setGrossProfitMarginCheck(String grossProfitMarginCheck) {
		this.grossProfitMarginCheck = grossProfitMarginCheck;
	}

	/**
	 * @return the debtLevelCheck
	 */
	public String getDebtLevelCheck() {
		return debtLevelCheck;
	}

	/**
	 * @param debtLevelCheck the debtLevelCheck to set
	 */
	public void setDebtLevelCheck(String debtLevelCheck) {
		this.debtLevelCheck = debtLevelCheck;
	}

	/**
	 * @return the inventoryGrowthInLineWithProfitAfterTaxCheck
	 */
	public String getInventoryGrowthInLineWithProfitAfterTaxCheck() {
		return inventoryGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @param inventoryGrowthInLineWithProfitAfterTaxCheck the
	 *                                                     inventoryGrowthInLineWithProfitAfterTaxCheck
	 *                                                     to set
	 */
	public void setInventoryGrowthInLineWithProfitAfterTaxCheck(String inventoryGrowthInLineWithProfitAfterTaxCheck) {
		this.inventoryGrowthInLineWithProfitAfterTaxCheck = inventoryGrowthInLineWithProfitAfterTaxCheck;
	}

	/**
	 * @return the tradeReceivablesToNetSalesCheck
	 */
	public String getTradeReceivablesToNetSalesCheck() {
		return tradeReceivablesToNetSalesCheck;
	}

	/**
	 * @param tradeReceivablesToNetSalesCheck the tradeReceivablesToNetSalesCheck to
	 *                                        set
	 */
	public void setTradeReceivablesToNetSalesCheck(String tradeReceivablesToNetSalesCheck) {
		this.tradeReceivablesToNetSalesCheck = tradeReceivablesToNetSalesCheck;
	}

	/**
	 * @return the cashFlowFromOperationsCheck
	 */
	public String getCashFlowFromOperationsCheck() {
		return cashFlowFromOperationsCheck;
	}

	/**
	 * @param cashFlowFromOperationsCheck the cashFlowFromOperationsCheck to set
	 */
	public void setCashFlowFromOperationsCheck(String cashFlowFromOperationsCheck) {
		this.cashFlowFromOperationsCheck = cashFlowFromOperationsCheck;
	}

	/**
	 * @return the returnOnEquityCheck
	 */
	public String getReturnOnEquityCheck() {
		return returnOnEquityCheck;
	}

	/**
	 * @param returnOnEquityCheck the returnOnEquityCheck to set
	 */
	public void setReturnOnEquityCheck(String returnOnEquityCheck) {
		this.returnOnEquityCheck = returnOnEquityCheck;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/**
	 * @return the lastUpdatedOn
	 */
	public LocalDateTime getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	/**
	 * @param lastUpdatedOn the lastUpdatedOn to set
	 */
	public void setLastUpdatedOn(LocalDateTime lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

}
