package com.naga.share.market.fundamental.analysis.model.utils;

import java.io.IOException;

import javax.persistence.AttributeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.naga.share.market.fundamental.analysis.model.cagr.HistoricalDataWrapper;

/**
 * @author Nagaaswin S
 *
 */
public class HistoricalDataWrapperConverter implements AttributeConverter<HistoricalDataWrapper, String> {

	Logger logger = LogManager.getLogger(this.getClass());

	@Override
	public String convertToDatabaseColumn(HistoricalDataWrapper attribute) {
		String databaseColumn = null;
		try {
			databaseColumn = new ObjectMapper().writeValueAsString(attribute);
		} catch (final JsonProcessingException e) {
			logger.error("JSON writing error", e);
		}
		return databaseColumn;
	}

	@Override
	public HistoricalDataWrapper convertToEntityAttribute(String dbData) {
		HistoricalDataWrapper historicalDataWrapper = new HistoricalDataWrapper();
		try {
			historicalDataWrapper = new ObjectMapper().readValue(dbData, HistoricalDataWrapper.class);
		} catch (final IOException e) {
			logger.error("JSON reading error", e);
		}
		return historicalDataWrapper;
	}

//	@Override
//	public String convertToDatabaseColumn(List<HistoricalData> attribute) {
//		StringBuffer column = new StringBuffer();
//		attribute.forEach(t -> column.append(t + ","));
//		column.deleteCharAt(column.length() - 1);
//		return column.toString();
//	}
//
//	@Override
//	public List<HistoricalData> convertToEntityAttribute(String dbData) {
//		List<Double> values = new ArrayList<Double>();
//		Arrays.asList(dbData.split("[,]")).forEach(value -> values.add(Double.parseDouble(value)));
//		return null;
//	}
}
